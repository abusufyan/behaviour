<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
        	protected $table = 'charts';

        	protected $fillable = [
		'userId', 'profileId','avatarId','backgroundId','behaviour','reward','rewardId','steps','status',
		];
}

