<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    //
    	protected $table = 'avatars';


 protected $fillable = [
         'avatarName', 'avatarImage','type','category','price',
    ];

}
