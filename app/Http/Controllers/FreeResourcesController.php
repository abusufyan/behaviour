<?php

namespace App\Http\Controllers;

use App\FreeResources;
use Carbon\Carbon;
use Froiden\Envato\Traits\AppBoot;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class FreeResourcesController extends Controller
{

	public function freeresources()
	{
		return view('freeresource');
	}

	public function filedownload($id)
    {
    $entry = FreeResources::where('id', '=', $id)->first();
    $pathToFile="public/category/".$entry->file;
/*    print_r($pathToFile);
    exit();*/
    return response()->download($pathToFile);           
    }
     
  public function categorylist(){

  		$data= FreeResources::all();
        return view('admin.freeresources.addcategory',compact('data'));

  }
  public function list($id){

  		$list = FreeResources::where('category',$id)->get();

        return view('buddytokens',compact('list','id'));

  }
  public function buddybucks(){

  		$buddybucks= FreeResources::where('category','Buddy Bucks')->get();

        return view('admin.freeresources.buddybucks',compact('buddybucks'));

  }public function coloringsheet(){

  // 		$buddytoken = 'Special Buddy Tokens';
		// $buddybuck = 'Buddy Bucks';
		// $coloring = 'Coloring Sheets';

  		$coloringsheet= FreeResources::where('category','Coloring Sheets')->get();

        return view('admin.freeresources.coloringsheet',compact('coloringsheet'));

  }

  public function categorylistt(){

  		$data= FreeResources::all();
        return view('admin.freeresources.categorylist',compact('data'));

  }

  public function deletecategory($id){
    FreeResources::where('id',$id)->delete();
    return redirect('categorylist')->with('message','Category Deleted Sucessfully');

    }


public function insertcategory(Request $request)
  {
         
          if ($request->hasFile('file'))
          {
            $extension=$request->file->extension();
            $filename=time()."_.".$extension;
            $request->file->move(public_path('category'),$filename);
 
             FreeResources::create([
            'status' => $request->status,
            'file' => $filename,
            'title' => $request->title,
            'description' => $request->description,
            'category' => $request->category,
            ]);
            return redirect('/addcategory')->with('message','New Category Added Sucessfully'); 
      }



  }


  public function editcategory(Request $request){
        $id = $request->id;
        $data=FreeResources::find($id);
        if ($request->hasFile('file'))
        {
        $extension=$request->file->extension();
        $filename=time()."_.".$extension;
        $request->file->move(public_path('category'),$filename);
        $data->status = $request->status;
        $data->category = $request->category;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->file = $filename;
        $data->save();

        return redirect('/categorylist')->with('message','Category Edited Sucessfully');
        }
        else
        {
            $data->status = $request->status;
            $data->category = $request->category;
            $data->title = $request->title;
            $data->description = $request->description;
            $data->save();
            return redirect('/categorylist')->with('message','Category Edited Sucessfully');
        }
    }

    public function updatecategory($id)
    {
        $dat=FreeResources::find($id);
        $data = json_decode(json_encode($dat), true);
        return $data;
    }

// Upload free resources doc with category special buddy tokens, buddy bucks, coloring sheet 
// endLine
}
