<?php

namespace App\Http\Controllers;

use App\TryForFree;
use Illuminate\Http\Request;

class TryForFreeController extends Controller
{
     
public function insertfreetry(Request $request){
    $extension=$request->backgroundimage->extension();
    $background=time()."_.".$extension;
    $extension=$request->buddyimage->extension();
    $buddyimage=time()."_.".$extension;
    $request->backgroundimage->move(public_path('tryforfree'),$background);
    $request->buddyimage->move(public_path('tryforfree'),$buddyimage);
    TryForFree::create([
    'title' => $request->title,   
    'buddyimage' => $buddyimage,
    'backgroundimage' => $background,
    'steps' => $request->steps,
]);
 return back()->with('message','Added Sucessfully'); 
}
public function list(){
  $tryforfree= TryForFree::all();
  return view('admin.tryforfree.list',compact('tryforfree'));
}

public function deletetryforfree($id){
   TryForFree::where('id',$id)->delete();
   return redirect('/tryforfreelist')->with('message','Deleted Sucessfully');
}


public function edittryforfree($id)
{
$try=TryForFree::find($id);
$data = json_decode(json_encode($try), true);
return $data;
}
 public function updateTry(Request $request){
  $id = $request->id;
  $try=TryForFree::find($id);
  
        if ($request->hasFile('buddyimage'))
        {
        $extension=$request->buddyimage->extension();
        $filename=time()."_.".$extension;
        
        $request->buddyimage->move(public_path('tryforfree'),$filename); 
        $try->title = $request->title;
        $try->steps = $request->steps;
        $try->buddyimage = $filename;
        $try->save();

        return redirect('/tryforfreelist')->with('message','Updated Sucessfully');
        }
          if ($request->hasFile('backgroundimage')  )
        {
       
        $extension2=$request->backgroundimage->extension();
        $filename2=time()."_.".$extension2;
        
        $request->backgroundimage->move(public_path('tryforfree'),$filename2);

        $try->title = $request->title;
        $try->steps = $request->steps;       
        $try->backgroundimage = $filename2;
        $try->save();

        return redirect('/tryforfreelist')->with('message','Updated Sucessfully');
        }
   
        else
        {
        $try->title = $request->title;
        $try->steps = $request->steps;
        
        $try->save();
        return redirect('/tryforfreelist')->with('message','Updated Sucessfully');;  
        }
   

 }

public function index()
{
return view('tryforfree');
}


public function indexdemo()
{
return view('demo');
}



}
