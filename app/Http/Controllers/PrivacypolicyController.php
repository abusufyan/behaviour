<?php

namespace App\Http\Controllers;

use App\Privacypolicy;
use Illuminate\Http\Request;

class PrivacypolicyController extends Controller
{
     
    public function privacypolicy(){
    $ploicy =  Privacypolicy::all();
   return view('privacypolicy',compact('ploicy' ));
    }
      

    public function editprivacypolicy(){
    $ploicy =  Privacypolicy::all();

    return view('admin.privacypolicy.privacypolicy',compact('ploicy' ));
    }
    public function updateprivacypolicy(Request $request){

    Privacypolicy::where('id',$request->id)->update(['description'=>$request->description]);
    return redirect('/editprivacypolicy')->with('message','Updated Sucessfully');

    }
}
