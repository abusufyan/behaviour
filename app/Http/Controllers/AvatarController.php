<?php

namespace App\Http\Controllers;

use App\Avatar;
use Illuminate\Http\Request;

class AvatarController extends Controller
{
    
    public function insertavatar(Request $request){
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('avatar'),$filename);
        $price=$request->price;
        $type=$request->type;
        
         Avatar::create([
        'avatarName' => $request->avatarName,
        'avatarImage' => $filename,
        'type' => $type,
        'price' => $price,
        'category' => 'Buddy',
        ]);
        return redirect('/addavatar')->with('message','New Avatar Added Sucessfully'); 

    }

    public function avatarlist(){
        $avatar= Avatar::all();
        return view('admin.avatar.avatarlist',compact('avatar'));
    }
    
    public function deleteavatar($id){
    Avatar::where('id',$id)->delete();
    return redirect('/avatarlist')->with('message','Avatar Deleted Sucessfully');

    }

    public function editavatar(Request $request){
        $id = $request->id;
        $avatar=Avatar::find($id);
        if ($request->hasFile('image'))
        {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('avatar'),$filename);
        $avatar->avatarName = $request->avatarName;
        $avatar->type = $request->type;
        $avatar->price = $request->price;
        $avatar->avatarImage = $filename;
        $avatar->save();

        return redirect('/avatarlist')->with('message','Avatar Edited Sucessfully');
        }
        else
        {   
             $avatar->type = $request->type;
        $avatar->price = $request->price;
            $avatar->avatarName = $request->avatarName;
            $avatar->save();
            return redirect('/avatarlist')->with('message','Avatar Edited Sucessfully');
        }
    }

    public function updateavatar($id)
    {
        $avatar=Avatar::find($id);
        $data = json_decode(json_encode($avatar), true);
        return $data;
    }


}
