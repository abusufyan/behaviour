<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Controllers\MailController;

use DB;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    public function sendemail(Request $request){

    $email=$request->email;

    $result = DB::table('users')
    ->select('*')
    ->where('email', $email)
    ->first();
    if($result != null){
    $id =$result->id;   
    MailController::sendforgetpwdEmail($id,$email);
    return back()->with('message','Please Check Your Email');
    }
    else{
    return back()->with('message','Email error');
    }      


    }

    public function verifyUser(Request $request)
    {
        $user_id = \Illuminate\Support\Facades\Request::get('code'); 
         
        return view('auth.passwords.reset',compact('user_id'));
    }
    public function updatepassword(Request $request){
          
            $confirmpassword=$request->confirmpassword;
            $newpassword=$request->password;
            if($newpassword == $confirmpassword ){
                $id=$request->user_id;

            $password = DB::table('users')
            ->where('id', $id)
            ->update(['password' => bcrypt($newpassword)]);
            return back()->with('message','Password Updated');;

            }
            else{
            return back()->with('message','Confirm Password not matched');;
            }
        

        } 

          
}
