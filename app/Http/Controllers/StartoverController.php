<?php

namespace App\Http\Controllers;

use App\Startover;
use Illuminate\Http\Request;
use App\Chart;
use Illuminate\Support\Facades\Session;
use DB;
use PDF;


class StartoverController extends Controller
{

	public function __construct()
    {
        $this->middleware('user');
    }
     
 public function startover($id){
  
	$profile = Chart::Join('avatars', 'avatars.id', '=', 'charts.avatarId')
	->Join('profile', 'profile.id', '=', 'charts.profileId')
	->Join('background_images', 'background_images.id', '=', 'charts.backgroundId')
	->Join('rewards', 'rewards.id', '=', 'charts.rewardId')
	->select('avatars.avatarImage','background_images.*','charts.*','profile.childName','rewards.rewardImage')		
	->where('charts.id',$id)
	->first(); 
 
	$chart_id=$profile->id;
	Session::put('chart_id',$chart_id);

	if ($profile->steps == 5)
	{
	 return view('startover.game5',compact('profile'));
	}  
	if ($profile->steps == 7)
	{
		return view('startover.game8',compact('profile'));
	}
    if ($profile->steps == 10)
	{ 		
	return view('startover.game11',compact('profile'));
    }
	if ($profile->steps == 20)
	{ 
	return view('startover.game20',compact('profile'));
    }
	else
	{
	return view('startover.game16',compact('profile'));	
	} 
  
 }
 public function updatesteps(Request $request){ 

 	$id=$request->counts;
 	if($id == null){
 		$id = 0;
 	}
 	$chart_id = Session::get('chart_id');
	$cartdata = Chart::find($chart_id);
	$steps=$cartdata->steps;
	$pid = Session::get('profile');
   	$coins = DB::table('coins')->select('*')->where('profileId', $pid)->first();  	 

	if ($id == $steps){		
	$update_chart2 = DB::table('charts')
	->where('id', $chart_id)
	->update(['jumps' => 0,
		      'status' => 1 ]);
	$prev=$coins->coins;
   	$updatecoin = $prev +1;

    $update = DB::table('coins')
	->where('profileId', $pid)
	->update(['coins' => $updatecoin]);

	return response()->json(['success' => 'Completed Sucessfully!!']);
	} 	 	
	if($coins)
	{
	$lastcoins=$coins->coins;
	$newcoins=$lastcoins+1;
	$update = DB::table('coins')
	->where('profileId', $pid)
	->update(['coins' => $newcoins]);

	$update_chart = DB::table('charts')
	->where('id', $chart_id)
	->update(['jumps' => $id]);

	return response()->json(['success' => 'Coins updated']);
    }
	else{
	$data = array('profileId' => $pid,'coins' => $id);
	DB::table('coins')->insert($data);
	$update_chart = DB::table('charts')
	->where('id', $chart_id)
	->update(['jumps' => $id]);
	return response()->json(['success' => 'Coins added']);
	}
   

 }

  public function reversesteps(Request $request){
 	$id=$request->counts;
 	  $pid = Session::get('profile');
      $coins = DB::table('coins')->select('*')->where('profileId', $pid)->first();
      if($coins){
      	$lastcoins=$coins->coins;
      	$newcoins=$lastcoins - 1;
      	$update = DB::table('coins')
        ->where('profileId', $pid)
        ->update(['coins' => $newcoins]);

		$chart_id = Session::get('chart_id'); 
		$update_chart = DB::table('charts')
		->where('id', $chart_id)
		->update(['jumps' => $id]);

        return response()->json(['success' => 'Coins reversed']);
      }     


 }

 public function download()
 {    
 	$pdf = PDF::loadView('certificatedownload');
 	return $pdf->download('certificate.pdf');
 }


 public function certificate($id)
 {    
 	$certificate = Chart::Join('avatars', 'avatars.id', '=', 'charts.avatarId')
	->Join('profile', 'profile.id', '=', 'charts.profileId')
	->Join('background_images', 'background_images.id', '=', 'charts.backgroundId')
	->Join('rewards', 'rewards.id', '=', 'charts.rewardId')
	->select('avatars.avatarImage','background_images.*','charts.*','profile.childName','rewards.rewardImage')		
	->where('charts.id',$id)
	->first();  
	if($certificate)
	{
   		$status	= $certificate->status;
		$jumps = $certificate->jumps;

	    if ($status == 1 && $jumps == 0 )
	    {

		Session::put('certificate',$certificate);
		return view('certificate',compact('certificate'));	
	    }
	    else
	    {
	       return redirect('/startover/'.$id);
	    }
	}

	else
	{
      return redirect('/accounthome')->with('message','Sorry!! Chart Not Found.');
	}      
 	
}



}
