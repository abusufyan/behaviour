<?php

namespace App\Http\Controllers;

use App\TermsAndConditions;
use Illuminate\Http\Request;

class TermsAndConditionsController extends Controller
{
    
   public function termsandcondition(){
    $terms =  TermsAndConditions::all();
   return view('termsandcondition',compact('terms' ));
    }
      

    public function manageterms(){
    $terms =  TermsAndConditions::all();

    return view('admin.termsandcondition.termsandcondition',compact('terms' ));
    }
    public function updateterms(Request $request){

    TermsAndConditions::where('id',$request->id)->update(['description'=>$request->description]);
    return redirect('/manageterms')->with('message','Updated Sucessfully');

    }

}
