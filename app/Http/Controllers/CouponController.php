<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use Stripe\Charge;
use Auth;
use App\User;
use App\Paymenthistory;    

class CouponController extends Controller
{  
     public function applycoupon($id){
      $coupon= Coupon::where('id',$id)->first();
      return view('applycoupon',compact('id','coupon'));
     } 
     public function verifycoupon(Request $request){
      $userEmail= Session::get('user-email');  
      $userData=User::where('email',$userEmail)->first(); 
      $userId=$userData->id;

      if($request->type == '%age'){
      $coupon=$request->coupon;
      $id=$request->id;
      $right= Coupon::where('id',$id)->first();
      $actual=$right->code;
      $price=$right->price;
      Session::put('price',$price);

      if($coupon == $actual){
      
      User::where('id',$userId)->update(['type'=>$request->type]);   
      return redirect('/payment');
      }
      else{
      return redirect('/applycoupon/'.$id)->with('message','Invalid Coupon');
      }
     }
     else{
      $coupon=$request->coupon;
      $id=$request->id;
      $right= Coupon::where('id',$id)->first();
      $actual=$right->code;
      $price=$right->price;
      Session::put('price',$price);  

      if($coupon == $actual){
         
      User::where('id',$userId)->update(['type'=>$request->type]);   
        return redirect('/thankyou');   
      }
      else
      {
      return redirect('/applycoupon/'.$id)->with('message','Invalid Coupon');
      }  
    } 
  } 

     public function payment(){

      return view('payment');
     }    
     //Confirm payment
     public function confirm(){
         
       $stripe=Stripe::setApiKey("sk_test_51HKICwI0QL79fDpahCJF2yTJc2l7RATNd2jMuUygYjtWvYJiU7tS5wrc7kJRcKEeihmPoSmOHoM4b9iesD5NycOK001FvAbJ9F");
        // dd($request->stripeToken);
        $price= Session::get('price');
        $userEmail= Session::get('user-email');  
        $userData=User::where('email',$userEmail)->first();      
        $userId=$userData->id;
        $charge=Charge::create(array(
            'amount' => $price * 100,
            'currency' => 'usd',
            'source'  => 'tok_mastercard',
            // 'source'  => $stripe,
           ));
          if($charge){
            Paymenthistory::create([
            'userId' => $userId,
            'amount' => $price,        
            ]);
           User::where('id',$userId)->update(['active'=>1]);
            
          MailController::registerEmail($userEmail,$price);
  
          }
        else{
        return 'Invalid Payment';
        }
        return redirect('/thankyou');   
       
     }

     public function insertcoupon(Request $request){
        Coupon::create([
        'title' => $request->title,
        'price' => $request->price,
        'description' => $request->description,
        'code' => $request->code,
        'type' => $request->type,
        'percentage' => $request->percentage,
        'for' => $request->for,
        'email' => $request->email,
        'startDate' => $request->startDate,
        'endDate' => $request->endDate,

        ]); 
        $email=$request->email;
        if($email){
        $coupon=$request->code;
        MailController::sendCoupon($email,$coupon);
        return redirect('/couponlist');   
        }
        else{
        return redirect('/couponlist');   
        }  
      }
      public function couponlist(){
      $coupon= Coupon::all();
      return view('admin.coupon.couponlist',compact('coupon'));
      }

      public function pricing(){
         //Auth::logout();
      $email=Auth::user()->email; 
      Session::put('user-email',$email);
      $percentage= Coupon::where('type','%age')->orderBy('id', 'desc')->take(1)->first();
     // $free= Coupon::where('type','free')->orderBy('id', 'desc')->take(1)->first();
      $fullaccess= Coupon::where('type','full')->orderBy('id', 'desc')->take(1)->first();
      $price=$fullaccess->price; 
       

      Session::put('price',$price);  
 
      return view('pricing',compact('percentage','fullaccess'));
      }
      public function deletecoupon($id){
      Coupon::where('id',$id)->delete();
      return redirect('/couponlist')->with('message','Deleted Sucessfully');
      }

      public function editcoupon($id)
      {
      $coupon=Coupon::find($id);
      $data = json_decode(json_encode($coupon), true);
      return $data;
      }
       public function updatecoupon(Request $request){
        $id = $request->id;
        $coupon=Coupon::find($id);
        $coupon->title = $request->title;
        $coupon->price = $request->price;
        $coupon->description = $request->description;
        $coupon->code = $request->code;
        $coupon->percentage = $request->percentage;
        $coupon->startDate = $request->startDate;
        $coupon->endDate = $request->endDate;
        $coupon->email = $request->email;
        $coupon->for = $request->for;
        $coupon->save();
        return redirect('/couponlist')->with('message','Updated Sucessfully');;   

       }
}
