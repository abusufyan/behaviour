<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Character;
use DB;
class ProfileController extends Controller
{

        public function __construct()
        {
        $this->middleware('auth');
        }
      
      public function insertprofile(Request $request){
      Profile::create([
      'childName' => $request->childName,
      'userId' => $request->userId,
      'characterId' => $request->characterId,
      ]);

      $last_id = DB::getPDO('profile')->lastInsertId(); 
      Session::put('profile',$last_id);           
      $childName= $request->childName;    
      $character = Character::where('id',$request->characterId)->first(); 
      Session::put('character',$character);
      Session::put('childname',$childName);

      return redirect('/chart'); 


      }
      public function editprofile($id){
        $characterimg= Character::all();
        $profile=Profile::find($id);
        return view('editchildprofile',compact('characterimg','profile','id')); 
      }
      public function updatechildprofile(Request $request){
        $id=$request->id;
        $profile=Profile::find($id);
         $profile->childName = $request->childName;
        $profile->characterId = $request->characterId;
        $profile->save();
        $character = Character::where('id',$request->characterId)->first(); 

        Session::put('character', $character);
        Session::put('childname', $request->childName);

      
        return back()->with('message','Profile Updated Sucessfully'); 
      }
      public function deleteprofile($id){

     Profile::where('id',$id)->delete();
        return redirect('/accounthome'); 
    }


}
