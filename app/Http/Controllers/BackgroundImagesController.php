<?php
namespace App\Http\Controllers;
use App\BackgroundImages;
use Illuminate\Http\Request;

class BackgroundImagesController extends Controller
{
      public function insertbackground(Request $request){
        $extension=$request->image->extension();
        $filename=rand().$extension;
        $image = $filename; 
        $request->image->move(public_path('background'),$filename);

        $extension=$request->podImage->extension();
        $filename2=rand().$extension;
        $image2 = $filename2; 
        $request->podImage->move(public_path('background'),$filename2);
        $aa = BackgroundImages::create([
        'podImage' => $image2,      
        'backgroundImage' => $image,   
        'backgroundName' => $request->backgroundName,
        'type' => $request->type,
        'price' => $request->price,
        'category' => 'BackgroundImage',
        ]);

        return redirect('/addbackground')->with('message','Added Sucessfully'); 

    }
   public function backgroundlist(){
        $background= BackgroundImages::all();
        return view('admin.backgroundImages.backgroundlist',compact('background'));
    }
    
    public function deletebackground($id){
    BackgroundImages::where('id',$id)->delete();
    return redirect('/backgroundlist')->with('message','Background Deleted Sucessfully');

    }

    public function editbackground($id)
    { 
    $try=BackgroundImages::find($id);
    $data = json_decode(json_encode($try), true);
    return $data;
    }
 public function updatebackground(Request $request){
        $id = $request->id;
        $try=BackgroundImages::find($id);
  
        if ($request->hasFile('backgroundImage'))
        {
        $extension=$request->backgroundImage->extension();
        $filename=time()."_.".$extension;
        
        $request->backgroundImage->move(public_path('background'),$filename); 
        $try->backgroundName = $request->backgroundName;
        $try->backgroundImage = $filename;
         $try->type = $request->type;
        $try->price = $request->price;
        $try->save();

        return redirect('/backgroundlist')->with('message','Updated Sucessfully');
        }
          if ($request->hasFile('podImage')  )
        {
       
        $extension2=$request->podImage->extension();
        $filename2=time()."_.".$extension2;
        
        $request->podImage->move(public_path('background'),$filename2);

        $try->backgroundName = $request->backgroundName;
         $try->podImage = $filename2;
         $try->type = $request->type;
        $try->price = $request->price;
        $try->save();

        return redirect('/backgroundlist')->with('message','Updated Sucessfully');
        }
   
        else
        {
        $try->backgroundName = $request->backgroundName;
        $try->type = $request->type;
        $try->price = $request->price;
        $try->save();
        return redirect('/backgroundlist')->with('message','Updated Sucessfully');;  
        }
   

 }
  
}
