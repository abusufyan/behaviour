<?php

namespace App\Http\Controllers;

use App\Character;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
 
    public function insertCharacter(Request $request){
    $extension=$request->image->extension();
    $filename=time()."_.".$extension;
    $image = $filename; 
    $request->image->move(public_path('character'),$filename);
    Character::create([
    'characterImage' => $image,   
    'characterName' => $request->characterName,
    'characterGender' => $request->characterGender,

    ]);

    return redirect('/addcharacter')->with('message','New Character Added Sucessfully'); 

    }
    public function characterlist(){
    $character= Character::all();
    return view('admin.character.characterlist',compact('character'));
    }
    
    public function deletecharacter($id){
    Character::where('id',$id)->delete();
    return redirect('/characterlist')->with('message','Character Deleted Sucessfully');

    }

    public function editcharacter($id){
    $character=Character::find($id);
    $data = json_decode(json_encode($character), true);
    return $data;  
    }
     public function updatecharacter(Request $request){
        $id = $request->id;
        $character=Character::find($id);
        if ($request->hasFile('image'))
        {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('character'),$filename);
        $character->characterName = $request->characterName;
        $character->characterGender = $request->characterGender;
        $character->characterImage = $filename;
        $character->save();

        return redirect('/characterlist')->with('message','Character Edited Sucessfully');
        }
        else
        {
            $character->characterName = $request->characterName;
                        $character->characterGender = $request->characterGender;

            $character->save();
            return redirect('/characterlist')->with('message','Character Edited Sucessfully');
        }
    }
 
    
 }
