<?php

namespace App\Http\Controllers;

 use App\Mail\sendCoupon;
 use App\Mail\email;
 use App\Mail\ForgetpwdEmail;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function sendCoupon($email, $coupon){
        $data = [
            'coupon' => $coupon,
            'email' => $email,
         ];
 
        Mail::to($email)->send(new sendCoupon($data));
        
    }
    public static function registerEmail ($userEmail, $price){
        $data = ['email' => $userEmail,
                   'price'=>$price];

        Mail::to($userEmail)->send(new email($data));
        
    }

     public static function sendforgetpwdEmail($id, $email){
        $data = [
            'id' => $id,
            'email' => $email
        ];
      
        Mail::to($email)->send(new ForgetpwdEmail($data));
        
    }


    


    
}
