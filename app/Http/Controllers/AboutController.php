<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
     public function aboutus(){
    $terms =  About::all();
   return view('aboutus',compact('terms' ));
    }
      

    public function manageaboutus(){
    $terms =  About::all();

    return view('admin.aboutus',compact('terms' ));
    }
    public function updatesAboutus(Request $request){

 
    About::where('id',$request->id)->update(['description'=>$request->description]);
    return redirect('/manageaboutus')->with('message','Updated Sucessfully');

    }
}
