<?php

namespace App\Http\Controllers;
use App\Character;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\AccountHome;
use Illuminate\Http\Request;
use App\Shop;
use Illuminate\Support\Facades\Session;
use DB;
use App\Avatar;
use App\BackgroundImages;


class AccountHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
         $this->middleware('user');
    }

    public function index()
    {    
        $userId=Auth::user()->id;
        $active=Auth::user()->active;
        $character= Character::all();
        $user = Profile::where('userId', $userId)->count(); 
        if($active == 0){
         $profile = Profile::where('userId', $userId)
        ->Join('characters', 'characters.id', '=', 'profile.characterId')
        ->Join('users', 'users.id', '=', 'profile.userId')
        ->select('profile.*','characters.characterImage')
        ->take(1)
        ->get();
        return view('account_home',compact('character','profile','user','active'));
       
        }
        else{
        $profile = Profile::where('userId', $userId)
        ->Join('characters', 'characters.id', '=', 'profile.characterId')
        ->Join('users', 'users.id', '=', 'profile.userId')
        ->select('profile.*','characters.characterImage')
        ->get();
        return view('account_home',compact('character','profile','user','active'));

        }       

    }

    public function editprofile($id){
       
       $user=User::find($id);
      
       return view('editprofile',compact('user'));

    }
    public function updateprofile(Request $request){

          $user=User::find($request->id);
          $password=$user->password;  
          if(Hash::check($request->oldpassword, $password))
          {
            $newpassword = Hash::make($request->newpassword); 
            $user->password = $newpassword;                   
            $user->save();  
            return back()->with('message','Password Updated Sucessfully');
          }
          else{
           return back()->with('message','Invalid Old Password ');
          }
 
    }

 
        public function shops(){
          $buddy= Avatar::where('type','shop')->get();
          $background= BackgroundImages::where('type','shop')->get();
          return view('shop',compact('buddy','background'));
        }

    public function shopconfirm($id)
    {
    $profileid = Session::get('profile');
    $item=Avatar::find($id);
    $avatar = DB::table('confirmorder')->select('*')->where('avatarId', $id)->where('profileId', $profileid)->count();

    $data = json_decode(json_encode($item), true);
    $data['data']=$data;
    $data['avatar']=$avatar;
    return $data;

    }


    public function shopBackground($id)
    {
    $profileid = Session::get('profile'); 
    $item=BackgroundImages::find($id);   
    $data = json_decode(json_encode($item), true);
    $background = DB::table('confirmorder')->select('*')->where('backgroundId', $id)->where('profileId', $profileid)->count();

    $data = json_decode(json_encode($item), true);
    $data['data']=$data;
    $data['background']=$background;

    return $data;

    }

    public function shopnow(Request $request)
    {  

       $coins=$request->coins;
       $id = Session::get('profile');
       $profile=Profile::find($id);
       $profileCoin = DB::table('coins')->select('*')->where('profileId', $id)->first();
       if($profileCoin)
       {
       $profileCoins=$profileCoin->coins;}
       else{

        $profileCoins=0;
       }

       // $profileCoins=250;
       if($profileCoins < $coins)
       {
         return back()->with('error', 'The error message here!');

       }
       else{
        $remainingcoins  =$profileCoins - $coins;
              
       if($request->category=='Buddy')
       {
          $data = array(
          'profileId' => $id,
          'avatarId' => $request->shopId,
          'status' => 1
          );
         $update = DB::table('coins')
        ->where('profileId', $id)
        ->update(['coins' => $remainingcoins]);  

          DB::table('confirmorder')->insert($data);
       return redirect('/indexhome/'.$id)->with('error','Thank you');
       }
       else{
          $data = array(
          'profileId' => $id,
          'backgroundId' => $request->shopId,
          'status' => 1
          );
          $update = DB::table('coins')
          ->where('profileId', $id)
          ->update(['coins' => $remainingcoins]);

          DB::table('confirmorder')->insert($data);
       return redirect('/indexhome/'.$id)->with('error','Thank you');

       } 
      

       
       }

      
                                        

    }
 
    
  }
