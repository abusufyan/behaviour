<?php

namespace App\Http\Controllers;

use App\Rewards;
use Illuminate\Http\Request;

class RewardsController extends Controller
{ 

     public function insertreward(Request $request){
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('reward'),$filename);
         Rewards::create([
        'rewardName' => $request->rewardName,
        'rewardImage' => $filename,
        ]);
        return redirect('/addreward')->with('message','New Reward Added Successfully'); 

    }


    public function rewardlist(){
        $reward= Rewards::all();
        return view('admin.rewards.rewardlist',compact('reward'));
    }
    
    public function deletereward($id){
    Rewards::where('id',$id)->delete();
    return redirect('/rewardlist')->with('message','Reward Deleted Successfully');

    } 

     public function editreward($id){
    $character=Rewards::find($id);
    $data = json_decode(json_encode($character), true);
    return $data;  
    }
     public function updatereward(Request $request){
        $id = $request->id;
        $character=Rewards::find($id);
        if ($request->hasFile('image'))
        {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('reward'),$filename);
        $character->rewardName = $request->rewardName;
         $character->rewardImage = $filename;
        $character->save();

        return redirect('/rewardlist')->with('message','Rewar Edited Successfully');
        }
        else
        {
        $character->rewardName = $request->rewardName;
        $character->save();
        return redirect('/rewardlist')->with('message','Reward Edited Successfully');
        }
    }
}
