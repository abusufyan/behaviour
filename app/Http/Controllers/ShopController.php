<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insertitem(Request $request)
    {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('shop'),$filename);
        Shop::create([
        'name' => $request->name,
        'price' => $request->price,
        'type' => $request->type,
        'image' => $filename,
        ]);
        return redirect('/addshopitem')->with('message','Added Sucessfully');
         
    }
    
    public function shoplist()
    {
    $item= Shop::all();
    return view('admin.shop.list',compact('item'));
    }
 
    public function updateitem(Request $request)
    {
        $id = $request->id;
        $item=Shop::find($id);
        if ($request->hasFile('image'))
        {
        $extension=$request->image->extension();
        $filename=time()."_.".$extension;
        $request->image->move(public_path('shop'),$filename);
        $item->name = $request->name;
        $item->image = $filename;
        $item->price = $request->price;
          $item->type = $request->type;
        $item->save();

        return redirect('/shoplist')->with('message','Updated Sucessfully');
        }
        else
        {   $item->type = $request->type;
            $item->name = $request->name;
            $item->price = $request->price;
            $item->save();
            return redirect('/shoplist')->with('message','Updated Sucessfully');
        }

    }
   
    public function edititem($id)
    {
    $item=Shop::find($id);
    $data = json_decode(json_encode($item), true);
    return $data;
    }
     
    public function deleteitem($id)
    {
         Shop::where('id',$id)->delete();
    return redirect('/shoplist')->with('message','Deleted Sucessfully');

    }
}
