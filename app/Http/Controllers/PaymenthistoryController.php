<?php

namespace App\Http\Controllers;

use App\Paymenthistory;
use Illuminate\Http\Request;

class PaymenthistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paymenthistory  $paymenthistory
     * @return \Illuminate\Http\Response
     */
    public function show(Paymenthistory $paymenthistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paymenthistory  $paymenthistory
     * @return \Illuminate\Http\Response
     */
    public function edit(Paymenthistory $paymenthistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paymenthistory  $paymenthistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paymenthistory $paymenthistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paymenthistory  $paymenthistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paymenthistory $paymenthistory)
    {
        //
    }
}
