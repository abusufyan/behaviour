<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Paymenthistory;    
use App\User;    

class AdminController extends Controller
{ 
      
    public function signin(Request $request){

       $username=$request->username;
        $admin = Admin::where('username', $username)->first();
        if($admin){
           $pwd =$admin->password;
           $adminid =$admin->id;
        if($pwd == $request->password)
        {   
            Session::put('admin',$adminid);
            return redirect('/dashboard');
        }  
        else{
            return redirect('/adminlogin')->with('message','Sorry Invalid Password');
        }
        }
        else{         
        return redirect('/adminlogin')->with('message','Sorry Invalid Username');
            }
       }  
 
      public function index()
      {
        return view('admin.dashboard');
      }

      public function adminprofile()
      {      
        $id = Session::get('admin');
        $data=Admin::find($id);

        return view('admin.editprofile',compact('data'));
      }
      public function adminupdateprofile(Request $request)
      { 
      Admin::where('id',$request->id)->update(['firstName'=>$request->firstName,'lastName'=>$request->lastName,'email'=>$request->email,'about'=>$request->about]);
      return redirect('/adminprofile')->with('message','Profile Updated Sucessfully');
       
      }

    public function changepassword(Request $request){
   
      $id=$request->id; 
      $oldpassword=$request->oldpassword;
      $data=Admin::find($id);
      $password=$data->password;
      if($oldpassword == $password){
      $newpassword=$request->newpassword;
      $confirmpassword=$request->confirmpassword;
       if($newpassword == $confirmpassword){
          Admin::where('id',$id)->update(['password'=>$request->newpassword]);
          return redirect('/adminprofile')->with('message','Password Updated Sucessfully');
       }
       else{
        return redirect('/adminprofile')->with('message','Invalid Password'); 
       }
      
      }
      else{
      return redirect('/adminprofile')->with('message','Password not matched with old password');
      } 

    }

    public function paymenthistory (){
        $payment = Paymenthistory::Join('users', 'users.id', '=', 'paymenthistory.userId')
         ->select('users.*','paymenthistory.*')
        ->get();         
      return view('admin.paymenthistory',compact('payment')); 
     }

     public function manageusers(){
      $free =  User::where('type','free')->get();
      $discount = User::where('type','%age')->get();
      $full =  User::where('type','full')->get();
      
       return view('admin.manageusers',compact('free','discount','full')); 
     }

      public function deleteuser($id){
      User::where('id',$id)->delete();      
      return redirect('/manageusers')->with('message','User Deleted Sucessfully');
      
     }

     public function edituser($id){
      $user = User::find($id);

      $data = json_decode(json_encode($user), true);
      return $data;  
    }
     public function updateuser(Request $request){
        $id = $request->id;

        $user=User::find($id);
        
          $user->type = $request->type;
          $user->active = $request->active;
          $user->save();
          return redirect('/manageusers')->with('message','Updated Sucessfully');
         
    }
   
      public function logout(){
      Session::flush(); // removes all session data
      return redirect('/adminlogin');

      }
 
}
