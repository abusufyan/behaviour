<?php
namespace App\Http\Controllers;
use App\Profile;
use App\Chart;
use App\Character;
use Illuminate\Http\Request;
use App\BackgroundImages;
use App\Avatar;
use App\Rewards;
use Auth;
use Illuminate\Support\Facades\Session;
use DB;
class ChartController extends Controller
{
 
    public function indexchart()
    {
    $reward= Rewards::all();    
    $buddy= Avatar::where('type','free')->get();
    $background= BackgroundImages::where('type','free')->get();
    $profileId = Session::get('profile'); 
    $userId=Auth::user()->id;
    $profile= Chart::where('userId',$userId)->select('id','behaviour')
    ->where('bstatus','!=',1)->get()->unique('behaviour');


   
     $rewards= Chart::where('userId',$userId)->select('id','reward')->get()->unique('reward');
     $static = DB::table('behavior')
     ->select('id','behaviour')
     ->get();    

    

    /*$array = json_decode(json_encode($static), true);
    print_r($array);
    exit();*/


    $newbuddies = DB::table('confirmorder')
        ->join('avatars','avatars.id','=','confirmorder.avatarId')
        ->select('avatars.*')      
        ->where('confirmorder.profileId', $profileId)
        ->get();
    $newbackground = DB::table('confirmorder')
        ->join('background_images','background_images.id','=','confirmorder.backgroundId')
        ->select('background_images.*')      
        ->where('confirmorder.profileId', $profileId)
        ->get();
 

    return view('charts',compact('reward','buddy','background','profile','newbuddies','newbackground','rewards','static'));
    }


    public function insertchart(Request $request){
        $userId=Auth::user()->id;
        $profileId = Session::get('profile');
        $behaviour=$request->behaviour;
        $static = DB::table('behavior')
        ->select('*')
        ->where('behaviour',$behaviour)
        ->first();

        if($static)
        {
        $staticbehaviour = $static->behaviour; 
        }
        else
        {
        $staticbehaviour = null;
        }     
      $result= strcmp($staticbehaviour,$behaviour);
      if($result == 0 )
        {  
        $one = 1;    
        Chart::create([
        'userId' => $userId,
        'profileId' => $profileId,        
        'avatarId' => $request->avatarId,
        'backgroundId' => $request->backgroundId,
        'behaviour' => $request->behaviour,
        'reward' => $request->reward,
        'rewardId' => $request->rewardId,
        'steps' => $request->steps,
        'bstatus' => $one,
        'status' => 0,
         ]);
        Session::put('profileid',$profileId);  
        return redirect('/charthome');          
        }

        else{

        Chart::create([
        'userId' => $userId,
        'profileId' => $profileId,        
        'avatarId' => $request->avatarId,
        'backgroundId' => $request->backgroundId,
        'behaviour' => $request->behaviour,
        'reward' => $request->reward,
        'rewardId' => $request->rewardId,
        'steps' => $request->steps,
        'bstatus' => 0,
        'status' => 0,
        ]);
        Session::put('profileid',$profileId);    
        return redirect('/charthome');         

        }


    } 

    public function indexhome($id)

    {
    Session::put('profile',$id);    
    $profile= Profile::where('id',$id)->first();
    $childname= $profile->childName;
    $characterId= $profile->characterId;
    $character=Character::where('id',$characterId)->first();   
    Session::put('character',$character);
    Session::put('childname',$childname);
    $chartprofile= Chart::where('profileId',$id)->count(); 
    $profileCoin = DB::table('coins')->select('*')->where('profileId', $id)->first();
    if($profileCoin){
    $coins=$profileCoin->coins;
    }else{
    $coins=0;
    }
 
    Session::put('profileExist',$chartprofile);  


    $chart= Chart::where('profileId',$id)->get(); 
    return view('charthome',compact('chart','childname','character','id','coins'));    
   
    }
    public function charthome()
    { 

    $profileid= Session::get('profile');
    $chart= Chart::where('profileId',$profileid)->get();
    $profile= Profile::where('id',$profileid)->first();
    $childname= $profile->childName;
    $characterId= $profile->characterId;     
    $chartprofile= Chart::where('profileId',$profileid)->count();    
    Session::put('profileExist',$chartprofile); 
    $character=Character::where('id',$characterId)->first();  
    $id=$profileid;
    $profileCoin = DB::table('coins')->select('*')->where('profileId', $profileid)->first();
    if($profileCoin){
    $coins=$profileCoin->coins;
    }else{
    $coins=0;
    }
    return view('charthome',compact('chart','childname','character','id','coins'));    
    }
    public function editchart($id){
      
        $chartId=$id;
        $reward= Rewards::all();    
        $buddy= Avatar::all();
        $background= BackgroundImages::all(); 
        $chart=Chart::find($id);
        $backgroundId=$chart->backgroundId;
        $buddy1=$chart->avatarId;
        $steps=$chart->steps;
        $reward2=$chart->rewardId;
  
        $reward1 = Rewards::where('id', $reward2)->first();
        $static = DB::table('behavior')
        ->select('id','behaviour')
        ->get(); 
        $selectedbuddy = Avatar::where('id', $buddy1)->first();
        $background1 = BackgroundImages::where('id', $backgroundId)->first();
        $profileId = Session::get('profile'); 
        $userId=Auth::user()->id;
        $profile= Chart::where('userId',$userId)->select('id','behaviour')
        ->where('bstatus','!=',1)->get()->unique('behaviour');       
        $rewards= Chart::where('userId',$userId)->select('id','reward')->get()->unique('reward');


        $newbuddies = DB::table('confirmorder')
        ->join('avatars','avatars.id','=','confirmorder.avatarId')
        ->select('avatars.*')      
        ->where('confirmorder.profileId', $profileId)
        ->get();
        $newbackground = DB::table('confirmorder')
        ->join('background_images','background_images.id','=','confirmorder.backgroundId')
        ->select('background_images.*')      
        ->where('confirmorder.profileId', $profileId)
        ->get();

        return view('editchart',compact('reward','buddy','background','background1','selectedbuddy','chart','steps','id','chartId','reward1','profile','rewards','static','newbuddies','newbackground'));  
  


    /*$chartid = $id;

    $reward= Rewards::all();    
    $buddy= Avatar::all();
    $background= BackgroundImages::all(); 
    $chart=Chart::find($id);
    $profileId = Session::get('profile'); 
     $rewards= Chart::where('profileId',$profileId)->select('id','reward')->get()->unique('reward');

     $profile= Chart::where('profileId',$profileId)->get();
     return view('editchart',compact('reward','buddy','background','chart','id','profile','chartid','rewards'));*/    

    }
    public function updatechart(Request $request){
        $id=$request->id;
        $chart=Chart::find($id);
        $profileId = Session::get('profile');           

        $behaviour=$request->behaviour;
        $static = DB::table('behavior')
        ->select('*')
        ->where('behaviour',$behaviour)
        ->first();

        if($static){
        $staticbehaviour = $static->behaviour; 
        }
        else{
        $staticbehaviour = null;
        }     
      $result= strcmp($staticbehaviour,$behaviour);
      if($result == 0 )
        {  
             $one = 1;
            if($chart->steps != $request->steps)
            {

            $chart1=Chart::where('id',$id)->update([
            'avatarId' => $request->avatarId,
            'backgroundId' => $request->backgroundId,
            'behaviour' => $request->behaviour,
            'reward' => $request->reward,
            'rewardId' => $request->rewardId,
            'steps' => $request->steps,
            'bstatus' => $one,
            'jumps' => 0,
            'status' => 0
            ]);

            return redirect('/indexhome/'.$profileId);
            }
            else 
            {
            $chart1=Chart::where('id',$id)->update([
            'avatarId' => $request->avatarId,
            'backgroundId' => $request->backgroundId,
            'behaviour' => $request->behaviour,
            'reward' => $request->reward,
            'rewardId' => $request->rewardId,
            'steps' => $request->steps,
            'bstatus' => $one
            ]);

            return redirect('/indexhome/'.$profileId);

            }
                      
        }

        else{

        if($chart->steps != $request->steps)
        {
            $chart1=Chart::where('id',$id)->update([
            'avatarId' => $request->avatarId,
            'backgroundId' => $request->backgroundId,
            'behaviour' => $request->behaviour,
            'reward' => $request->reward,
            'rewardId' => $request->rewardId,
            'steps' => $request->steps,
            'jumps' => 0,
            'status' => 0]);

            return redirect('/indexhome/'.$profileId);

        }
        else{

        $chart1=Chart::where('id',$id)->update([
        'avatarId' => $request->avatarId,
        'backgroundId' => $request->backgroundId,
        'behaviour' => $request->behaviour,
        'reward' => $request->reward,
        'rewardId' => $request->rewardId,
        'steps' => $request->steps
        ]);

        return redirect('/indexhome/'.$profileId);

        }

                 

        }
  
    }
    public function deletechart($id){

    Chart::where('id',$id)->delete();
    return back()->with('message','Chart Deleted Sucessfully');
    }


     
}
