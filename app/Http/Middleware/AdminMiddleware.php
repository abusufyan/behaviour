<?php

namespace App\Http\Middleware;
use App\Admin;
use Illuminate\Support\Facades\Session;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $admin = Session::get('admin');
      
      if($admin){
        return $next($request);

      }
      else{
        return redirect('adminlogin');
      }
    }
}
