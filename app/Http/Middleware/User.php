<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::User()->active == 0 || Auth::User()->active == 1 )
        {
            return $next($request);
        }
       else
        {
        return redirect('/approval');
        }
    }
}
