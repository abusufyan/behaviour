<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TryForFree extends Model
{
    //


    	protected $table = 'tryforfree';
        
        protected $fillable = [
         'title', 'buddyimage','backgroundimage','steps',
    ];
}
