<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    //
protected $table = 'coupons';

        	protected $fillable = [
		'title', 'description','price','type','code','for','status','startDate','endDate',
		];

}
