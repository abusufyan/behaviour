<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeResources extends Model
{
    protected $fillable = [
		'status', 'file','category','title','description',
		];
}
