<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundImages extends Model
{
      
    	protected $table = 'background_images';
        
        protected $fillable = [
         'backgroundName', 'backgroundImage','podImage','type','category','price',
    ];
  }
