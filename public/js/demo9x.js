resume = jumps;
count = jumps;
PodsCondition = 10;
ScrollToRight = 180;
AvatarButtonMovement = 200;
ActionTime = 1000;
AnimationTime = 2000;
marginBottomEven = "30%";
marginBottomOdd = "30%";
paddingBottomEven = "10%";
paddingBottomOdd = "45%";
//alert(resume);
function retCount(c) {
    return c;
}

function fireWorksAnimation() {
    var audio2 = document.getElementById("myAudio2");
    var Bg = document.getElementById("BG");

    audio2.play();
    $("#BG").fireworks();

    window.setTimeout(function () {
        window.location.href =
            "https://behaviourbuddy.facebhoook.com/certificate/"+chartid;
    }, 8000);
}

function fireWorksAnimation2() {
    var audio2 = document.getElementById("myAudio2-sm");
    var Bg = document.getElementById("sm-BG");

    audio2.play();
    $("#sm-BG").fireworks();

    window.setTimeout(function () {
        window.location.href =
            "https://behaviourbuddy.facebhoook.com/certificate/"+chartid;
    }, 15000);
}

function onJump() {
    //console.log("jumps: " + jumps);

    var a = count;
    if (count >= 0 && count < PodsCondition) {
        // var a = count += 1;
        a = parseInt(a) + parseInt(1);
        console.log("Jump:", a);
        count = a;
    }

    document.getElementById("count").value = retCount(a); //Now you get the js variable inside your form element

    // alert(a);
    console.log(a);
    var elem = document.getElementById("foxID");
    var elem3 = document.getElementById("next");
    // var ButtonDiv = document.getElementById("btnDiv");
    var audio = document.getElementById("myAudio");
    // var Avatar = document.getElementById("avatar");

    elem3.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
        ],
        {
            duration: 100,
        }
    );

    if (a <= PodsCondition) {
        console.log("IN 7");
        if (a % 2 == 0) {
            console.log("EVEN");
            audio.play();

            elem.animate(
                [
                    // keyframes

                    {
                        top: "-5%",
                    },
                    {
                        top: "-50%",
                    },
                    {
                        left: a * 100 + "%",
                    },
                    {
                        top: "10%",
                    },
                    {
                        top: "5%",
                    },
                ],
                {
                    // timing options
                    duration: AnimationTime,
                    //   iterations: 2,
                }
            );
            setTimeout(function () {
                var elem2 = document.getElementById("tokenRotate" + a);
                console.log("RIGHT MOVE:", a);
                elem.style.left = a * 100 + "%";

                //   if (a!==0) {
                //   ButtonDiv.style.marginLeft=a * "250" +"px";
                //   }else{
                //     ButtonDiv.style.marginLeft="250" +"px";
                //   }
                // ButtonDiv.style.marginLeft = a * AvatarButtonMovement + "px";
                // Avatar.style.marginLeft = a * AvatarButtonMovement + "px";

                // elem.style.paddingBottom = "-25%";
                elem.style.marginBottom = marginBottomEven;

                elem2.style.visibility = "hidden";
                //   var container = document.getElementById('BG');
                //   sideScroll(container,'right',130,50,80);
                //   }, 1000);
                window.scrollBy(ScrollToRight, 0);
            }, ActionTime);
            if (a == PodsCondition) {
                console.log("FIREWORKS");
                fireWorksAnimation();
            }
        } else if (a % 2 != 0) {
            audio.play();
            elem.animate(
                [
                    {
                        top: "-5%",
                    },
                    {
                        top: "-50%",
                    },
                    {
                        left: a * 100 + "%",
                    },
                    {
                        top: "10%",
                    },
                    {
                        top: "5%",
                    },
                ],
                {
                    duration: AnimationTime,
                }
            );
            setTimeout(function () {
                elem.style.left = a * 100 + "%";
                var elem2 = document.getElementById("tokenRotate" + a);
                // ButtonDiv.style.marginLeft = a * AvatarButtonMovement + "px";
                // Avatar.style.marginLeft = a * AvatarButtonMovement + "px";
                elem.style.paddingBottom = marginBottomOdd;
                elem2.style.visibility = "hidden";
                window.scrollBy(ScrollToRight, 0);
            }, ActionTime);
            // window.scrollBy(300, 0);
            if (a == PodsCondition) {
                console.log("FIREWORKS");
                fireWorksAnimation();
            }
        }
    }
}

function onJumpBack() {
    if (count > 0) {
        var a = (count -= 1);
        document.getElementById("count").value = retCount(a); //Now you get the js variable inside your form element
    }

    console.log("JUMPBACK", a);
    var elem = document.getElementById("foxID");
    //         var elem2 = document.getElementById("tokenRotate");
    var elem3 = document.getElementById("back");
    // var ButtonDiv = document.getElementById("btnDiv");
    var audio = document.getElementById("myAudio");
    // var Avatar = document.getElementById("avatar");

    elem3.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
        ],
        {
            duration: 100,
        }
    );
    var token = a + 1;
    if (a < PodsCondition && a >= 0) {
        if (a >= 0) {
            elem.animate(
                [
                    {
                        transform: "scale(1)",
                    },
                    {
                        transform: "scale(0.66)",
                    },
                    {
                        transform: "scale(0.33)",
                    },
                ],
                {
                    duration: 800,
                }
            );
            setTimeout(function () {
                var elem2 = document.getElementById("tokenRotate" + token);
                elem.style.left = a * 100 + "%";
                console.log("A:", a);
                if (a % 2 === 0) {
                    elem.style.paddingBottom = "20%";
                    console.log("IF");
                } else {
                    elem.style.paddingBottom = "45%";
                    console.log("ELSE");
                }
                elem2.style.visibility = "visible";
                // ButtonDiv.style.marginLeft = a * "-250" + "px";
                // Avatar.style.marginLeft = a * "-250" + "px";
                window.scrollBy(-ScrollToRight, 0);

                //   }, 1000);
            }, 800);
        }
    } else {
        alert("UNDEFINED ERROR");
    }
}

function onJump2() {
    var a = count;

    if (count >= 0 && count < PodsCondition) {
        // var a = count += 1;
        a = parseInt(a) + parseInt(1);
        console.log("Jump:", a);
        count = a;
    }

    // document.getElementById("count-sm").value = retCount(a); //Now you get the js variable inside your form element

    // alert(a);
    console.log(a);
    var elem = document.getElementById("foxID-sm");
    var elem3 = document.getElementById("next-sm");
    // var ButtonDiv = document.getElementById("btnDiv");
    var audio = document.getElementById("myAudio-sm");
    // var Avatar = document.getElementById("avatar");

    elem3.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
        ],
        {
            duration: 100,
        }
    );

    if (a <= PodsCondition) {
        console.log("IN 7");
        if (a % 2 == 0) {
            console.log("EVEN");
            audio.play();

            elem.animate(
                [
                    // keyframes

                    {
                        top: "-5%",
                    },
                    {
                        top: "-50%",
                    },
                    {
                        left: a * 100 + "%",
                    },
                    {
                        top: "10%",
                    },
                    {
                        top: "5%",
                    },
                ],
                {
                    // timing options
                    duration: AnimationTime,
                    //   iterations: 2,
                }
            );
            setTimeout(function () {
                var elem2 = document.getElementById("sm-tokenRotate" + a);
                console.log("RIGHT MOVE:", a);
                elem.style.left = a * 100 + "%";

                //   if (a!==0) {
                //   ButtonDiv.style.marginLeft=a * "250" +"px";
                //   }else{
                //     ButtonDiv.style.marginLeft="250" +"px";
                //   }
                // ButtonDiv.style.marginLeft = a * AvatarButtonMovement + "px";
                // Avatar.style.marginLeft = a * AvatarButtonMovement + "px";

                // elem.style.paddingBottom = "-25%";
                elem.style.marginBottom = marginBottomEven;

                elem2.style.visibility = "hidden";
                //   var container = document.getElementById('BG');
                //   sideScroll(container,'right',130,50,80);
                //   }, 1000);
                window.scrollBy(ScrollToRight, 0);
            }, ActionTime);
            if (a == PodsCondition) {
                console.log("FIREWORKS");
                fireWorksAnimation2();
            }
        } else if (a % 2 != 0) {
            audio.play();
            elem.animate(
                [
                    {
                        top: "-5%",
                    },
                    {
                        top: "-50%",
                    },
                    {
                        left: a * 100 + "%",
                    },
                    {
                        top: "10%",
                    },
                    {
                        top: "5%",
                    },
                ],
                {
                    duration: AnimationTime,
                }
            );
            setTimeout(function () {
                elem.style.left = a * 100 + "%";
                var elem2 = document.getElementById("sm-tokenRotate" + a);
                // ButtonDiv.style.marginLeft = a * AvatarButtonMovement + "px";
                // Avatar.style.marginLeft = a * AvatarButtonMovement + "px";
                // marginBottomOdd = marginBottomOdd + "15%";
                elem.style.paddingBottom = marginBottomOdd;
                elem2.style.visibility = "hidden";
                window.scrollBy(ScrollToRight, 0);
            }, ActionTime);
            if (a == PodsCondition) {
                console.log("FIREWORKS");
                fireWorksAnimation2();
            }
            // window.scrollBy(300, 0);
        }
    }
}

function onJumpBack2() {
    if (count > 0) {
        var a = (count -= 1);
        document.getElementById("count-sm").value = retCount(a); //Now you get the js variable inside your form element
    }

    console.log("JUMPBACK", a);
    var elem = document.getElementById("foxID-sm");
    //         var elem2 = document.getElementById("tokenRotate");
    var elem3 = document.getElementById("back-sm");
    // var ButtonDiv = document.getElementById("btnDiv");
    var audio = document.getElementById("myAudio-sm");
    // var Avatar = document.getElementById("avatar");

    elem3.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
        ],
        {
            duration: 100,
        }
    );
    var token = a + 1;
    if (a < PodsCondition && a >= 0) {
        if (a >= 0) {
            elem.animate(
                [
                    {
                        transform: "scale(1)",
                    },
                    {
                        transform: "scale(0.66)",
                    },
                    {
                        transform: "scale(0.33)",
                    },
                ],
                {
                    duration: 800,
                }
            );
            setTimeout(function () {
                var elem2 = document.getElementById("sm-tokenRotate" + token);
                elem.style.left = a * 100 + "%";
                console.log("A:", a);
                if (a % 2 === 0) {
                    elem.style.paddingBottom = "20%";
                    console.log("IF");
                } else {
                    elem.style.paddingBottom = "45%";
                    console.log("ELSE");
                }
                elem2.style.visibility = "visible";
                // ButtonDiv.style.marginLeft = a * "-250" + "px";
                // Avatar.style.marginLeft = a * "-250" + "px";
                window.scrollBy(-ScrollToRight, 0);

                //   }, 1000);
            }, 800);
        }
    } else {
        alert("UNDEFINED ERROR");
    }
}

function resumeGame() {
    console.log("Pak");
    // document.getElementById("count").value = retCount(count); //Now you get the js variable inside your form element
    // document.getElementById("count-sm").value = retCount(count); //Now you get the js variable inside your form element

    var ResumePos = resume;
    var tokenHide = null;
    var tokenHide2 = null;
    console.log("RESUME POS", ResumePos);
    for (let i = 1; i <= ResumePos; i++) {
        // console.log("Loop:", i);
        tokenHide = document.getElementById("tokenRotate" + i);
        tokenHide2 = document.getElementById("sm-tokenRotate" + i);

        tokenHide.style.visibility = "hidden";
        tokenHide2.style.visibility = "hidden";
        window.scrollBy(i * ScrollToRight, 0);
    }
    var elem = document.getElementById("foxID");
    var elem2 = document.getElementById("foxID-sm");

    elem.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
            {
                transform: "scale(0.33)",
            },
        ],
        {
            duration: 100,
        }
    );

    elem2.animate(
        [
            {
                transform: "scale(1)",
            },
            {
                transform: "scale(0.66)",
            },
            {
                transform: "scale(0.33)",
            },
        ],
        {
            duration: 100,
        }
    );

    setTimeout(function () {
        elem.style.left = resume * 100 + "%";
        elem2.style.left = resume * 100 + "%";

        if (ResumePos % 2 === 0) {
            elem.style.paddingBottom = "20%";
            elem2.style.paddingBottom = "20%";
            console.log("IF");
        } else {
            elem.style.paddingBottom = "45%";
            elem2.style.paddingBottom = "45%";
            console.log("ELSE");
        }
    }, 100);
    $("#modal").hide();
}
