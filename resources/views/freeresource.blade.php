  @include('layouts.header')
@section('content')

@endsection
<div class="container-fluid">
 
<div class="mt-5">
<center>
	<div class="col-lg-8 col-md-12 col-sm-12">
		<div class="text-center mt-5">
		<a href="{{url('list/'.'coloringsheets')}}"><h3 class="font2 font">Coloring Sheets</h3></a>
		<span class="popfont spanfont5 font2  pl-4 pr-4 d-flex justify-content-center">
		Buddy Coloring sheets can be used as a fun visual reminder of the goals each child is working towards.
	</span>
		</div>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 mt-5">
	<a href="{{url('list/'.'coloringsheets')}}">
	<img src="public/images/free resources/buddy coloring at desk.png" class="img-fluid w-50 h-50" alt="Cinque Terre">
	</a>
	</div>

<hr class="m-0">


    	<div class="col-lg-8 col-md-12 col-sm-12">
		<div class="text-center mt-5">
			<a href="{{url('list/'.'specialbuddytokens')}}">
				<h3 class="font font2">Special Buddy Tokens</h3>
			</a>
		<span class="popfont spanfont5 font2  pl-4 pr-4 d-flex justify-content-center">
			Any adult working with a child at school can award these special Buddy Tokens.   These Buddy Tokens let a student’s family know that they did well at school.  If the family is using My Behavior Buddy at home, these tokens also give their child a FREE jump, bringing them one step closer to their HOME reward. 
			  </span>
		</div>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12">
	<a href="{{url('list/'.'specialbuddytokens')}}"><img src="public/images/free resources/buddy token sheet thumbnail image.png" class="img-fluid w-50 h-50" alt="Cinque Terre"></a>
	</div>

	<hr class="m-0">


    <div class="col-lg-8 col-md-12 col-sm-12">
		<div class="text-center mt-5">
		<a href="{{url('list/'.'buddybucks')}}">
			<h3 class="font2 font">Buddy Bucks</h3>
		</a>
		<span class="popfont spanfont5 font2  pl-4 pr-4 d-flex justify-content-center">
		There are many fun and creative ways teachers can use Buddy Bucks to supplement our reward charts.  For example, a student  can have a FREE jump after earning 5 Buddy Bucks!</span>
		</div>
	</div>
	

	<div class=" mt-5 col-lg-8 col-md-12 col-sm-12">
	<a href="{{url('list/'.'buddybucks')}}"><img src="public/images/free resources/buddy buck.png" class="img-fluid w-75 h-75" alt="Cinque Terre"></a>
	</div>
	
</center>
</div>

</div>

@include('layouts.footer')
@section('content')
@endsection
