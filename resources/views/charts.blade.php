@include('layouts.head')
<body>
<nav class="navbar navbar-expand-sm navbar-light">
<a class="navbar-brand" href="#"><img src="{{ asset('public/images/alien in spaceship.jpg') }}" alt="logo" class="blogo"></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link font" href="{{url('/accounthome')}}"><h4 class="fontsize"><b>Home</b></h4></a>
</li>
</ul>
<ul class="navbar-nav m-auto">
<?php 
$childname = Session::get('childname');
$character = Session::get('character');
$id = Session::get('profile');
$profileExist = Session::get('profileExist');
?>
 
<li class="nav-item">
<a class="nav-link font" href="#"><span class="fontsize"><b>Create a Chart</b> </span></a>
 
</ul>
<ul class="navbar-nav">
<li class="nav-item dropdown mt-3">
<a class="nav-link hname hrd2 text-white dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >{{$childname}}</a>
<div class="dropdown-menu">
<?php 
if(Auth::user()->active == 0 && $profileExist == 0){?>
<a class="dropdown-item" href="{{url('/chart')}}">Add Chart</a>
<?php } if(Auth::user()->active == 0 && $profileExist == 1) {?>
<a class="dropdown-item" data-toggle="modal" data-target="#premium">Add Chart</a>
<?php }if(Auth::user()->active == 1 ) {?>
<a class="dropdown-item" href="{{url('/chart')}}">Add Chart</a>
<?php }
?>  
<a class="dropdown-item" href="{{url('/editchildprofile/'.$id)}}">Edit Profile</a>
<a class="dropdown-item" href="{{url('/accounthome')}}">Switch Profile</a>
<a class="dropdown-item" href="{{url('/deleteprofile/'.$id)}}" onclick="return confirm('Are you sure you want to delete this ?')">Delete Profile</a>
</div>
</li>
<li class="nav-item mr-2">

<img src="{{url('public/character/'.$character->characterImage)}}" alt="logo" class="hlogo" >
</li> 
</ul>
</div>
</nav>

<hr class="hrd1">
<div class="modal fade" id="premium">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body ">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </div>
            <div class="row mt-5">
              <center><h4 class="ml-5 text-danger">Try Premium Account</h4></center>
              
            </div>
          <div class="m-5" >
           <span class="popfont mt-5">Please try premium account to add multiple profile's</span>
           </div>
          <div class="m-5">
          <a href="{{url('pricing')}}" title="" class="regbtn1 h5 mt-5">Upgrade to Premium</a>
            
          </div>
          </div>

        </div>
      </div>
    </div>
@yield('content')

<script>
function myFunction() {
alert("fdsfas")
document.getElementById("demo").innerHTML = "Hello World";
}
</script>
<body>

  <div class="container-fluid">
    
<link rel="stylesheet" type="text/css" href="public/css/tabmenu.css">

<div class="container-fluid mt-3">
  <!-- Tab menu start -->

    <div class="row">
      <div class="col-lg-1 col-md-0 col-sm-0"></div>
      <!-- Tab screen -->
      <div class="col-lg-8 col-md-10 col-sm-10">
  <form action="{{url('/insertchart')}}" method="Post" accept-charset="utf-8">
    {{ csrf_field() }}

        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

         <!--  Book Image tab menu -->
         <div class="tab-pane fade show active" id="nav-Owl" role="tabpanel" aria-labelledby="nav-Owl-tab">

          <div class="row mt-4">
            <img src="public/images/buddy book.png" alt="" class="img-fluid">
          </div>
        </div>
        <!-- Budddy Tab menu -->
        <div class="tab-pane fade show fade" id="nav-buddies" role="tabpanel" aria-labelledby="nav-buddies-tab">


          <div class="container-fluid ">
            <div class="containerabc">
              <img class="circle-left img-fluid" src="public/images/Buddybookopened.png" />
              <div class="circle-right l11">
            <div class="row m-2">
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <button type="button" class="regbtn3" id="nav-Owl-tab" data-toggle="tab" href="#nav-Owl" role="tab" aria-controls="nav-Owl" value="Back" style="display: none;" aria-selected="false">Back</button>
             </div>
             <div class="col-lg-8 col-md-6 col-sm-6">
                    <h4 class="font pt-2 text-white"><b>Select a Buddy</b></h4>
             </div>
             <div class="nav col-lg-2 col-md-3 col-sm-3">
                 <button type="button" class="regbtn3 tablinks tablinks"  id="nav-theme-tab" data-toggle="tab" href="#nav-theme" role="tab" aria-controls="nav-theme" value="Next" aria-selected="true" onclick="openCity(event, 'theme')">Next</button>
               </div>  
                  </div>   
                  <div class="ex2 budy1 mt-3 mb-3">

                    <div class="row p-4 ">

                     @foreach($buddy as $row)
                     <div class="col-lg-4 col-md-4 col-sm-4 product-chooser">
                      <div class=" product-chooser-item row-cols-9">
                        <center>
                          <div class="addprof text-center checkimg1">
                           <img src="{{url('public/avatar/'.$row->avatarImage)}}" class="addimg2  img-responsive"  id="addimg" data-img-src="{{url('public/avatar/'.$row->avatarImage)}}" value="{{$row->id}}" name="avatarId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
                           <input type="radio" name="avatarId" value="{{$row->id}}" checked="checked">
                         </div>
                       </center>
                     </div>
                   </div>
                   @endforeach
                   
                   
                    @foreach($newbuddies as $row)
                     <div class="col-lg-4 col-md-4 col-sm-4 product-chooser">
                      <div class=" product-chooser-item row-cols-9">
                        <center>
                          <div class="addprof text-center checkimg1">
                           <img src="{{url('public/avatar/'.$row->avatarImage)}}" class="addimg2  img-responsive"  id="addimg" data-img-src="{{url('public/avatar/'.$row->avatarImage)}}" value="{{$row->id}}" name="avatarId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
                           <input type="radio" name="avatarId" value="{{$row->id}}" checked="checked">
                         </div>
                       </center>
                     </div>
                   </div>
                   @endforeach
                </div>
             </div>  
               
           </div>
         </div>
       </div>
     </div>


     <div class="tab-pane fade show fade" id="nav-theme" role="tabpanel" aria-labelledby="nav-theme-tab">
       <div class="container-fluid">

        <div class="containerabc">
          <img class="circle-left img-fluid" src="public/images/Buddybookopened.png" />
          <div class="circle-right l22">
           <div class="row m-2">
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <input type="button" class="regbtn3 tablinks"  onclick="openCity(event, 'buddies')" id="nav-buddies-tab" data-toggle="tab" href="#nav-buddies" role="tab" aria-controls="nav-buddies" value="Back" aria-selected="false">
             </div>
             <div class="col-lg-8 col-md-6 col-sm-6">
                <h4 class="font pt-2 text-white"><b>Select a Background</b></h4>
             </div>
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <input type="button" class="regbtn3 tablinks"  id="nav-behaviors-tab" data-toggle="tab" href="#nav-behaviors" role="tab" aria-controls="nav-behaviors" value="Next"  onclick="openCity(event, 'behaviors')" aria-selected="false">
               </div>  
                  </div>   
              <div class="ex2 budy1 mt-3 mb-3">
                <div class="row p-4 ">
                 @foreach($background as $row)
                 <div class="col-lg-6 col-md-6 col-sm-6 product-chooser">
                  <div class=" product-chooser-item">
                    <center>
                      <div class="addprof text-center checkimg1">
                       <img src="{{url('public/background/'.$row->backgroundImage)}}" class="addimg4"  id="addimg" data-img-src="{{url('public/background/'.$row->backgroundImage)}}" value="{{$row->id}}" name="backgroundId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
                       <input type="radio" name="backgroundId" value="{{$row->id}}" checked="checked">
                     </div>
                   </center>
                 </div>
               </div>
               @endforeach
           </div>
         </div>  
    
       </div>
     </div>
   </div>
 </div>

 <div class="tab-pane fade show fade" id="nav-behaviors" role="tabpanel" aria-labelledby="nav-behaviors-tab">

  <div class="container-fluid">
    <div class="containerabc">
      <img class="circle-left img-fluid" src="public/images/Buddybookopened.png" />
      <div class="circle-right l33">
                   <div class="row m-2">
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <input type="button" class="regbtn3 tablinks"  onclick="openCity(event, 'theme')"   id="nav-theme-tab" data-toggle="tab" href="#nav-theme" role="tab" aria-controls="nav-theme" value="Back" aria-selected="false">
             </div>
             <div class="col-lg-8 col-md-6 col-sm-6">
                <h4 class="font pt-2 text-white"><b>Select a Behavior</b></h4>
             </div>
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <input type="button" class="regbtn3 tablinks"  id="nav-reward-tab"  onclick="openCity(event, 'reward')"  data-toggle="tab" href="#nav-reward" role="tab" aria-controls="nav-reward" value="Next" aria-selected="false">
               </div>  
             </div>  
        <div class="row pl-4 pt-2 pr-4">
          <input list="browsers" id="browser" placeholder="Enter your own goal behavior or select from the options below" name="behaviour"  class="iptx2" required>
             
        </div> 

        <div class="ex8 col-12 mt-3 budy1 mb-1">
               @foreach($profile as $row)
           <div class="row pl-5 border-1">
            &nbsp;<span id="{{$row->id}}" class="popfont spanfont6" onClick="getData({{$row->id}})" > {{$row->behaviour}}</span>
          </div>
          @endforeach

           @foreach($static as $row)
          <div class="row pl-5 border-1">
           &nbsp; <span id="{{$row->id}}" onClick="getStaticData({{$row->id}})" class="popfont spanfont6">  {{$row->behaviour}}</span>

          </div>
          @endforeach
          
     
        </div> 
            <div class="row m-2">
             <div class="col-6">
             </div>
             <div class="col-6">
               </div>  
           </div>      
      </div>
    </div>
  </div>
</div>
<div class="tab-pane fade show fade" id="nav-reward" role="tabpanel" aria-labelledby="nav-reward-tab">

  <div class="container-fluid">


   <div class="containerabc">
    <img class="circle-left img-fluid" src="public/images/Buddybookopened.png" />
    <div class="circle-right l44">
    
           <div class="row m-2">
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <button type="button" class="regbtn3 tablinks"  onclick="openCity(event, 'behaviors')" id="nav-behaviors-tab" data-toggle="tab" href="#nav-behaviors" role="tab" aria-controls="nav-behaviors" aria-selected="false">Back</button>
             </div>
             <div class="col-lg-8 col-md-6 col-sm-6">
                <h4 class="font pt-2 text-primary" style="font-size: 20px;"><b>Select a Reward and photo</b></h4>
             </div>
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <button type="btn" class="regbtn3 tablinks"  onclick="openCity(event, 'steps')" id="nav-steps-tab" data-toggle="tab" href="#nav-steps" role="tab" aria-controls="nav-steps" value="Next" aria-selected="false">Next</button>
               </div>  
             </div>   
      <div class="row pl-4 pr-4 pt-2">
        <input list="browsers2" id="browser2" name="reward" placeholder="enter a reward" class="iptx2" required>
      </div> 
      <div class="ex0 budy1 mt-2 mb-3">
        <!-- <div class="row pl-4 border-1"> -->
         @foreach($rewards as $row)
           <div class="row pl-5 border-1">

           &nbsp;<span class="popfont spanfont6 spanNew {{$row->id}}" id="{{$row->id}}" value="{{$row->reward}}" name="{{$row->id}}" onClick="getDataReward({{$row->id}})">{{$row->reward}}</span>
          </div>
          @endforeach
        <!-- </div> -->
         
      </div>
      <div class="ex0 w-75 budy1 mb-2" style="margin-left: 12%;">
       <div class=" row budy1 p-2">
        
        @foreach($reward as $row)
          <div class="product-chooser col-3">
            <div class=" product-chooser-item">
                       <img src="{{url('public/reward/'.$row->rewardImage)}}" class="addimg4 contextmenu"  id="addimg" data-img-src="{{url('public/reward/'.$row->rewardImage)}}" value="{{$row->id}}" name="rewardId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
                       <input type="radio" name="rewardId" value="{{$row->id}}" checked="checked">
                     </div>
          </div>
        @endforeach
        
      </div>
    </div>
  </div>
</div>
</div>    
</div>

<div class="tab-pane fade show fade" id="nav-steps" role="tabpanel" aria-labelledby="nav-steps-tab">

  <div class="container-fluid">

   <div class="containerabc">
    <img class="circle-left img-fluid " src="public/images/Buddybookopened.png" / >
    <div class="circle-right l55">
          <div class="row m-2">
             <div class="col-lg-2 col-md-3 col-sm-3">
                 <button type="button" class="regbtn3 tablinks"  onclick="openCity(event, 'reward')"  id="nav-reward-tab" data-toggle="tab" href="#nav-reward" role="tab" aria-controls="nav-reward" aria-selected="false">Back</button>
             </div>
             <div class="col-lg-10 col-md-9 col-sm-9">
        <h4 class=" text-white font pt-2"  style="font-size: 20px;"><b>Select a number of steps to reward</b></h4>
             </div>
           </div>
      <div class="ex7 budy1 m-1"> 
        <div class="row mt-5 d-flex justify-content-center">
          <img src="public/images/pod create a chart tab.png" alt="" class="col-5 img-fluid">
        </div>
        <div class="row mt-1 d-flex justify-content-center">
          <div class="product-chooser col-6">
                 <div class=" product-chooser-item">
                      <div class="text-center checkimg1">
                       <img src="public/images/5 steps button.png" class="img-fluid addprof2 col-4"  id="addimg" data-img-src="public/images/5 steps button.png" value="5" name="steps" data-id="5" data-img-class="first" alt="plus">
                       <input type="radio" name="steps" value="5" checked="checked">
                     </div>
      
          </div>
        </div>

        <div class="product-chooser col-6">
                 <div class=" product-chooser-item">
                      <div class="text-center checkimg1">
                       <img src="public/images/7 steps button.png" class="img-fluid addprof2 col-4"  id="addimg" data-img-src="public/images/7 steps button.png" value="7" name="steps" data-id="7" data-img-class="first" alt="plus">
                       <input type="radio" name="steps" value="7" checked="checked">
                     </div>
      
          </div>
        </div>
     
  <div class="product-chooser col-4 mt-2">
                 <div class=" product-chooser-item">
                      <div class="text-center checkimg1">
                       <img src="public/images/10 steps button.png" class="img-fluid addprof2 col-6"  id="addimg" data-img-src="public/images/10 steps button.png" value="10" name="steps" data-id="10" data-img-class="first" alt="plus">
                       <input type="radio" name="steps" value="10" checked="checked">
                     </div>
      
          </div>
        </div>    

  <div class="product-chooser col-4 mt-2">
                 <div class=" product-chooser-item">
                      <div class="text-center checkimg1">
                       <img src="public/images/15 steps button.png" class="img-fluid addprof2 col-6"  id="addimg" data-img-src="public/images/15 steps button.png" value="15" name="steps" data-id="15" data-img-class="first" alt="plus">
                       <input type="radio" name="steps" value="15" checked="checked">
                     </div>
      
          </div>
        </div>

  <div class="product-chooser col-4 mt-2">
                 <div class=" product-chooser-item">
                      <div class="text-center checkimg1">
                       <img src="public/images/20 steps button.png" class="img-fluid addprof2 col-6"  id="addimg" data-img-src="public/images/20 steps button.png" value="20" name="steps" data-id="20" data-img-class="first" alt="plus">
                       <input type="radio" name="steps" value="20" checked="checked">
                     </div>
      
          </div>
        </div>
 <div class="row mt-5">
    <div class="col-12">
    <input type="submit" class="regbtn3" value="submit">
   </div>
    </div> 

        </div>

      </div>
     

    </div>    
  </div>
</div>


</div>

</div>
</form>
</div>
<div class="tab122 col-2 over" >
  <nav>
    <div class="nav nav-tabs nav-fill col-lg-8 col-md-11" id="nav-tab" role="tablist">
      <a class="nav-item nav-link tab_css active" id="nav-Owl-tab" data-toggle="tab" style="display: none;" href="#nav-Owl" role="tab" aria-controls="nav-Owl" aria-selected="true">
        <img src="public/images/Owl.png" alt="logo" class="img-fluid ab2" >
      </a>

      <a class="nav-item tab_css nav-link l11 ab3 mt-5" id="nav-buddies-tab" data-toggle="tab" href="#nav-buddies" role="tab" aria-controls="nav-buddies" aria-selected="false">
      <div class="l11 tab_css1">
        <img src="public/images/buddies-icon.png" alt="logo" class="img-fluid ab2">
      </div>
      </a>

      <a class="nav-item nav-link l22 tab_css ab3" id="nav-theme-tab" data-toggle="tab" href="#nav-theme" role="tab" aria-controls="nav-theme" aria-selected="false">
      <div class="l22 tab_css1">
        <img src="public/images/theme-icon.png" alt="logo" class="img-fluid ab2">
      </div>  
      </a>

      <a class="nav-item tab_css l33 nav-link ab3" id="nav-behaviors-tab" data-toggle="tab" href="#nav-behaviors" role="tab" aria-controls="nav-behaviors" aria-selected="false">
      <div class="l33 tab_css1">
        <img src="public/images/behaviors-icon.png" alt="logo" class="img-fluid ab2">
    </div>
      </a>

      <a class="nav-item nav-link l44 tab_css ab3" id="nav-reward-tab" data-toggle="tab" href="#nav-reward" role="tab" aria-controls="nav-reward" aria-selected="false">

      <div class="l44 tab_css1">
        <img src="public/images/reward-icon.png" alt="logo" class="img-fluid ab2">
</div>
      </a>
      <a class="nav-item tab_css l55 nav-link ab3" id="nav-steps-tab" data-toggle="tab" href="#nav-steps" role="tab" aria-controls="nav-steps" aria-selected="false">

      <div class="l55 tab_css1">
        <img src="public/images/steps-icon.png" alt="logo" class="img-fluid ab2">
</div>
      </a>

    </div>
  </nav>
</div>

</div>



</form>




</div>


</div>
</body>

<!-- Script -->

<script>

function getData(id) {

  var span_Text = document.getElementById(id).innerText;
  console.log("Data:",span_Text)
  document.getElementById('browser').value=span_Text


}

function getDataReward(id) {
  // document.getElementsByName("fname")[0].value
  // alert(id)
  // document.getElementsByClassName("new")[0].innerHTML
var span_Text = document.getElementsByClassName(id)[0].innerHTML;
console.log("Data:",span_Text)
document.getElementById('browser2').value=span_Text


}
function getStaticData(id) {
var span_Text = id.innerHTML;
console.log("Data:",id.innerHTML)
document.getElementById('browser').value=span_Text


}
  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>

<script>
  $(function(){
    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
      $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
      $(this).addClass('selected');
      $(this).find('input[type="radio"]').prop("checked", true);

      console.log(document.getElementsByClassName("selected"));
    });
  });
</script>

<script>
  
$(".tab122").tabs();
$(".nexttab").click(function() {
    var selected = $(".tab122").tabs("option", "selected");
    $(".tab122").tabs("option", "selected", selected + 1);
});

</script>




<style type="text/css" media="screen">
  .over
  {
    margin-left: -30px !important;
  }

  /* Image Code */

  .containerabc {
  }
  .circle-left{
    position: absolute;
    width: 100%;
    height: 100%;

  } .circle-left, .circle-right {
    position: absolute;
  }
  img {
    padding: 0;
  }
  .circle-right {
    z-index:1;
    margin: 2.5% 3% 15% 17%;
    padding: 1%;
    text-align: center;
    width: 77%;
    height: 95.2%;
    border-radius: 5px;
    object-fit: cover;
    overflow: hidden;
  }

  .ab3{
    -webkit-transform:rotate(270deg);
    -moz-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    transform: rotate(270deg);
  }
  .ab2{
    -webkit-transform:rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
  }

</style>

</body>
</html> 

<!-- <script>
function myFunction() {
var x = document.getElementById("nav-theme");
var y = document.getElementById("nav-Owl");
if (x.style.display === "block") {
x.style.display = "none";
y.style.display = "block";
} else if (y.style.display === "block") {
y.style.display = "none";
x.style.display = "block";
}
}
</script> -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</body>
</html>
