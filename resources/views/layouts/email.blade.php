@include('layouts.head')

  <center style="margin-top: 20px;">
	<div class="container" style="width: 300px; height: 300px; border: solid 1px #caffff; padding: 10px;">
	<div class="row">
			<img src="{{ asset('public/images/buddy logo transparent background with rectangle border.png')}}" alt="" style="margin-left: auto; margin-right: auto; width: 100px; height: 100px;">
	</div>
	<div class="row">
			<h5 style="margin-left: auto; margin-right: auto; display: block; color: #D3D3D3;">Behaviour World</h5>
	</div>
	<div class="row">
	<h3>Email : {{$email_data['email']}}</h3>
	<p class="popfont">
	Thank you for registering with My Behavior Buddy!  Your account has been successfully created.  If you would like to log directly into your account, please log in at <a href="{{url('/')}}">Login</a>.   Once you are logged in, you will be taken directly to your account homepage, where you can immediately begin creating reward charts.  
	</p>
	<p class="popfont">
	If you have any questions, please let us know!
	</p>
	<p class="popfont">
	Regards,
	</p>
	<p class="popfont">			
	Team My Behavior Buddy
	</p>
	<div class="popfont">			
	<a href="https://www.facebook.com/mybehaviorbuddy/" style="" class="btn"><i class="fa fa-facebook-square"></i></a>
	<a href="https://instagram.com/mybehaviorbuddy" style="" class="btn"><i class="fa fa-instagram-square"></i></a>

	<a href="Hello@mybehaviorbuddy.com" style="" class="btn"><i class="fa fa-google-square"></i></a>
	</div>
	</div>
	<div class="row">
			<span class="font">Enjoy Behaviour World</span>
	</div>
	<div class="row">
 
	</div>
	</div>
</center>
