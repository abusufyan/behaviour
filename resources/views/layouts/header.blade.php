 @include('layouts.head')
<body>

<div class="container-fluid mt-2">

<hr class="m-0">
<nav class="navbar navbar-expand-sm navbar-light">
<a class="navbar-brand float-right" href="{{url('/')}}"><img src="{{ asset('public/images/buddy logo transparent background.png') }}" alt="logo" class="blogo"><span class="h11 popfont"><b>My Behavior Buddy</b></span></a>
<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">

@if(Auth::user())
<ul class="navbar-nav ml-auto">
<li class="nav-item">
<a class="nav-link" href="{{ route('logout') }}"
onclick="event.preventDefault();
document.getElementById('logout-form').submit();">
<span class="h12 popfont">{{ __('Logout') }}</span>
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
@csrf
</form>
</li>&nbsp;
 
<li class="nav-item dropdown">
<a class="nav-link btn11 popfont text-white dropdown-toggle mt-1" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >Account Home</a>
<div class="dropdown-menu mr-auto">
<a class="dropdown-item popfont spanfont2" href="{{url('/accounthome')}}">Account Home</a>
<a class="dropdown-item popfont spanfont2" href="{{url('/editprofile/'.Auth::user()->id)}}">Edit Profile</a>
</div>
</li>

</ul>
@else
<ul class="navbar-nav ml-auto">
<li class="nav-item ml-3">
<a class="nav-link mt-2 fontsize" href="#" data-toggle="modal" data-target="#login" ><span class="h12 popfont">Login</span></a>
</li>&nbsp;&nbsp;
<li class="nav-item mt-2">
<input type="button" name="Logout" data-toggle="modal" data-target="#account" value="Sign Up" class="btn11 popfont">
</li>
</ul>
@endif

</div>
</nav>
<hr class="m-0">
<div class="navbar-nav">
	
<nav class="navbar navbar-expand-sm bg-light headpart ">


<ul class="navbar-nav widthhrd">

<li class="nav-item ml-3">
<a class="nav-link popfont" href="https://behaviourbuddy.facebhoook.com/#learn"><span class="h12 js-scroll-trigger first" > Learn more </span></a>

</li>
<li class="nav-item ml-3">
<a class="nav-link popfont" href="{{url('freeresources')}}"><span class="h12">Free Resources</span></a>
</li>
</ul>
</nav>
</div>


</div>
</nav>

@yield('content')


<!-- Login Modal -->
<div class="modal fade" id="login">
<div class="modal-dialog mdldialog">
<div class="modal-content smalscrn1">
<div class="modal-body">
<div>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true" class="h12">&times;</span>
</div>
<div class="mt-5">
	<center>
<span class="text-center h12 font">Login</span>
</center>
</div>
<form action="{{ route('login') }}" method="Post">
{{ csrf_field() }}

<div class="m-3">
<input type="email" name="email" placeholder="Email Address" class=" iptx validate @error('email') is-invalid @enderror" required>
@error('email')
<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>
<span class="invalid-feedback" data-toggle="modal" data-target="#login" role="alert">
<strong class="spanfont6">{{ $message }}</strong>
</span>
@enderror
</div>
<div class="m-3">
<input type="password" name="password" placeholder="Password" class="iptx validate @error('password') is-invalid @enderror" autocomplete="current-password" required>
@error('password')
<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>
<span class="invalid-feedback" data-toggle="modal" data-target="#login" role="alert">
<strong class="spanfont6">{{ $message }}</strong>
</span>
@enderror

</div>

<div class="m-3 row">
<input type="submit" class="regbtn2" value="Log In">

</div>
</form>
<div class="m-2 text-center">
<span class="popfont fontsize1">Not a mybehaviorbuddy.com member yet?</span>
<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#account" class="popfont font2 fontsize1" title="" > Create An Account</a><br>
<a href="{{url('/forgetpassword')}}" class="popfont font2 fontsize1" title="" >Forget Password</a>

</div>

<div class="mt-3">
<img src="{{ asset('public/images/buddy logo transparent background.png') }}" alt="logo" class=" ml-auto mr-auto d-block blogo1">
</div>
</div>

</div>
</div>
</div>






<!-- Create Account Modal -->
<div class="modal fade" id="account">
<div class="modal-dialog mdldialog">
<div class="modal-content smalscrn2">
<div class="modal-body">
<div>

<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true" class="h12">&times;</span>
</div>
<form method="post" action="{{ route('register') }}">
<div class="mt-5">
{{ csrf_field() }}
<center>
<span class="text-center h12 font">Create Account</span>
</center>
</div>
<div class="m-4">
<img src="{{ asset('public/images/buddy logo transparent background.png') }}" alt="logo" class=" ml-auto mr-auto d-block blogo1">
</div>
<div class="m-3">
<input type="email" name="email" placeholder="Email Address" class="iptx validate" required>

</div>


<div class="m-3">
<input type="password" name="password" placeholder="Password" class="iptx validate" required>
</div>

@error('email')
<script type='text/javascript'>
$(document).ready(function(){
$('#login').modal('show');
});
</script>
<span class="invalid-feedback" role="alert">
<strong class="spanfont6">{{ $message }}</strong>
</span>
@enderror

<div class="create1">
<input type="submit" class="regbtn1" value="Create Account">

</div>
</form>
<div class="m-2">
<span class="popfont ml-3 fontsize1">Already a member?</span><a href="#" title="" data-dismiss="modal" data-toggle="modal" data-target="#login" class="popfont fontsize1"> Log In</a>

</div>
<div class="m-2 text-center">

<span class="popfont spanfont">By clicking 'Create Account' above, I agree that I have read and agree to mybehaviorbuddy.com <a href="{{url('/termsandcondition')}}" title="">Terms of Use </a>and <a href="{{url('/privacypolicy')}}" title="">Privacy Policy.</a></span>
</div>
</div>

</div>
</div>
</div>