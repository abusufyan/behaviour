 <!DOCTYPE html>
<html>
<head>
<!-- Bootstrap Css -->
<link rel="stylesheet" href="{{ asset('public/css/image.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/behaviour.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap-grid.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap-grid.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap-reboot.css') }}" >
<link rel="stylesheet" href="{{ asset('public/css/bootstrap/css/bootstrap-reboot.min.css') }}" >

<!-- fonts icons -->
<link rel="stylesheet" href="{{ asset('public/icons/css/all.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/all.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/brands.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/brands.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/fontawesome.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/fontawesome.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/regular.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/regular.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/solid.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/solid.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/svg-with-js.min.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/svg-with-js.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/v4-shims.css') }}" >
<link rel="stylesheet" href="{{ asset('public/icons/css/v4-shims.min.css') }}" >


<link rel="stylesheet" href="https://use.typekit.net/igp2igs.css">


<link rel="apple-touch-icon" sizes="76x76" href="public/images/buddylogo-favicon.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="icon" type="image/png" sizes="16x16" href="public/images/buddylogo-favicon.png">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

  <title>My Behavior Buddy</title>
</head>
        @yield('content')
<style>
  @import url('https://use.typekit.net/igp2igs.css');
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('public/css/bootstrap/js/bootstrap.bundle.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/css/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/css/bootstrap/js/bootstrap.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/css/bootstrap/js/bootstrap.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/css/bootstrap/js/bootstrap.js') }}" type="text/javascript" charset="utf-8" async defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<script src="{{ asset('public/js/image-picker.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/js/image-picker.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/js/imagesloaded.pkgd.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="{{ asset('public/js/masonry.pkgd.min.js') }}" type="text/javascript" charset="utf-8" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/image-picker/0.3.1/image-picker.min.js" type="text/javascript" charset="utf-8" async defer></script>
<script type="text/javascript">
$(window).on('load', function() { // makes sure the whole site is loaded 
$('#status').fadeOut(); // will first fade out the loading animation 
$('#preloader').delay(50).fadeOut('slow'); // will fade out the white DIV that covers the website. 
$('body').delay(50).css({'overflow':'visible'});
})
</script> 
<style type="text/css">
  
  body {
  overflow: hidden;
}


/* Preloader */

#preloader {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #fff;
  /* change if the mask should have another color then white */
  z-index: 99;
  /* makes sure it stays on top */
}

#status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(https://raw.githubusercontent.com/niklausgerber/PreLoadMe/master/img/status.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- Preloader -->
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>

<script>
  $(document).ready(function()
{
$('img').bind('contextmenu', function(e){
return false;
});
});
</script>


@yield('javascript')
