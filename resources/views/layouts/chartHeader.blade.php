@include('layouts.head')
<body>
<nav class="navbar navbar-expand-sm navbar-light">
<a class="navbar-brand" href="{{url('/accounthome')}}"><img src="{{ asset('public/images/alien in spaceship.jpg') }}" alt="logo" class="blogo"></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item">
<a class="nav-link font" href="{{url('/accounthome')}}"><h4 class="fontsize"><b>Home</b></h4></a>
</li>
</ul>
<ul class="navbar-nav m-auto">
<?php 
$childname = Session::get('childname');
$character = Session::get('character');
$id = Session::get('profile');
$profileExist = Session::get('profileExist');
?>
 
<li class="nav-item">
<a class="nav-link font" href="#"><span class="fontsize"><b>My Charts</b> </span></a>
 
</ul>
<ul class="navbar-nav">
<li class="nav-item dropdown mt-3">
<a class="nav-link hname hrd2 text-white dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >{{$childname}}</a>
<div class="dropdown-menu">
<?php 
if(Auth::user()->active == 0 && $profileExist == 0){?>
<a class="dropdown-item" href="{{url('/chart')}}">Add Chart</a>
<?php } if(Auth::user()->active == 0 && $profileExist == 1) {?>
<a class="dropdown-item" data-toggle="modal" data-target="#premium">Add Chart</a>
<?php }if(Auth::user()->active == 1 ) {?>
<a class="dropdown-item" href="{{url('/chart')}}">Add Chart</a>
<?php }
?>  
<a class="dropdown-item" href="{{url('/editchildprofile/'.$id)}}">Edit Profile</a>
<a class="dropdown-item" href="{{url('/accounthome')}}">Switch Profile</a>
<a class="dropdown-item" href="{{url('/deleteprofile/'.$id)}}" onclick="return confirm('Are you sure you want to delete this ?')">Delete Profile</a>
</div>
</li>
<li class="nav-item mr-2">

<img src="{{url('public/character/'.$character->characterImage)}}" alt="logo" class="hlogo" >
</li> 
</ul>
</div>
</nav>

<hr class="hrd1">
<div class="modal fade" id="premium">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body ">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </div>
            <div class="row mt-5">
              <center><h4 class="ml-5 text-danger">Try Premium Account</h4></center>
              
            </div>
          <div class="m-5" >
           <span class="popfont mt-5">Please try premium account to add multiple profile's</span>
           </div>
          <div class="m-5">
          <a href="{{url('pricing')}}" title="" class="regbtn1 h5 mt-5">Upgrade to Premium</a>
            
          </div>
          </div>

        </div>
      </div>
    </div>
@yield('content')

<script>
function myFunction() {
alert("fdsfas")
document.getElementById("demo").innerHTML = "Hello World";
}
</script>



  
