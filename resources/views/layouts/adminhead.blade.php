<!DOCTYPE html>
<html lang="en">
    
 
<head>        
<title>My Behavior Buddy</title>            
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="icon" href="favicon.ico" type="image/x-icon" />
 
<link rel="stylesheet" type="text/css" id="theme" href="{{ asset('public/admin/css/theme-default.css') }}"/>
 </head>
    <body>
         <div class="page-container">
            
             <div class="page-sidebar">
  
                <ul class="x-navigation">
                    <li class="">
                        <a href="">My Behavior Buddy</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="{{url('/dashboard')}}" class="profile-mini" style="border-radius: 5% !important; ">
                            <img src="{{ asset('public/images/buddy logo transparent background.png') }}" class="blogo" alt="Logo" style="border-radius: 5% !important; " />
                        </a>
                        <div class="profile" >
                            <a href="{{url('/dashboard')}}">
                            <div class="profile-image"  >
                                <img src="{{ asset('public/images/buddy logo transparent background.png') }}" class="blogo" alt="Logo"/>
                            </div>
                        </a>
                            <div class="profile-data">
                                <a href="{{url('/dashboard')}}">
                                <div class="profile-data-name">My Behavior Buddy</div>
                                </a>
                             </div>
                            
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="xn-openable ">
                        <a href="#"><span class="fa fa-dashboard"></span> <span class="xn-text">Buddies</span></a>
                        <ul>
                            <li class="active"><a href="{{url('addavatar')}}"><span class="xn-text"><span class="fa fa-image"></span>Add New</span></a></li>
                            <li><a href="{{url('avatarlist')}}"><span class="xn-text"><span class="fa fa-dashboard"></span>View List</span></a></li>
                         </ul>
                    </li> 
                     <li class="xn-openable ">
                        <a href="#"><span class="fa fa-dashboard"></span> <span class="xn-text">Profile Characters</span></a>
                        <ul>
                            <li class="active"><a href="{{url('addcharacter')}}"><span class="xn-text"><span class="fa fa-image"></span>Add New</span></a></li>
                            <li><a href="{{url('characterlist')}}"><span class="xn-text"><span class="fa fa-dashboard"></span>View List</span></a></li>
                         </ul>
                    </li> 
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text"> Chart Background</span></a>
                        <ul>
                            <li><a href="{{url('addbackground')}}"><span class="fa fa-image"></span>Add New </a></li>

                            <li><a href="{{url('backgroundlist')}}"><span class="fa fa-dashboard"></span>View List</a></li>
                            
                        </ul>
                    </li>                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Reward Icons</span></a>
                     <ul>
                            <li><a href="{{url('/addreward')}}"><span class="fa fa-image"></span>Add New </a></li>
                            <li><a href="{{url('/rewardlist')}}"><span class="fa fa-dashboard"></span>View List</a></li>
                             
                         </ul> 
                    </li>


               <!--  <li class="xn-openable">
                <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Try For Free</span></a>
                <ul>
                
                <li><a href="{{url('/tryforfreelist')}}"><span class="fa fa-dashboard"></span>View List</a></li>

                </ul> 
                </li> -->
                   <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Free Resources</span></a>
                        <ul>
                            <li class="active"><a href="{{url('/addcategory')}}"><span class="xn-text"><span class="fa fa-image"></span>Add New</span></a></li>
                            <li><a href="{{url('/categorylist')}}"><span class="xn-text"><span class="fa fa-dashboard"></span>View List</span></a></li>
                         </ul>
                    </li> 
                <li>
                <a href="{{url('/editprivacypolicy')}}"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Privacy Policy</span></a>

                </li>
                <li>
                <a href="{{url('/manageterms')}}"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Terms & Conditions</span></a>

                </li>
                 <li>
                <a href="{{url('/manageaboutus')}}"><span class="fa fa-files-o"></span> <span class="xn-text">Manage About us</span></a>

                </li>
                <li>
                <a href="{{url('/couponlist')}}"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Coupon</span></a>
                 
                </li>
                 <li>
                <a href="{{url('/shoplist')}}"><span class="fa fa-files-o"></span> <span class="xn-text">Manage Shop</span></a>                 
                </li>

                <li>
                <a href="{{url('/paymenthistory')}}"><span class="fa fa-dollar"></span> <span class="xn-text">Payment History</span></a>
                 
                </li>
                <li>
                <a href="{{url('/manageusers')}}"><span class="fa fa-user"></span> <span class="xn-text">User Details</span></a>
                 
                </li>
                
             
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
                             <li><a href="{{url('/adminprofile')}}"><span class="fa fa-edit"></span> Edit Profile</a></li>
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                   
                </ul>
              
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>

                  <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="{{url('/logout')}}" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{ asset('public/admin/audio/alert.mp3') }}" preload="auto"></audio>
        <audio id="audio-fail" src="{{ asset('public/admin/audio/fail.mp3') }}" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/jquery/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/jquery/jquery-ui.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/bootstrap/bootstrap.min.js') }}"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src= 'public/admin/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/morris/raphael-min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/morris/morris.min.js') }}"></script>       
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/rickshaw/d3.v3.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/rickshaw/rickshaw.min.js') }}"></script>
        <script type='text/javascript' src='public/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src="{{ asset('public/admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/owl/owl.carousel.min.js') }}"></script>                 
        
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{ asset('public/admin/js/settings.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins.js') }}"></script>        
        <script type="text/javascript" src="{{ asset('public/admin/js/actions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/admin/js/demo_dashboard.js') }}"></script>
        <script src="https://cdn.tiny.cloud/1/5wnoyz7e9t8soyjfgoo06tbkq3vmmh11jzuofs7crhuzjr62/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

        <script>
        tinymce.init({
        selector: 'textarea',
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
        });
        </script>


        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
 
</html>
               

    @yield('content')

  




