@include('layouts.chartHeader')
@section('content')

@endsection
<?php $character = Session::get('character') ?>
 
<div class="container-fluid mt-5">
  
<center>
 
<div class="card p-5 mt-5 col-6 d-flex justify-content-center">
  @if(session('message'))
  <p class="alert alert-warning">
  {{session('message')}}</p>
  @endif
  <div class="row m-3  d-flex justify-content-center">
    <img src="{{url('public/character/'.$character->characterImage)}}" class="blog2">
  </div>
<form method="Post" action="{{url('/updatechildprofile')}}">
                      {{ csrf_field() }}

<input type="hidden" name="id" value="{{$id}}}"> 
<div class="row m-3">
<label class="popfont mt-auto mb-auto d-flex spanfont6 col-4">Child’s Name</label><input type="text" class="iptx col-8" name="childName" value="{{$profile->childName}}">
</div>

<div class="row m-3">
  <label class="popfont mt-auto mb-auto d-flex spanfont6 col-4">Change Character Image</label>
  <div class=" ex10 mt-3 mb-4 col-8">
          <div class="row m-auto">
        @foreach($characterimg as $row) 
        <div class="col-4 product-chooser">
        <div class="product-chooser-item">
        <img src="{{url('public/character/'.$row->characterImage)}}" class="addimg"  id="addimg" data-img-src="{{url('public/character'.$row->characterImage)}}" value="{{$row->id}}" name="characterId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
        <input type="radio" name="characterId" value="{{$row->id}}" checked="checked">
        </div>

        </div>
        @endforeach
    
          </div>
   </div>
  
</div>
<div class="row d-flex justify-content-center">

<input type="submit" name="" class="regbtn" value="Update">

</div>
</form>
</div>
</center>
</div>

<!-- Modal -->
<link rel="stylesheet" type="text/css" href="public/css/tabmenu.css">


<div class="modal fade" id="addAvatar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body ">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </div>
            <div class="row mt-5">
              <h4 class="ml-4">ADD A PROFILE</h4>
              
            </div>
            <form action="{{url('/insertprofile')}}" method="Post">  
            {{ csrf_field() }}

          <div class="m-3 mt-2">
          <input type="text" name="childName" placeholder="Child's name" class=" iptx validate" required>
          <input type="hidden" name="userId" value="{{Auth::user()->id}}">
          </div>
        
    

        <div class="m-auto mt-5 row">
        <button type="Reset" class="cnclbtn col-5 p-2">Cancel</button>
        <div class="col-2">

        </div>
        <input type="submit" value="ADD PROFILE" class="regbtn2 col-5 p-2">


        </div>
        </form>
          </div>

        </div>
      </div>
    </div>


    <script>
  $(function(){
    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
      $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
      $(this).addClass('selected');
      $(this).find('input[type="radio"]').prop("checked", true);

      console.log(document.getElementsByClassName("selected"));
    });
  });
</script>


<style type="text/css" media="screen">
  .over
  {
    margin-left: -40px !important;
  }

  /* Image Code */

  .containerab {
  }
  .circle-left, .circle-right {
    position: absolute;

  }
  img {
    padding: 0;
  }
  .circle-right {
    z-index:1;
    margin: 3% 3% 15% 17%;
    padding: 1%;
    text-align: center;
    width: 77%;
    border-radius: 5px;
    object-fit: cover;
  }

  .ab3{
    -webkit-transform:rotate(270deg);
    -moz-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    transform: rotate(270deg);
  }
  .ab2{
    -webkit-transform:rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
  }

</style>