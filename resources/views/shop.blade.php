@include('layouts.header')
@section('content')

@endsection

<div class="container-fluid ">
  <div class="row">
    <div class="col-1"></div>
    <div class="col-10">
<div class="containerab">
  <div class="row">

    <div class="row" style="margin-top:25%; margin-left: 4%">
      <div class="col-3">
<nav>
<div class="nav nav-tabs nav-fill row bg-transparent"  style="margin-top:62%;"  id="nav-tab" role="tablist">
<a class="nav-item col-6"  id="nav-Owl-tab" data-toggle="tab" href="#nav-Owl" role="tab" aria-controls="nav-Owl" aria-selected="true"><img src="{{ asset('public/images/buddies buddy store tab.png') }}" alt="&nbsp;" class="img-fluid img5"></a>


<a class="nav-item col-6" id="nav-steps-tab" style="margin-left: -8%;" data-toggle="tab" href="#nav-steps" role="tab" aria-controls="nav-steps" aria-selected="false"><img src="{{ asset('public/images/bgimg.png') }}" alt="&nbsp;" class="img-fluid img5"></a>

</div>
</nav>
</div>
<div class="col-9 imgcol1">
  
<div class="tab-content py-3 px-3 px-sm-0 " id="nav-tabContent">
  <!--  Avatar Image tab menu -->
  <div class="tab-pane fade show active overflow-hidden" id="nav-Owl" role="tabpanel" aria-labelledby="nav-Owl-tab">
<div class="overflow-auto col-12" id="containerrow">
  <div class="d-flex"  id="contentrow">
    <div class="row col92 ">
    @foreach($buddy as $row)
    <?php if(Auth::user()->active == 0 ){?>

    <div class="product-chooser m-2 imgcol2">
      <div class=" product-chooser-item rowcal2">
        <div class="">        
           <a data-toggle="modal" data-target="#premium" style="margin-left: 5px;  width: 90%;">                    
          <img src="{{url('public/shop/'.$row->image)}}" class="img-responsive imgfile"  data-img-src="public/avatar/android boy.png" value="AndroidBoy" name="avatarId" alt="plus">
          <input type="radio" name="AndroidBoy" value="AndroidBoy" checked="checked">
           </a>
         </div>
        <div class="number1" style="">
          <img src="{{ asset('public/images/Behavior Buddy token.png') }}" alt="" class="img-fluid col-5 ">
          <span class=" h4 font-weight-bold">{{$row->price}}</span>
        </div>
       </div>
     </div>
   <?php }
   else{?>

    <div class="product-chooser m-2  imgcol2">
      <div class=" product-chooser-item rowcal2">
        <div class="">        
           <a href="javascript:void(0);" class="update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px; width: 90%;">                    
          <img src="{{url('public/avatar/'.$row->avatarImage)}}" class="img-responsive imgfile" data-img-src="public/avatar/android boy.png" value="AndroidBoy" name="avatarId" alt="plus">
          <input type="radio" name="AndroidBoy" value="AndroidBoy" checked="checked">
           </a>
         </div>
        <div class="number1" style="">
          <img src="{{ asset('public/images/Behavior Buddy token.png') }}" alt="" class="img-fluid col-5">
          <span class=" h4 font-weight-bold">{{$row->price}}</span>
        </div>
       </div>
     </div>



   <?php }
   ?>
     @endforeach   
    </div>   

      
 
    </div> 
</div>
<div class="row mrgn1">
<div class="col-4">
<div class=" row d-flex justify-content-center">
<img src="{{ asset('public/images/buddy store arrow backwards.png') }}"  id="slideBack"  class="img-fluid btn col-5" alt="">
</div>
</div>
<div class="col-4">
<div class=" row d-flex justify-content-center">
<img src="{{ asset('public/images/buddy store arrow forward.png') }}"  id="slide" class="img-fluid btn col-5" alt="">
</div>
</div>
</div>
</div>




  <!--  Background Image tab menu -->
<div class="tab-pane fade show fade" id="nav-steps" role="tabpanel" aria-labelledby="nav-steps-tab">
<div class="overflow-auto col-12" id="containerrow2">
  <div class="d-flex"  id="contentrow2">
    <div class="row col92 " style="margin-top: -1%;">
    @foreach($background as $row)
    <div class="product-chooser m-3  widthabc">
      <div class=" product-chooser-item">
      <div class="">
         <a href="javascript:void(0);" class="update_product" id="background" data-id="{{$row->id}}" style="margin-left: 5px;"> 
        <img src="{{url('public/background/'.$row->backgroundImage)}}" class="img-responsive imgfile2"  data-img-src="public/avatar/android boy.png" value="AndroidBoy" name="avatarId" alt="plus">
        <input type="radio" name="AndroidBoy" value="AndroidBoy" checked="checked">
      </a>
      </div>
      <div class="number2" style="">
        <img src="{{ asset('public/images/Behavior Buddy token.png') }}" alt="" class="img-fluid col-lg-1 col-md-4">
        <span class=" h4 font-weight-bold">{{$row->price}}</span>
      </div>
    </div>
     </div>
     @endforeach   
    </div>    
      
 
    </div> 
</div>
<div class="row mrgn1">
<div class="col-4">
<div class=" row d-flex justify-content-center">
<img src="public/images/buddy store arrow backwards.png"  id="slideBack2"  class="img-fluid btn col-lg-4 col-md-5" alt="">
</div>
</div>
<div class="col-4">
<div class=" row d-flex justify-content-center">
<img src="public/images/buddy store arrow forward.png"  id="slide2" class="img-fluid btn col-lg-4 col-md-5" alt="">
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</div>



</div>


        </div>
      </div>


    </div>

  <div class="modal fade" id="premium">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body ">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </div>
          <div class="row mt-5">
          <center><h4 class="ml-5 text-danger">Try Premium Account</h4></center>

          </div>
          <div class="m-5" >
            <span class="popfont mt-5">Please try premium account to add multiple profile's</span>
          </div>
          <div class="m-5">
          <a href="{{url('pricing')}}" title="" class="regbtn1 h5 mt-5">Upgrade to Premium</a>

          </div>
        </div>

      </div>
    </div>
  </div>

   <div class="modal fade" id="error">
    @if(session()->has('error'))
            <script type='text/javascript'>
            $(document).ready(function(){
            $('#error').modal('show');
            });
            </script>
             
        @endif
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body p-5">
            <div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </div>
            <div class=" mt-2 ">
            <center> <span class="text-danger h4 font text-center">Sorry</span></center> 

            </div>
            <div class="mt-5" >
            <center>
              <span class="popfont text-center  ">Your coins balance is insufficient </span>
            </center>  
            </div>
             
          </div>

        </div>
      </div>
  </div>

 
<!-- Create Account Modal -->
<div class="modal fade" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content p-3">
      <div class="modal-body ">
        <div>
        

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          </div>
          <form method="POST" action="{{ url('/shopnow') }}">
            <input type="hidden" name="shopId" class="id">
            <input type="hidden" name="coins" class="price">
            <input type="hidden" name="category" class="category">
            <div class="notshow">
            <div class="mt-4">
              {{ csrf_field() }}
              <h4 class="text-center font">Are You Sure ?</h4>
              <p class="text-center font">you want to redeem </p>
            </div>
            <div class="row mt-5">
              <div class="col-6 text-center">
                <label class="font text-center">Coins</label>
              </div>
              <div class="col-6 d-flex flex-row justify-content-center">
                <p id="price"  class="font"></p><span> <img src="{{ asset('public/images/Behavior Buddy token.png') }}" alt="" class="img-fluid col-4"> </span>
                                
              </div>
              
            </div>           
           
            <span class="invalid-feedback" role="alert">
            </span>
             <div class="d-flex justify-content-center mt-5 ">
              <input type="submit"  class="regbtn1"  value="Shop Now">
            </div>
           </div>
           <div class="buy d-none">
             <h4 class="text-center font">Sorry!!</h4>
              <p class="text-center font">you have already bought this</p>

           </div>
           </form>
          <div class="m-2">

          </div>
          
        </div>

      </div>

    </div>
  </div>


<div class="modal fade" id="buy">
  <div class="modal-dialog">
    <div class="modal-content p-3">
      <div class="modal-body ">
        <div>
        

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          </div>

          <div class="buy">
             <h4 class="text-center font">Sorry!!</h4>
              <p class="text-center font">you have already bought this</p>

           </div>
           
          <div class="m-2">

          </div>
          
        </div>

      </div>

    </div>
  </div>
<style>
.containerab {
}
.circle-left, .circle-right {
position: absolute;

}
img {
padding: 0;
}
.circle-right {
z-index:1;
padding: 1%;
text-align: center;
object-fit: cover;
}

html {
scroll-behavior: smooth;
overflow: scroll;
}
::-webkit-scrollbar {
width: 0px;
background: transparent; / make scrollbar transparent /
}

</style>



<script>
  $(function(){
    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
      $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
      $(this).addClass('selected');
      $(this).find('input[type="radio"]').prop("checked", true);

      console.log(document.getElementsByClassName("selected"));
    });
  });
</script>

<!-- Slide 1 -->
<script>
  var button = document.getElementById('slide');
button.onclick = function () {
    var container = document.getElementById('containerrow');
    sideScroll(container,'right',140,240,80);
};

var back = document.getElementById('slideBack');
back.onclick = function () {
    var container = document.getElementById('containerrow');
    sideScroll(container,'left',140,242,80);
};

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

</script>

 
<!-- Slider 2 -->


<script>
  var button = document.getElementById('slide2');
button.onclick = function () {
    var container = document.getElementById('containerrow2');
    sideScroll(container,'right',130,290,85);
};

var back = document.getElementById('slideBack2');
back.onclick = function () {
    var container = document.getElementById('containerrow2');
    sideScroll(container,'left',130,235,85);
};

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

</script>



<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#background', function(e){
data =$(this).data('id');
 //alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'shopBackground/'+data,
 
success:function(data){
 // $("#confirm").modal('show');
 $('.id').val(data.id);
$('.backgroundName').val(data.backgroundName);
$('.price').val(data.price);
$('.type').val(data.type);
$('.category').val(data.category);

 var price =data.price;
 var background =data.background;
    //console.log(avatar);

 if (background >= 1) {
     $("#confirm").modal('hide');
     $("#buy").modal('show');
 }else{
  $("#buy").modal('hide');
  $("#confirm").modal('show');   
 }
 
  document.getElementById("price").innerHTML = price;

 
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>





<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#edit', function(e){
data =$(this).data('id');
 //alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'shopconfirm/'+data,
 
success:function(data){
//$("#confirm").modal('show');
$('.id').val(data.id);
$('.avatarName').val(data.avatarName);
$('.price').val(data.price);
$('.type').val(data.type);
$('.category').val(data.category);


 var price =data.price;

 var avatar =data.avatar;
    console.log(data.category);

 if (avatar >=1) {
     $("#confirm").modal('hide');
     $("#buy").modal('show');
 }
 else
 {
  $("#buy").modal('hide');
  $("#confirm").modal('show');   
 }
  document.getElementById("price").innerHTML = price;

 
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>