
<!DOCTYPE html>
<html>

<head>
  <title>Behavior</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <style>


      .roundModal {
      border-radius: 30px;
      }

      .modal {
      height: 100%;
      width: 100%;
      }

      .imgClass {
      /* max-width: 200px;
      min-width: 50px; */
      width: 85px;
      height: 100px;
      /* max-height: 120px;
      min-height: 50px; */
      margin-left: 40%;
      }

      .circle-left, .circle-right, .circle-right2{
      position: absolute;

      }
      .circle-right {
      z-index:1;
      margin: 11% 3% 15% 7%;
      padding: 1%;
      text-align: center;
      width: 77%;
      border-radius: 5px;
      object-fit: cover;
      }
      .circle-right2 {
      z-index:1;
      margin: 11% 3% 15% 7%;
      object-fit: cover;
      }



      .object {
      animation: MoveUpDown 2s linear infinite;
      position: absolute;
      left: 0;
      bottom: 0;
      margin-bottom: 100px;
      }

      .object2 {
      animation: MoveUpDown 2s linear infinite;
      position: absolute;
      left: 0;
      bottom: 0;
      top: 100;
      }

      .nextbackRow {
      margin-top: 10%;
      }

      .pod {
      margin-top: 100px;
      width: "12.5%";
      }

      .pod2 {
      width: "12.5%";
      }

      .rewardID {
      margin-left: -10%;
      }

      @keyframes MoveUpDown {

      0%,
      100% {
      bottom: 0;
      }

      50% {
      bottom: 15px;
      }
      }

      #tokenRotate1,
      #tokenRotate2,
      #tokenRotate3,
      #tokenRotate4,
      #tokenRotate5,
      #tokenRotate6,
      #tokenRotate7 {
      padding-bottom: 15%;
      margin-left: 30%;
      /* margin: 0 auto; */
      -webkit-animation: rotation 2s infinite linear;
      }

      @-webkit-keyframes rotation {
      from {
      -webkit-transform: rotate(0deg);
      }

      to {
      -webkit-transform: rotateY(359deg);
      }
      }

      @media only screen and (max-width: 991px) {
      .tech-slideshow {
      max-width:100%;
      margin: 0 auto;
      position: relative;
      overflow: hidden;
      transform: translate3d(0, 0, 0);
      }

      .tech-slideshow > div {
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      height: 900px;
      transform: translate3d(0, 0, 0);
      }
      .tech-slideshow .mover-1 {
      animation: moveSlideshow 12s linear infinite;
      }
      .tech-slideshow .mover-2 {
      opacity: 0;
      transition: opacity 0.5s ease-out;
      background-position: 0 -200px;
      animation: moveSlideshow 15s linear infinite;
      }

      @keyframes moveSlideshow {
      100% { 
      transform: translateX(-66.6666%);  
      }
      }

      .imgClass2 {
      width: 50%;
      height: 60%;
      margin-left: 10%;
      }
      .imgresp
      {
      background: url('public/images/bg3.jpg')  no-repeat center center fixed;
      height: 1100px;
      width: 1960px;
      }

      }


      @media only screen and (max-width: 768px) {
      .tech-slideshow {
      height: 200px;
      margin: 0 auto;
      position: relative;
      overflow: hidden;
      transform: translate3d(0, 0, 0);
      }

      .tech-slideshow > div {
      height: 200px;
      width: 2526px;
      background: url("/public/images/bg3.jpg");
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      transform: translate3d(0, 0, 0);
      }
      .tech-slideshow .mover-1 {
      animation: moveSlideshow 12s linear infinite;
      }
      .tech-slideshow .mover-2 {
      opacity: 0;
      transition: opacity 0.5s ease-out;
      background-position: 0 -200px;
      animation: moveSlideshow 15s linear infinite;
      }

      @keyframes moveSlideshow {
      100% { 
      transform: translateX(-66.6666%);  
      }
      }
      }

      @media only screen 
      and (max-width : 480px) {
      .SmallWid{
      width: 1950px !important;
      overflow-x: scroll;
      overflow-y: none;
      height: 100px;
      }

      }

      </style>

      <script>
      count = 0;

      function retCount(c){
      return c;
      }

      function onJump() {

      if (screen.width < 500) {
      console.log("WIDTH",screen.width)
      }
      var a = count += 1;
      // alert(a);
      document.getElementById("count").value = retCount(a);//Now you get the js variable inside your form element

      console.log(a)
      var elem = document.getElementById("foxID");
      var elem3 = document.getElementById("next");
      var audio = document.getElementById("myAudio");

      elem3.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }], {
      duration: 100,
      }
      );

      if (a <= 7) {
      console.log("IN 7")
      if (a % 2 == 0) {
      console.log("EVEN")
      audio.play()

      elem.animate(
      [
      // keyframes

      {
        top: "-5%"
      },
      {
        top: "-50%"
      },
      {
        left: a * 100 + "%"
      },
      {
        bottom: "10%"
      },
      {
        bottom: "5%"
      },

      ], {
      // timing options
      duration: 2000,
      //   iterations: 2,
      }
      );
      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + a);

      elem.style.left = a * 100 + "%";
      // elem.style.paddingBottom = "-25%";
      elem.style.marginBottom = "40%";

      elem2.style.visibility = "hidden";
      //   }, 1000);
      }, 1000);
      } else if (a % 2 != 0) {
      audio.play()
      elem.animate(
      [

      {
        top: "-5%"
      },
      {
        top: "-50%"
       },
      {
         left: a * 100 + "%"
      },
      {
        bottom: "5%"
      },
      {
        bottom: "2%"
      },

      ], {
      duration: 2000,
      }
      );
      setTimeout(function () {
      elem.style.left = a * 100 + "%";
      var elem2 = document.getElementById("tokenRotate" + a);

      elem.style.paddingBottom = "10%";
      elem2.style.visibility = "hidden";
      }, 1000);

      } 
      }else {
      console.log("&&&&& OFF 7")
      var audio2 = document.getElementById("myAudio2");
      audio2.play()
      var SCREEN_WIDTH = window.innerWidth,
      SCREEN_HEIGHT = window.innerHeight,
      mousePos = {
      x: 400,
      y: 300
      },

      // create canvas
      canvas = document.createElement('canvas'),
      context = canvas.getContext('2d'),
      particles = [],
      rockets = [],
      MAX_PARTICLES = 400,
      colorCode = 0;

      // init
      $(document).ready(function () {
      document.body.appendChild(canvas);
      canvas.width = SCREEN_WIDTH;
      canvas.height = SCREEN_HEIGHT;
      canvas.style.marginTop = "-" + SCREEN_HEIGHT + "px"

      setInterval(launch, 800);
      setInterval(loop, 1000 / 50);
      });

      // update mouse position
      $(document).mousemove(function (e) {
      e.preventDefault();
      mousePos = {
      x: e.clientX,
      y: e.clientY
      };
      });

      // launch more rockets!!!
      $(document).mousedown(function (e) {
       for (var i = 0; i < 5; i++) {
            launchFrom(Math.random() * SCREEN_WIDTH * 2 / 3 + SCREEN_WIDTH / 6);
        }
      });

      function launch() {
      launchFrom(mousePos.x);
      }

      function launchFrom(x) {
        if (rockets.length < 10) {
          var rocket = new Rocket(x);
          rocket.explosionColor = Math.floor(Math.random() * 360 / 10) * 10;
          rocket.vel.y = Math.random() * -3 - 4;
          rocket.vel.x = Math.random() * 6 - 3;
          rocket.size = 8;
          rocket.shrink = 0.999;
          rocket.gravity = 0.01;
          rockets.push(rocket);
        }
      }

      function loop() {
      // update screen size
      if (SCREEN_WIDTH != window.innerWidth) {
        canvas.width = SCREEN_WIDTH = window.innerWidth;
      }
      if (SCREEN_HEIGHT != window.innerHeight) {
         canvas.height = SCREEN_HEIGHT = window.innerHeight;
      }

      // clear canvas
      context.fillStyle = "rgba(0, 0, 0, 0.05)";
      context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

      var existingRockets = [];

      for (var i = 0; i < rockets.length; i++) {
      // update and render
      rockets[i].update();
      rockets[i].render(context);

      // calculate distance with Pythagoras
      var distance = Math.sqrt(Math.pow(mousePos.x - rockets[i].pos.x, 2) + Math.pow(mousePos.y - rockets[i].pos
      .y, 2));

      // random chance of 1% if rockets is above the middle
      var randomChance = rockets[i].pos.y < (SCREEN_HEIGHT * 2 / 3) ? (Math.random() * 100 <= 1) : false;
 
      if (rockets[i].pos.y < SCREEN_HEIGHT / 5 || rockets[i].vel.y >= 0 || distance < 50 || randomChance) {
      rockets[i].explode();
      } else {
      existingRockets.push(rockets[i]);
      }
      }

      rockets = existingRockets;

      var existingParticles = [];

      for (var i = 0; i < particles.length; i++) {
      particles[i].update();

      // render and save particles that can be rendered
      if (particles[i].exists()) {
      particles[i].render(context);
      existingParticles.push(particles[i]);
      }
      }

      // update array with existing particles - old particles should be garbage collected
      particles = existingParticles;

      while (particles.length > MAX_PARTICLES) {
      particles.shift();
      }
      }

      function Particle(pos) {
      this.pos = {
      x: pos ? pos.x : 0,
      y: pos ? pos.y : 0
      };
      this.vel = {
      x: 0,
      y: 0
      };
      this.shrink = .97;
      this.size = 2;

      this.resistance = 1;
      this.gravity = 0;

      this.flick = false;

      this.alpha = 1;
      this.fade = 0;
      this.color = 0;
      }

      Particle.prototype.update = function () {
      // apply resistance
      this.vel.x *= this.resistance;
      this.vel.y *= this.resistance;

      // gravity down
      this.vel.y += this.gravity;

      // update position based on speed
      this.pos.x += this.vel.x;
      this.pos.y += this.vel.y;

      // shrink
      this.size *= this.shrink;

      // fade out
      this.alpha -= this.fade;
      };

      Particle.prototype.render = function (c) {
      if (!this.exists()) {
      return;
      }

      c.save();

      c.globalCompositeOperation = 'lighter';

      var x = this.pos.x,
      y = this.pos.y,
      r = this.size / 2;

      var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
      gradient.addColorStop(0.1, "rgba(255,255,255," + this.alpha + ")");
      gradient.addColorStop(0.8, "hsla(" + this.color + ", 100%, 50%, " + this.alpha + ")");
      gradient.addColorStop(1, "hsla(" + this.color + ", 100%, 50%, 0.1)");

      c.fillStyle = gradient;

      c.beginPath();
      c.arc(this.pos.x, this.pos.y, this.flick ? Math.random() * this.size : this.size, 0, Math.PI * 2, true);
      c.closePath();
      c.fill();

      c.restore();
      };

      Particle.prototype.exists = function () {
      return this.alpha >= 0.1 && this.size >= 1;
      };

      function Rocket(x) {
      Particle.apply(this, [{
      x: x,
      y: SCREEN_HEIGHT
      }]);

      this.explosionColor = 0;
      }

      Rocket.prototype = new Particle();
      Rocket.prototype.constructor = Rocket;

      Rocket.prototype.explode = function () {
      var count = Math.random() * 10 + 80;

      for (var i = 0; i < count; i++) {
      var particle = new Particle(this.pos);
      var angle = Math.random() * Math.PI * 2;

      // emulate 3D effect by using cosine and put more particles in the middle
      var speed = Math.cos(Math.random() * Math.PI / 2) * 15;

      particle.vel.x = Math.cos(angle) * speed;
      particle.vel.y = Math.sin(angle) * speed;

      particle.size = 10;

      particle.gravity = 0.2;
      particle.resistance = 0.92;
      particle.shrink = Math.random() * 0.05 + 0.93;

      particle.flick = true;
      particle.color = this.explosionColor;

      particles.push(particle);
      }
      };

      Rocket.prototype.render = function (c) {
      if (!this.exists()) {
      return;
      }

      c.save();

      c.globalCompositeOperation = 'lighter';

      var x = this.pos.x,
      y = this.pos.y,
      r = this.size / 2;

      var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
      gradient.addColorStop(0.1, "rgba(255, 255, 255 ," + this.alpha + ")");
      gradient.addColorStop(1, "rgba(0, 0, 0, " + this.alpha + ")");

      c.fillStyle = gradient;

      c.beginPath();
      c.arc(this.pos.x, this.pos.y, this.flick ? Math.random() * this.size / 2 + this.size / 2 : this.size, 0,
      Math.PI * 2, true);
      c.closePath();
      c.fill();

      c.restore();
      };
      const id=3
      window.setTimeout(function () {
       
      window.location.href = "http://certificate/"+id;
      console.log("OK")
      }, 1000)
      }



      }


 
      function onJumpBack() {
      var a = count -= 1;

      console.log("JUMPBACK", a)
      var elem = document.getElementById("foxID");
      //         var elem2 = document.getElementById("tokenRotate");
      var elem3 = document.getElementById("back");
      document.getElementById("count").value = retCount(a);//Now you get the js variable inside your form element

      elem3.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }], {
      duration: 100,
      }
      );
      var token = a + 1
      if (a === 0) {
      elem.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }, {
      transform: "scale(0.33)"
      }], {
      duration: 800,
      }
      );
      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + token);
      elem.style.left = a * 100 + "%";
      elem.style.paddingBottom = "0px";
      elem2.style.visibility = "visible";
      //   }, 1000);
      }, 800);
      } else if (a === 1) {
      elem.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }, {
      transform: "scale(0.33)"
      }], {
      duration: 800,
      }
      );

      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + token);
      elem.style.left = a * 100 + "%";
      elem.style.paddingBottom = "30px";

      elem2.style.visibility = "visible";
      //   }, 1000);
      }, 800);
      } else if (a === 2) {
      elem.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }, {
      transform: "scale(0.33)"
      }], {
      duration: 800,
      }
      );

      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + token);
      elem.style.left = a * 100 + "%";
      elem.style.paddingBottom = "70px";

      elem2.style.visibility = "visible";
      //   }, 1000);
      }, 800);
      } else if (a === 3) {
      elem.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }, {
      transform: "scale(0.33)"
      }], {
      duration: 800,
      }
      );

      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + token);
      elem.style.left = a * 100 + "%";
      elem.style.paddingBottom = "30px";

      elem2.style.visibility = "visible";
      //   }, 1000);
      }, 800);
      } else if (a === 4) {
      elem.animate(
      [{
      transform: "scale(1)"
      }, {
      transform: "scale(0.66)"
      }, {
      transform: "scale(0.33)"
      }], {
      duration: 800,
      }
      );

      setTimeout(function () {

      var elem2 = document.getElementById("tokenRotate" + token);
      elem.style.left = a * 100 + "%";
      elem.style.paddingBottom = "70px";

      elem2.style.visibility = "visible";
      //   }, 1000);
      }, 800);
      } else {
      alert("UNDEFINED ERROR")
      }
      }
      </script>
</head>

<body>
        <?php $bgimg = "public/background/$profile->backgroundImage" ?>

  <div 
  class="tech-slideshow imgresp SmallWid"
 id="BG"

 style="background-image: url('<?php echo url($bgimg);?>');
background-size: 100%;"
      >
    <div class="container-fluid SmallWid" 
    >
        <div class="row mt-2">
          <div class="col-lg-3 col-md-3 col-sm-3 ml-auto d-block">
          <div class="containerab">
          <img
          src="{{url('public/images/name in upper right corner of chart-design.png')}}"
          class="img-fluid circle-left"/>
          <div class="circle-right row">
          <img src="{{url('public/images/Boy5.png')}}" class="img-fluid w-25 h-25"/>
          <div class="text-white">
          <h4 class="font ml-5 mt-3 "><b>{{$profile->childName}}</b></h4>
          </div> 
          </div>
          </div>
        </div>
      </div>

          <div class="row" style="margin-top: 12%;">
          <div class="col-2 ml-auto d-block">
          <div class="containerab">
          <img src="{{url('public/images/buddy token icon counter for profile page.png')}}" class="circle-left img-fluid col-10">
          <div class="circle-right2">
 <input name="count" type="text" class="h5 font" id="count" style="background: transparent; border:none; color: blue; margin-left: 20%;" readonly="">

          </div>

        </div>
</div>
</div>
      <audio src="{{url('public/audio/jump.mp3')}}" id="myAudio" style="visibility: hidden">
        <!-- <source
            src="public/images/audio/jump.mp3"
            id="myAudio"
            type="audio/ogg"
          /> -->
      </audio>

      <audio src="{{url('public/audio/celebrate.mp3')}}" id="myAudio2" style="visibility: hidden">
        <!-- <source
            src="public/images/audio/jump.mp3"
            id="myAudio"
            type="audio/ogg"
          /> -->
      </audio>


      <div class="row mt-5">
        <div class="col-lg-3 col-md-3 col-sm-3 mt-5">
          <div class="row">
            <div class="col-lg-6">
              <div class="col-lg-8 col-md-8 col-sm-8 object" id="foxID">
                <img src="{{url('public/avatar/'.$profile->avatarImage)}}" alt="" class="img-fluid imgClass" />
              </div>
              <div class="pod">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate1" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 mt-5">
          <div class="row">
            <div class="col-lg-6 mt-5">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate2" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate3" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 mt-5">
          <div class="row">
            <div class="col-lg-6 mt-5">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate4" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate5" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 mt-5">
          <div class="row">
            <div class="col-lg-6 mt-5">
              <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
                <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50" class="img-fluid" id="tokenRotate6" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="col-lg-12 col-md-12 col-sm-12 rewardID">
                <img src="{{url('public/images/reward.png')}}" alt="" height="70" width="70" class="img-fluid" id="tokenRotate7" />
              </div>
              <div class="pod2">
                <img src="{{url('public/background/'.$profile->podImage)}}" alt="" class="img-fluid imgClass2" />
              </div>
            </div>
          </div>

        </div>
      
      </div>
      <div class="row d-flex justify-content-end nextbackRow">
        <div class="col-1 float-left" onclick="onJumpBack()">
          <img src="{{url('public/images/back.png')}}" alt="" class="img-fluid" id="back" height="80" width="80" />
        </div>
        <div class="col-1 float-right" onclick="onJump()">
          <img src="{{url('public/images/forward.png')}}" alt="" class="img-fluid" height="75" width="75" id="next" />
        </div>
      </div>
    </div>
  </div>


  <div class="modal slide-up" id="modal" tabindex="-1">
    <div class="modal-dialog w-100 ">
      <div class="modal-content w-100 roundModal" style="background-color: #564428;">

        <div class="modal-body">
          <div class="row">
            <div class="col-2">
              <img src="public/images/fox.png" class="img-fluid" alt="">
            </div>
            <div class="col-7">
              <h3 class="font mt-3" style="color: #cbcb01;">WELL DONE, KAYLE</h3>
            </div>
            <div class="col-3">
              <img src="public/images/profile.png" class="img-fluid rounded-circle border border-white" alt="">
            </div>

          </div>
          <div class="row mt-1">
            <div class="col-4 mt-auto">
              <img src="public/images/fox.png" class="img-fluid" alt="">
            </div>
            <div class="col-8 mx-auto">
              <img src="public/images/weldetails.png" class="img-fluid" alt="" />
              <div class="row d-flex justify-content-center">

                <div class="btn btn-primary justify-content-center border-0 rounded m-1 px-4"
                  style="background-color: #7a9a34;">Share</div>

              </div>

            </div>

          </div>
          <div class="row mt-3">
            <div class="col-3"></div>
            <div class="col-6" style="height: 10px;background-color: grey;border-radius: 20px;">

            </div>
            <div class="col-3"></div>
          </div>

        </div>
      </div>
    </div>
  </div>


</body>


</html>


<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#edit', function(e){
 //alert(count);
$.ajax({
method:"GET",
data: 'id='+ count,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'updatesteps/'+count,
 
success:function(data){
//$("#modal_change_password").modal('show');
/* 
$('.id').val(data.id);
$('.backgroundName').val(data.backgroundName);*/
 //console.log(data);
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>





