  @include('layouts.header')
@section('content')

@endsection


<div class="container-fluid">
<div class="row mt-5">
<?php if($id == 'specialbuddytokens'){?>  
<img src="{{ asset('public/images/free resources/buddy token sheet.png')}} " class="img-fluid m-auto justify-content-center w-25 h-25" alt="">

<?php }
if($id == 'buddybucks'){?>  
<img src="{{ asset('public/images/free resources/buddy buck.png')}} " class="img-fluid m-auto justify-content-center w-25 h-25" alt="">

<?php }
if($id == 'coloringsheets'){?>  
<img src="{{ asset('public/images/free resources/buddy coloring at desk.png')}} " class="img-fluid m-auto justify-content-center w-25 h-25" alt="">



<?php }
?>
</div>
 <?php $pos=1; ?>
@foreach($list as $row)
<div class="row">
<div class="col-lg-3 col-md-1 col-sm-1"></div>
<div class="col-lg-4 col-md-9 col-sm-9">
<div class="mt-5">
<h6 class="popfont"><span class="popfont spanfont7"><?php print $pos++ ?> : </span> <span class="popfont spanfont7"> <b>{{$row->title}}</b></span></h6>
<p class="popfont spanfont7">{{$row->description}}</p>
</div>
</div>
<div class="col-2">
<div class=" mt-5 d-flex justify-content-center">
<a href="{{url('filedownload/'.$row->id)}}" ><i class="regbtn fa fa-download" aria-hidden="true"></i></a>
</div>
</div>
</div>
@endforeach
<div class="mt-5">

</div>



</div>
@include('layouts.footer')
@section('content')
@endsection
