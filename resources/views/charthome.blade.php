   @include('layouts.chartHeader')
<body>
<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
 <!-- <script src="bootstrap/js/bootstrap.js"></script> -->

<div class="row mr-2">
  <div class="" style="margin-left: 88%;">
    <img src="{{url('public/images/buddy token icon counter for profile page.png')}}" class="img-fluid float-right col-12" style="z-index: -1">
      
  </div>
    <div class="font charthome6" >
      <a href="{{url('/shops')}}">
      <span class="font-weight-bold h5">{{$coins}}</span>
      </a>
    </div>
    
</div>
  <div class="container-fluid">
   
  
<div class="container">

    <div class="row mt-5 fontsize2 table1">
        <div class="col-10">
        <div class="row">
    <div class="col-5 fontsize2" ><b>Behavior</b></div>
    <div class="col-4 fontsize2"><b>Reward</b></div>
    <div class="col-3 fontsize2"><b>Jumps Remaining</b></div>
  </div>
</div>
  <div class="col-2">
    <div class="col-12 fontsize2"><b>Options</b>
    </div>
    </div> 
  </div>
@foreach($chart as $row)
<div class="row mt-2 font3 table1">
  <div class="col-10 ">
  <div class="row mt-auto mb-auto d-block startbehaviour1">
<a class="col-12" href="{{url('/startover/'.$row->id)}}">
  <div class="row ">
<div class="col-5 font3" id="test">&nbsp;&nbsp;&nbsp;{{$row->behaviour}}</div>
<div class="col-4 font3" id="test1" >{{$row->reward}}</div>
<div class="col-3 font3" id="test2">
  <?php $jumps = $row->steps - $row->jumps;
  if($row->status == 1 && $row->jumps == 0)
  {
    echo "Completed";
  }
  else{
  echo $jumps;

  }
?></div>
 </div>
</a>
</div>
</div>
<div class="col-2 mt-auto mb-auto d-block">
  <div class="row ">
    
<div class="col-12 font3 dropdown m-auto a11">
<a href="#" class="a11 dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" title="">
<img src="{{ asset('public/images/Behavior Buddy token.png')}}" alt="logo" class="blogo2" >
<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
<a class="dropdown-item table1 item " href="{{url('/editchart/'.$row->id)}}">Edit</a>
<a class="dropdown-item table1" href="{{url('/deletechart/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete this ?')">Delete</a>
<?php 
 if($row->status == 1 && $row->jumps == 0 )
 {?>
  <a class="dropdown-item table1" href="{{url('/startover/'.$row->id)}}">Start Over</a>
 <?php }
 if($row->jumps == 0 && $row->status == 0)
  {?>
  <a class="dropdown-item table1" href="{{url('/startover/'.$row->id)}}">Start Chart</a>
 <?php }
 if($row->jumps != 0){?>
  <a class="dropdown-item table1" href="{{url('/startover/'.$row->id)}}">Resume</a>
 <?php }

?>


</div>

</a>

</div>
</div>
</div>
</div>
 
@endforeach
  </div> 
  </div>

 <div class="modal fade" id="thankyou">
    @if(session()->has('error'))
            <script type='text/javascript'>
            $(document).ready(function(){
            $('#thankyou').modal('show');
            });
            </script>
             
        @endif
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body p-5">
            <div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </div>
            <div class=" mt-2 ">
            <center> <span class="text-primary h4 font text-center">Thanks</span></center> 

            </div>
            <div class="mt-5" >
            <center>
              <span class="popfont text-center  ">The new shop item is added to your chart.</span>
            </center>  
            </div>
             
          </div>

        </div>
      </div>
  </div>


