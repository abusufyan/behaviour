   @include('layouts.header')
@section('content')

@endsection
<div class="container-fluid overflow-hidden">
<div class="row">
	<div class="imghght p-5">
	<span class="font2 font make1 text-center d-flex justify-content-center m-auto mtmob">Make good behavior fun at school and home!</span>
<div class="col-lg-6 col-md-12 col-sm-12 d-flex justify-content-center mtmob">
	<span class="popfont font2 indexfont mt-3">Get children motivated during this unique  school year with mybehaviorbuddy.com! Join for free today and begin creating  engaging and customized reward charts.</span>
</div>

<div class="col-lg-6 col-md-12 col-sm-12 mt-3 mb-5 d-flex justify-content-center">
<div class=""> 
   <input type="button" name="Logout" data-toggle="modal" data-target="#account" value="Get Started Now" class="btn11 popfont">
</div>

</div>
<div class="m-5">
	&nbsp;&nbsp;
</div><div class="m-5">
	&nbsp;&nbsp;
</div><div class="m-5">
	&nbsp;&nbsp;
</div>
</div>
</div>

<div class="row mt-3 text-center">

<div class="row mt-3 text-center ">
	<span class="popfont indexfont font2 text-center ml-5 mr-5 mtmob">Children are included in creating their reward charts.   We make it fun and easy for them to track their progress and see how close they are to earning their next reward! </span>
</div>

</div>

<div class="row mt-3 m-auto">
    
	<div  class="col-sm-12">
		<img src="public/images/mini chart.png" alt="" class="img-fluid w-100">
	</div>
	

</div>

<div class="row mt-3 m-auto">
	<div class="col-sm-12">
		<span class="fontsize popfont d-flex justify-content-center ">
		<ul class="popfont indexfont ">
			<li>Over 30 Buddies</li>
			<li>10 Background Themes</li>
			<li>Customizable Goals and Rewards</li>
			<li>Create Unlimited Reward Charts</li>
			<li>Unlock special buddies and more!</li>
		</ul>
		</span>
	</div>
</div>



<hr class="mt-2 hrd1">




<div class="row mt-5 second text-center bg-light p-3" id="learn">
<div class="container-fluid ">

<div id="carouselExampleIndicators" class="carousel slide slide2">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" class="bg-dark mt-5" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" class="bg-dark mt-5" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" class="bg-dark mt-5" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
<div class="row mt-4">
	<div class="col-lg-1 col-md-12 col-sm-12"></div>
	<div class="col-lg-6 col-md-12 col-sm-12 m-auto text-center" id="cmarg">
		<span class="font2 h2 text-center font mt-5 index1"><b>Getting Started With My Behavior Buddy</b> </span>
		<span class="popfont spanfont4 font2 pl-5 pt-3 pr-5 d-flex justify-content-center">
			Once you have created a reward chart, it’s time to begin awarding Buddy Tokens!  Buddy Tokens are earned each time improvement in behavior is demonstrated.  With every Buddy Token earned, each child moves one step closer to their next reward!  
		</span>
	</div>
	<div  class="col-lg-5 col-md-12 col-sm-12 ">
		<img src="https://behaviourbuddy.facebhoook.com/public/images/Android%20buddies%20holding%20coin.png" alt="" class=" img-fluid" style="height: 280px;">
	</div>	
</div>
    </div>

    <div class="carousel-item">
<div class="row mt-4">
		<div class="col-lg-1 col-md-12 col-sm-12"></div>
		<div class="col-lg-6 col-md-12 col-sm-12 m-auto text-center" id="cmarg">
		<span class="font2 text-center h2 font mt-5"><b>Class Management Made Simple</b> </span>
			<span class="spanfont4 font2 popfont pl-5 pt-3 pr-5 d-flex justify-content-center"> 
			Teachers have the option of creating individual reward charts for each student or having the class work as a group towards a classroom reward.  

		</span>
	</div>
	<div  class="col-lg-5 col-md-12 col-sm-12 ">
		<img src="public/images/dino typing at computer.png" alt="" style="height: 280px;" class="img-fluid">
	</div>


</div>


  </div>

  <div class="carousel-item">
  	<div class="row mt-4">
  			<div class="col-lg-1 col-md-12 col-sm-12"></div>
	<div class="col-lg-6 col-md-12 col-sm-12 m-auto text-center cmarg3" id="cmarg">
		<span class="font2 font h2 text-center mt-5"><b>Share the Certificate of Achievement!</b></span>
		<span class="popfont spanfont4 font2  pl-5 pt-3 pr-5 d-flex justify-content-center">
			Children love to be praised for their efforts, especially asit relates to good behavior!  Once a reward is earned, you will have the option of downloading a Certificate of  Achievement for your child, student or classroom.  

		</span>
	</div>
	<div class="col-lg-5 col-md-12 col-sm-12 ">
		<div>
			
		<img src="public/images/buddy logo holding reward certificate.png" style="height: 280px;" alt="" class="img-fluid">
		</div>

	</div>
</div>	

  </div>
  <a class="carousel-control-prev car1" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="fas fa-arrow-circle-left faicon1" aria-hidden="true"></span>
  </a>
  <a class="carousel-control-next car1" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="fas fa-arrow-circle-right  img-circle faicon1" aria-hidden="true"></span>
  </a>
</div>




</div>
</div>

</div>
</div>
@include('layouts.footer')
@section('content')
@endsection

<script>
	$("learnMore").click(function() {
    $('html,body').animate({
        scrollTop: $(".second").offset().top},
        'slow');
});
</script>



