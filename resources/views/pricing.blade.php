  @include('layouts.header')
<div class="container-fluid mt-5">
    
   <div class="row h-auto">
    <div class="col-lg-1 col-md-0 col-sm-0"></div>
  
    
    <div class="col-lg-3 col-md-5 col-sm-12 bgbr1 m-auto">

        <div class="bgbr2 p-3">
            
            <div class="font1 spanfont6 pt-3 text-white">{{$percentage->title}}</div>              
            <hr class="hrd2">
            <div class="popfont2 mt-5">
                <center> <p><strong class="spanfont6 popfont2">{{$percentage->price}}<br></strong></p></center>
                <p><strong class="popfont2 spanfont6">{{$percentage->percentage}}<br></strong></p>
                <span class="popfont2 spanfont6">{{$percentage->description}}</span>
                
            </div>
            <div class="mt-5 ">
                <a class="regbtn" href="{{url('/applycoupon/'.$percentage->id)}}" rel="nofollow">Apply Coupon</a>
            </div>
        </div>
    </div>


    <div class="col-lg-3 col-md-5 col-sm-12 bgbr1 m-auto">
        <div class="bgbr2 p-3">
            
            <div class="font1 spanfont6 pt-3 text-white">{{$fullaccess->title}}</div>
            <hr class="hrd2">
            <div class="popfont2 mt-5">
               <center>  
                    <p class=""><strong class="spanfont6 popfont2">{{$fullaccess->price}}</strong></p>
                </center>
                <p><strong class="spanfont6 popfont2">{{$fullaccess->percentage}}<br></strong></p>
                <p class="popfont2 spanfont6">{{$fullaccess->description}}</p>
                 
            </div>
            <div class="mt-5">
                <a class="regbtn" href="{{url('/payment')}}" rel="nofollow">Pay Now</a>
            </div>
        </div>
    </div>

</div> <!-- .et_pb_row -->


</div>