 @include('layouts.header')
@section('content')

@endsection

<div class="container-fluid mt-5">
<div class="row m-3">
<?php if($active == 1 || $user == 0)
{?>

<div class="col-lg-1 col-md-2 col-sm-3">
  <div class="" ><center><a href="" title="" data-toggle="modal" data-target="#addAvatar" ><div class="addprof text-center"><img src="public/images/aplus.png"  class="img-fluid p-3" alt="plus"></div></a><span class="spanfont6 text-center"><b>Add Profile</b></span></center></div>
</div>

<?php }
else{ ?>
<div class="col-lg-1 col-md-2 col-sm-3">
  <div class="" ><center><a href="" title="" data-toggle="modal" data-target="#premium" ><div class="addprof text-center"><img src="public/images/aplus.png"  class="img-fluid p-3" alt="plus"></div></a><span class="spanfont6 text-center"><b>Add Profile</b></span></center></div>
</div>

<?php }
?>

@foreach($profile as $row)
<div class="col-lg-1 col-md-2 col-sm-3">
 <div class=""><center><div class="addprof text-center"><a href="{{url('indexhome/'.$row->id)}}"> <img src="{{url('public/character/'.$row->characterImage)}}" class="img-fluid" alt="plus"></a></div><span class="text-center spanfont6"><b>{{$row->childName}}</b></span></center></div>
</div>
@endforeach
  
</div>
<?php if($active == 0)
{?>

<div class="row" style="background-color: #edffd78f;">

<div class="col-6">
<img src="public/images/buddy logo holding box of stuff.png" class="img-fluid w-75">
</div>
<div class="col-6 mt-5">

<span class="font spanfont3 mt-5 font-weight-bold">
Go Premium to Get <br> <br>
</span>
<span class="popfont  spanfont3 mt-5">
 
&bull; Unlimited Profiles <br> <br>
&bull; Unlimited Reward Charts <br> <br>
&bull; Access to Premium Buddies And Backgrounds <br> <br>
</span>
<a href="{{url('pricing')}}" title="" class="regbtn1 h5 mt-2">Upgrade to Premium</a>
</div>

</div>
<?php }
?>
</div>
<!-- Add Avatar Image  -->
<div class="modal fade" id="addAvatar">
<div class="modal-dialog mdldialog">
<div class="modal-content smalscrn2">
<div class="modal-body mdlbdy">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true" class="h12">&times;</span>
            </div>
            <div class="mt-5">
<center>
              <span class="text-center h12 font centertext">ADD A PROFILE</span>
</center>
            </div>
            <form action="{{url('/insertprofile')}}" method="Post">  
            {{ csrf_field() }}

          <div class="m-3 mt-2">
          <input type="text" name="childName" placeholder="Child's name" class=" iptx validate" required>
          <input type="hidden" name="userId" value="{{Auth::user()->id}}">
          </div>
        <div class="ex1 mt-3 mb-5">
        <div class="row m-auto">
        @foreach($character as $row)
        <div class="col-lg-3 col-md-3 col-sm-3 product-chooser">
          <div class="product-chooser-item">
            <img src="{{url('public/character/'.$row->characterImage)}}" class="addimg1 addimg"  id="addimg" data-img-src="{{url('public/character'.$row->characterImage)}}" value="{{$row->id}}" name="characterId" data-id="{{$row->id}}" data-img-class="first" alt="plus">
            <input type="radio" name="characterId" value="{{$row->id}}" checked="checked">
          </div>
        </div>
              <div class="clear"></div>


        @endforeach

        </div>
        </div>
    

        <div class="m-auto mt-5 row">
        <button type="Reset" class="cnclbtn col-5 p-2">Cancel</button>
        <div class="col-2">

        </div>
        <input type="submit" value="ADD PROFILE" class="regbtn2 col-5 p-2">


        </div>
        </form>
          </div>

        </div>
      </div>
    </div>

<div class="modal fade" id="premium">
<div class="modal-dialog mdldialog2">
<div class="modal-content smalscrn3">
<div class="modal-body mdlbdy">
          <div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true" class="h12">&times;</span>
            </div>
            <div class="row mt-5">
<span class="text-center h12 font ml-5 text-danger">Try Premium Account</span>
              
            </div>
          <div class="m-4" >
           <span class="popfont spanfont mt-5">Please try premium account to add multiple profile's</span>
           </div>
          <div class="m-5">
<a href="{{url('pricing')}}" title="" class="regbtn1 h5 mt-5">Upgrade to Premium</a>
            
          </div>
          </div>

        </div>
      </div>
    </div>

<script>
        $(function(){
  $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
    $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
    $(this).addClass('selected');
    $(this).find('input[type="radio"]').prop("checked", true);

    console.log(document.getElementsByClassName("selected"));
  });
});
</script>
</body>
</html>
