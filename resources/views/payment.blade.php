@include('layouts.header')
 
<style type="text/css">
  .submit-button {
  margin-top: 10px;
}
</style>
<div class="container mt-5">
    <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4'>
          <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
          <form accept-charset="UTF-8" action="{{url('/confirm')}}" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="pk_test_51HKICwI0QL79fDpaRWNtJ1SobDcVT5bjBV4JYlbMqgk4Eb6ikNJgjfNTcO3s5pwmDGCAZS63wKBaHDsTXZ5uf5AU006dh5hGHl" id="payment-form" method="post">
                      {{ csrf_field() }}

            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓" /> <input name="authenticity_token" type="hidden" value="qLZ9cScer7ZxqulsUWazw4x3cSEzv899SP/7ThPCOV8=" /></div>
            <div class='form-row'>
              <div class='col-md-12 col-xs-12 form-group required'>
                <label class='control-label font'>Name on Card</label>
                <input class='form-control' type='text'>
              </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 col-xs-12 form-group required'>
                <label class='control-label font'>Card Number</label>
                <input autocomplete='off' class='form-control card-number' type='text'>
              </div>
            </div>
            <div class='form-row'>
              <div class='col-md-4 col-xs-4 form-group cvc required'>
                <label class='control-label font'>CVC</label>
                <input autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text'>
              </div>
              <div class='col-xs-4 col-md-4 form-group expiration required'>
                <label class='control-label font'>Expiration</label>
                <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
              </div>
              <div class='col-md-4 col-xs-4 form-group expiration required'>
                <label class='control-label font'> </label>
                <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
              </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 '>
                <div class='form-control total btn btn-info regbtn'>
                  Total:
                  <span class='amount'><?php 
                           $price= Session::get('price');
                           echo $price;
                  ?></span>
                </div>
              </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 form-group'>
                <input type="submit" value="Submit" class='regbtn2 submit-button' >
               </div>
            </div>
            <div class='form-row'>
              <div class='col-md-12 error form-group hide'>
                <div class='alert-danger alert'>
                  Please correct the errors and try again.
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class='col-md-4'></div>
    </div>
</div>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
  $(function() {
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(e.target).closest('form'),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;

    $errorMessage.addClass('hide');
    $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault(); // cancel on first error
      }
    });
  });
});

$(function() {
  var $form = $("#payment-form");

  $form.on('submit', function(e) {
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  });

  function stripeResponseHandler(status, response) {
    if (response.error) {
      $('.error')
        .removeClass('hide')
        .find('.alert')
        .text(response.error.message);
    } else {
      // token contains id, last4, and card type
      var token = response['id'];
      // insert the token into the form so it gets submitted to the server
      $form.find('input[type=text]').empty();
      $form.append("<input type='hidden' name='reservation[stripe_token]' value='" + token + "'/>");
      $form.get(0).submit();
    }
  }
})
</script>