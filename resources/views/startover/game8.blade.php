<!DOCTYPE html>
<html>

<head>
    <title>Behavior</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{url('public/js/jquery.fireworks.js')}}"></script>
    <link rel="stylesheet" href="{{url('public/css/demopod8.css')}}">
    <?php  $jumps =$profile->jumps; ?>
        <?php  $chartid =$profile->id; ?>

    <script type="text/javascript">
        var jumps = "<?= $jumps ?>";
        var chartid = "<?= $chartid ?>";

    </script>
    
    <script src="{{url('public/js/demopod8.js')}}"></script>




</head>
<?php $character = Session::get('character');
$pieces = explode(" ", $profile->childName);


 ?>

<body>
    <div class="fullscreen">
        <div id="avatar">
        <div class="containerabc">
                <div class="row">
                <div class="d-flex bg1 flex-row">
                        <img src="{{url('public/character/'.$character->characterImage)}}" width="60px"
                            class="img-fluid pl-2 charcAvatar" />
                        <div class="text-white chracm">
                            <h5 class="font chracText">{{$pieces[0]}}</h5>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mr-2 d-none">
                <div class="" style="z-index: 9999;margin-left: 5%;">
                    <img src="{{url('public/images/buddy token icon counter for profile page.png')}}" height="60"
                        width="120">
                    <input name="count" type="text" class="h5" id="count"
                        style="background-color:transparent;border: 0;color: blue;margin-left: -25%;" />

                </div>

            </div>
        </div>
        <div id="home">
            <a href="{{url('/accounthome')}}">
                <div class="d-flex justify-content-center">
                    <img src="{{url('public/images/Home button for charts.png')}}" class="homeButton" height="75px" width="150px" />

                </div>

            </a>
            <!-- <div class="h3 text-white">Back</div> -->
        </div>
        <?php $bgimg = "public/background/$profile->backgroundImage" ?>

        <div class="tech-slideshow  SmallWid" id="BG" style="background-image: url('<?php echo url($bgimg);?>');
          ">

            <div class="container-fluid SmallWid" id="containerrow">
                <!-- <div class="row mt-2"> -->

                <!--</div>-->

                <audio src="{{url('public/audio/jump.mp3')}}" id="myAudio" style="visibility: hidden">
                    <!-- <source
                src="images/audio/jump.mp3"
                id="myAudio"
                type="audio/ogg"
              /> -->
                </audio>

                <audio src="{{url('public/audio/celebrate.mp3')}}" id="myAudio2" style="visibility: hidden">
                    <!-- <source
                src="images/audio/jump.mp3"
                id="myAudio"
                type="audio/ogg"
              /> -->
                </audio>


                <div class="row marginPods">
                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-8 object" id="foxID">
                                    <img src="{{url('public/avatar/'.$profile->avatarImage)}}" alt=""
                                        class="img-fluid imgClass" />
                                </div>
                                <div class="pod">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate1" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate2" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate3" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate4" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate5" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="tokenRotate6" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/reward/'.$profile->rewardImage)}}" alt="" height="80"
                                        width="80" class="img-fluid" id="tokenRotate7" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>



                </div>

            </div>
        </div>

        <div class="row mt-5  nextbackRow d-flex justify-content-end">
            <!-- <div class="col-lg-6 col-md-3"></div> -->
            <div class="" onclick="onJumpBack()" id="minuscoin">
                <img src="{{url('public/images/back.png')}}" alt="" class="img-fluid" height="60" width="60"
                    id="back" />
            </div>
            <div class="col-1" onclick="onJump()" id="pluscoin">
                <img src="{{url('public/images/forward.png')}}" alt="" class="img-fluid" height="60" width="60"
                    id="next" />
            </div>
        </div>

    </div>



    <div class="SmallScreen">

        <div id="avatar">
        <div class="containerabc">
                <div class="row">
                <div class="d-flex bg1 flex-row">
                        <img src="{{url('public/character/'.$character->characterImage)}}" width="60px"
                            class="img-fluid pl-2 charcAvatar" />
                        <div class="text-white chracm">
                            <h5 class="font chracText">{{$pieces[0]}}</h5>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mr-2 d-none">
                <div class="" style="z-index: 9999;margin-left: 5%;">
                    <img src="{{url('public/images/buddy token icon counter for profile page.png')}}" height="60"
                        width="120">
                    <input name="count" type="text" class="h5" id="count-sm"
                        style="background-color:transparent;border: 0;color: blue;margin-left: -25%;" />

                </div>

            </div>
        </div>
        <div id="home">
            <a href="{{url('/accounthome')}}">
                <div class="d-flex justify-content-center">
                    <img src="{{url('public/images/Home button for charts.png')}}" class="" width="250px" />

                </div>

            </a>
            <!-- <div class="h3 text-white">Back</div> -->
        </div>


        <!-- ---------------------PODS-------------------------------- -->


        <div class="tech-slideshow  SmallWid" id="sm-BG"
            style="background-image: url('<?php echo url($bgimg);?>');background-repeat: no-repeat !important;">

            <div class="container-fluid SmallWid" id="containerrow">
                <audio src="{{url('public/audio/jump.mp3')}}" id="myAudio-sm" style="visibility: hidden"></audio>
                <audio src="{{url('public/audio/celebrate.mp3')}}" id="myAudio2-sm" style="visibility: hidden"></audio>

                <div class="row marginPods">
                    <div class="col-3 ">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-8 object" id="foxID-sm">
                                    <img src="{{url('public/avatar/'.$profile->avatarImage)}}" alt=""
                                        class="img-fluid imgClass" />
                                </div>
                                <div class="pod">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>
                            <div class="col-6 ">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate1" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate2" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6 ">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate3" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate4" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6 ">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate5" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-3">
                        <div class="row">
                            <div class="col-6 mt-5">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/images/token.png')}}" alt="" height="50" width="50"
                                        class="img-fluid" id="sm-tokenRotate6" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="col-12 tokenID">
                                    <img src="{{url('public/reward/'.$profile->rewardImage)}}" alt="" height="50"
                                        width="50" class="img-fluid" id="sm-tokenRotate7" />
                                </div>
                                <div class="pod2">
                                    <img src="{{url('public/background/'.$profile->podImage)}}" alt=""
                                        class="img-fluid imgClass2" />
                                </div>
                            </div>

                        </div>

                    </div>



                </div>


            </div>
        </div>


        <!-- ------------------------------END_PODS------------------------------------ -->

        <div class="row nextbackRow d-flex justify-content-end">
            <!-- <div class="col-lg-6 col-md-3"></div> -->
            <div class="" onclick="onJumpBack2()" id="mubminuscoin">
                <img src="{{url('public/images/back.png')}}" alt=""  height="100" 
                    id="back-sm" />
            </div>
            <div class="col-1" onclick="onJump2()" id="mubpluscoin">
                <img src="{{url('public/images/forward.png')}}" alt=""  height="100" 
                    id="next-sm" />
            </div>
        </div>
    </div>

    <div class="modal slide-up" id="modal" tabindex="-1">
        <div class="modal-dialog w-100 ">
            <div class="modal-content w-100 roundModal" style="background-color: #564428;">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{url('public/avatar/'.$profile->avatarImage)}}" class="img-fluid" alt="">
                        </div>
                        <div class="col-7">
                            <h3 class="font mt-3" style="color: #cbcb01;">welcome back {{$profile->childName}}</h3>
                        </div>
                        <div class="col-3">
                            <img src="{{url('public/reward/'.$profile->rewardImage)}}"
                                class="img-fluid rounded-circle border border-white" alt="">
                        </div>

                    </div>
                    <div class="row mt-1">
                        <div class="col-4 mt-auto">
                            <img src="{{url('public/avatar/'.$profile->avatarImage)}}" class="img-fluid" alt="">
                        </div>
                        <div class="col-8 mx-auto">

                            <div class="row d-flex justify-content-center">

                                <div class="btn btn-primary justify-content-center border-0 rounded m-1 mt-5 px-4"
                                    style="background-color: #7a9a34;" onclick="resumeGame()">Let's Play Again</div>

                            </div>

                        </div>

                    </div>
                    <div class="row mt-3">
                        <div class="col-3"></div>
                        <div class="col-6" style="height: 10px;background-color: grey;border-radius: 20px;">

                        </div>
                        <div class="col-3"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>



</body>


</html>





<script type="text/javascript">
    $(document).ready(function () {
        document.getElementById("BG").style.height = screen.height + 'px';
        $("body").on("contextmenu", function (e) {
            return false;
        });
        if (jumps > 0) {
            $('#modal').show();
        }
        $(document).on('click', '#mubpluscoin', function (e) {
            counts = $('#count-sm').val();
            //alert(counts);


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: {
                    counts: counts,
                },

                url: "{{ url('updatesteps/') }}",

                success: function (data) {


                    console.log(data);

                },
                error: function () {
                    toastr.error('Error!',
                        'Something Went Wrong. Please try again later. If the issue persists contact support.', {
                            "positionClass": "toast-bottom-right"
                        });

                }
            });

        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {

        $(document).on('click', '#pluscoin', function (e) {
            counts = $('#count').val();
            //alert(counts);


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: {
                    counts: counts,
                },

                url: "{{ url('updatesteps/') }}",

                success: function (data) {


                    console.log(data);

                },
                error: function () {
                    toastr.error('Error!',
                        'Something Went Wrong. Please try again later. If the issue persists contact support.', {
                            "positionClass": "toast-bottom-right"
                        });

                }
            });

        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function () {

        $(document).on('click', '#minuscoin', function (e) {
            counts = $('#count').val();
            //alert(counts);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: {
                    counts: counts,
                },
                url: "{{ url('reversesteps/') }}",

                success: function (data) {

                    console.log(data);
                },
                error: function () {
                    toastr.error('Error!',
                        'Something Went Wrong. Please try again later. If the issue persists contact support.', {
                            "positionClass": "toast-bottom-right"
                        });

                }
            });

        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {

        $(document).on('click', '#mubminuscoin', function (e) {
            counts = $('#count-sm').val();
            //alert(counts);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: {
                    counts: counts,
                },
                url: "{{ url('reversesteps/') }}",

                success: function (data) {

                    console.log(data);
                },
                error: function () {
                    toastr.error('Error!',
                        'Something Went Wrong. Please try again later. If the issue persists contact support.', {
                            "positionClass": "toast-bottom-right"
                        });

                }
            });

        });
    });

</script>
