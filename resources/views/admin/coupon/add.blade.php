@extends('layouts.adminhead')

@section('content')
            
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="{{url('/insertcoupon')}}" method="post">
                                                      {{ csrf_field() }}
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add new deal</strong></h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                 
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="title" required>
                                            </div>                                            
                                            <span class="help-block">This is Title of deal</span>
                                        </div>
                                    </div>
                                     <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Price</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="price"required>
                                            </div>                                            
                                            <span class="help-block">This is price of deal</span>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group"  >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Description</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-edit"></span></span>
                                                <input type="text" class="form-control" name="description" required>
                                            </div>            
                                            <span class="help-block">Description</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group"  >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Coupon Code</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-text"></span></span>
                                                <input type="text" class="form-control" name="code" required>                                            
                                            </div>
                                            <span class="help-block">Coupen Code</span>
                                        </div>
                                    </div>
                                       <div class="form-group"  >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Start Date</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="startDate">                                            
                                            </div>
                                            <span class="help-block">Start Date of Coupon</span>
                                        </div>
                                    </div>
                                       <div class="form-group"  >                                        
                                        <label class="col-md-3 col-xs-12 control-label">End Date</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="endDate" required>                                            
                                            </div>
                                            <span class="help-block">Ending Date of Coupon</span>
                                        </div>
                                    </div>
                                     
                                    
                                    <div class="form-group"  >
                                        <label class="col-md-3 col-xs-12 control-label">Select Type</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" id="test" name="type" required>
                                                <option value="full">Full Access</option>
                                                <option value="%age">%age off</option>
                                                
                                            </select>
                                            <span class="help-block">Select box example</span>
                                        </div>
                                    </div> 
                                    <div class="form-group" id="percentage" style="display: none;" >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Percentage</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-text"></span></span>
                                                <input type="text" name="percentage" class="form-control ">                                            
                                            </div>
                                            <span class="help-block">Enter off %age</span>
                                        </div>
                                    </div>

                                    <div class="form-group"  >
                                        <label class="col-md-3 col-xs-12 control-label">For</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" id="for" name="for" required>
                                                <option value="all">All</option>
                                                <option value="specific">Specific</option>
                                            
                                            </select>
                                            <span class="help-block">This Coupon is for all or some specific person</span>
                                        </div>
                                    </div>
                                     <div class="form-group" id="email" style="display: none;" >
                                        <label class="col-md-3 col-xs-12 control-label">Email</label>
                                        <div class="col-md-6 col-xs-12">                                               
                                            <input type="email" name="email" class="form-control ">
                                             
                                            <span class="help-block">Please enter email of user who can avail this coupon</span>
                                        </div>
                                    </div>
                          
                                </div>
                                <div class="panel-footer">
                                    <input type="Submit" value="Submit" class="btn btn-primary pull-right">
                                 </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                                                           
            </div>            
         </div>
         
       

<script type="text/javascript">
    $('#test').on('change', function() {
 // alert( 'this.value' ); // or $(this).val()
  if(this.value == "%age") {
     $('#percentage').show();
   } else {
     $('#percentage').hide();
   }
});
  $('#for').on('change', function() {
 // alert( 'this.value' ); // or $(this).val()
  if(this.value == "specific") {
      $('#email').show();
   } else {
     $('#email').hide();
   }
});  
</script>





@endsection





