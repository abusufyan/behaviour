@extends('layouts.adminhead')

@section('content')
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Coupen's & Deals </h2>
                </div>
                <!-- END PAGE TITLE -->                
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                 
                    <div class="row">
                        <div class="col-md-12">
                            
                             <div class="panel panel-default">
                                <div class="panel-heading">
                                     <div class="btn-group pull-right">
                                        <a href="{{url('/addcoupon')}}">
                                        <button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
                                    </a> 
                                    </div>                                    
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                     <th>Title</th>
                                                     <th>Type</th>
                                                     <th>Coupon</th>
                                                     <th>Date</th>
                                                    <th>Delete</th>
                                                     <th>Edit</th>             
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @foreach($coupon as $row)
                                                <tr>
                                                    <td>{{$row->title}}</td>
                                                    <td>{{$row->type}}</td>
                                                     <td>{{$row->code}}</td>

                                                    <td>{{$row->created_at}}</td>
                                                    <td><a href="{{url('/deletecoupon/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                    <td>
                                                      <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product edit" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                     
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </div>         
             
            </div>            
         </div>

       <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update</h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 

                <form class="form-horizontal" action="{{url('updatecoupon/')}}" method="Post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label avatarName">Titile</label>
                <div class="col-md-9">
                <input type="text" class="form-control title" name="title" value="" />
                </div>
                </div>
                <div class="form-group">
                 <label class="col-md-3 control-label avatarName">Price</label>
                <div class="col-md-9">
                <input type="text" class="form-control price" name="price" value="" />
                </div>
                </div>
                 <div class="form-group">
                <label class="col-md-3 control-label avatarName">Description</label>
                <div class="col-md-9">
                <input type="text" class="form-control description" name="description" value="" />
                </div>
                </div>
                <div class="form-group">  
                <label class="col-md-3 control-label avatarName">Code</label>
                <div class="col-md-9">
                <input type="text" class="form-control code" name="code" value="" />
                </div>
                </div>
                <div class="form-group">   
                <label class="col-md-3 control-label avatarName">Type</label>

                <div class="col-md-9">
                <select class="form-control select type" id="test" name="type">
<!--                 <option value="free">Free</option>
-->                <option value="%age">%age off</option>
                <option value="full">Full Access</option>

                </select>
                 </div>
                </div>
                <div class="form-group">   
                <label class="col-md-3 control-label avatarName">For</label>
                <div class="col-md-9">
                <select class="form-control select for" id="for" name="for">
                <option value="all">All</option>
                <option value="specific">Specific</option>

                </select>
                <span class="help-block">This Coupon is for all or some specific person</span>
                </div>
                </div>
                <div class="form-group" id="email" style="display: none;" >
                <label class="col-md-3 control-label avatarName">Email</label>
                <div class="col-md-9">                                               
                <input type="email" name="email" class="form-control email">

                <span class="help-block">Please enter email of user who can avail this coupon</span>
                </div>
                </div>
                 <div class="form-group" id="percentage" style="display: none;">                
                <label class="col-md-3 control-label avatarName">Percentage</label>
                <div class="col-md-9">
                <input type="text" class="form-control percentage" name="percentage" value="" />
                </div>
                </div>
                <div class="form-group">                
                <label class="col-md-3 control-label avatarName">Start Date</label>
                <div class="col-md-9">
                <input type="date" class="form-control startDate" name="startDate" value="" />
                </div>
                </div>
                <div class="form-group">                 
                <label class="col-md-3 control-label avatarName">End Date</label>
                <div class="col-md-9">
                <input type="date" class="form-control endDate" name="endDate" value="" />
                </div>
                </div>
                 
                </div>
                <div class="modal-footer">
                <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
            </div>
        </div>        
               
  

<script type="text/javascript">

$(document).ready(function(){
 
$(document).on('click', '#edit', function(e){
data =$(this).data('id');
 //alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'editcoupon/'+data,
 
success:function(data){
$("#modal_change_password").modal('show');
$('.id').val(data.id);
$('.title').val(data.title);
$('.price').val(data.price);
$('.description').val(data.description);
$('.code').val(data.code);
$('.type').val(data.type);
$('.percentage').val(data.percentage);
$('.startDate').val(data.startDate);
$('.endDate').val(data.endDate);
$('.for').val(data.for);
$('.email').val(data.email);

// console.log(data);
 
},
 
});

});
});


</script>
<script type="text/javascript">
    $('#test').on('change', function() {
 // alert( 'this.value' ); // or $(this).val()
  if(this.value == "%age") {
     $('#percentage').show();
   } else {
     $('#percentage').hide();
   }
});
  $('#for').on('change', function() {
 // alert( 'this.value' ); // or $(this).val()
  if(this.value == "specific") {
      $('#email').show();
   } else {
     $('#email').hide();
   }
});  
</script>

@endsection











