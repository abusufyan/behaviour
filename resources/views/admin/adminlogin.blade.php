<!DOCTYPE html>
<html lang="en" class="body-full-height">
 
<head>        
        <!-- META SECTION -->
        <title>My Behaviour Buddy</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
            
        <link rel="stylesheet" type="text/css" id="theme" href="public/admin/css/theme-default.css"/>
                                           
    </head>
    <body>
        
        <div class="login-container login-v2">
            
            <div class="login-box animated fadeInDown">
                <div class="login-body">
                     @if(session('message'))
                    <p class="alert alert-danger">
                    {{session('message')}}</p>
                    @endif
                    <center class="mt-3 mb-4">
                   <img src="{{ asset('public/images/buddy logo transparent background.png') }}" alt="logo" class="img-fluid" style="width: 100px; height: 100px;"></center>
                   
            
                    <form action="{{url('/adminsignin')}}"  class="form-horizontal" method="post">

                                              {{ csrf_field() }}

                    <div class="form-group" style="margin-top:30px;">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-user"></span>
                                </div>
                                <input type="text" class="form-control" name="username" placeholder="Username"/ required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="fa fa-lock"></span>
                                </div>                                
                                <input type="password" class="form-control" name="password" placeholder="Password"/ required>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-12">
                            <button class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2020 My Behaviour Buddy 
                    </div>
                     
                </div>
            </div>
            
        </div>
        
    </body>
 
</html>






