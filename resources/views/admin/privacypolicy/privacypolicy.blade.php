@extends('layouts.adminhead')

@section('content')
             <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
             
            
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="{{url('updateprivacypolicy')}}" method="Post" enctype="multipart/form-data">
                      {{ csrf_field() }}
                           <input type="hidden" name="id" value="1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Privacy policy</strong></h3>
                                    
                                @if(session('message'))
                                <p class="alert alert-warning">
                                {{session('message')}}</p>
                                @endif
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                   
                                    
                                    <div class="form-group" >                                        
                                         <div class="col-md-12 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-edit"></span></span>
                                                     
                                                   
                                                <textarea class="form-control" name="description" rows="20">@foreach($ploicy as $row)
                                                          {{$row->description}}
                                                          @endforeach                            
                                              </textarea>
                                            </div>            
                                         </div>
                                    </div>
                                     

                                </div>
                                <div class="panel-footer">
                                     <button class="btn btn-primary pull-right">Update</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                                                           
            </div>            
         </div>
 
 

@endsection

