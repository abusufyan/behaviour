@extends('layouts.adminhead')

@section('content')
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> BackgroundImage Lists</h2>
                </div>
                <!-- END PAGE TITLE -->                
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <div class="btn-group pull-right">
                                        <a href="{{url('/addbackground')}}">
                                        <button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
                                    </a> 
                                    </div>                                    
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>                                                                                   <th>Type</th> 
                                                    <th>Background Image</th>
                                                    <th>Pod Image</th>
                                                     <th>Date</th>
                                                    <th>Delete</th>
                                                   <th>Edit</th>
              
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @foreach($background as $row)
                                                <tr>
                                                    <td>{{$row->backgroundName}}</td>
                                                    <td>{{$row->type}}</td>
                                
                                                    <td><img src="{{url('public/background/'.$row->backgroundImage)}}" style="height:70px; width: 300px;"></td>
                                                    <td><img src="{{url('public/background/'.$row->podImage)}}" style="height:50px;"></td>
 
                                                    <td>{{$row->created_at}}</td>
                                                    <td><a href="{{url('/deletebackground/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                    <td>

                                                    <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a>

                                                    </td>
                                                     
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    
 
 
<div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update</h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 

                <form class="form-horizontal" action="{{url('updatebackground/')}}" method="Post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label avatarName">Background Name</label>
                <div class="col-md-9">
                <input type="text" class="form-control backgroundName" name="backgroundName" value="" />
                </div>
                </div>
                 
                <div class="form-group">
                <label class="col-md-3 control-label">Background Image</label>
                <div class="col-md-9">
                <input type="file" class="form-control" name="backgroundImage"/>
                </div>
                </div>

                <div class="form-group">
                <label class="col-md-3 control-label"> Pod Image</label>
                <div class="col-md-9">
                <input type="file" class="form-control" name="podImage"/>
                </div>
                </div>

                <div class="form-group">
 
                <label class="col-md-3 control-label avatarName">Type</label>
                <div class="col-md-9">

                <select class="form-control select type" id="test" name="type" required>
                <option value="free">Free</option>
                <option value="shop">Shop</option>

                </select>                
                </div>
                </div>

                <div class="form-group" id="price" style="display: none;" >                                        
                <label class="col-md-3 col-xs-12 control-label">Price</label>
                <div class="col-md-6 col-xs-12">
                <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-text"></span></span>
                <input type="number" name="price" class="form-control price ">                                            
                </div>
                <span class="help-block">Enter Price of Buddy</span>
                </div>
                </div>
                        

                </div>
               
                <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              
                </div>
                </form>
            </div>
        </div>        
        

 <script type="text/javascript">
    $('#test').on('change', function() {
//alert( 'this.value' ); // or $(this).val()
  if(this.value == "shop") {
     $('#price').show();
   } else {
     $('#price').hide();
   }
});
</script>
<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#edit', function(e){
data =$(this).data('id');
// alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'editbackground/'+data,
 
success:function(data){
     $("#modal_change_password").modal('show');
   
 $('.id').val(data.id);
$('.backgroundName').val(data.backgroundName);
$('.type').val(data.type);
$('.price').val(data.price);

 //console.log(data);
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>


@endsection


 








