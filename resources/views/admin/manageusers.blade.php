@extends('layouts.adminhead')

@section('content')
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal">
                                                            
                                <div class="panel panel-default tabs">                            
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Free Users</a></li>
                                        <li><a href="#tab-second" role="tab" data-toggle="tab">Discounted Users</a></li>
                                        <li><a href="#tab-third" role="tab" data-toggle="tab">Full Access Users</a></li>
                                    </ul>
                                    <div class="panel-body tab-content">
                                        <div class="tab-pane active" id="tab-first">
                                            <h2>Free Users</h2>
                                             
                                    <div class="panel-body mt-5">
                                        <div class="table-responsive">
                                            <table id="customers2" class="table datatable">
                                          <table id="customers2" class="table datatable">
                                                    <thead>
                                                        <tr>
                                                        <th>Email </th>
                                                        <th>Account Status</th>
                                                        <th>Date</th>
                                                        <th>Delete</th>
                                                        <th>Status</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($free as $row)
                                                       <tr>
                                                        <td>{{$row->email}}</td>
                                                        <td><?php 
                                                              if($row->active == 0)
                                                              echo 'Inactive';
                                                              else{
                                                            echo'Active';  
                                                              }   
                                                            ?></td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td> <a href="{{url('/deleteuser/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                         <td> <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a></td>
                                                       </tr>
                                                       @endforeach
                                                     </tbody>
                                                    </table>                                   
                                        </div>
                                    </div> 
                                            
                                        </div>
                                        <div class="tab-pane" id="tab-second">
                                               <h2>Discounted Users</h2>

                                            
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                           <table id="customers2" class="table datatable">
                                                    <thead>
                                                        <tr>
                                                        <th>Email </th>
                                                        <th>Account Status</th>
                                                        <th>Payment Status</th>
                                                        <th>Date</th>
                                                         <th>Delete</th>
                                                        <th>Status</th>
 
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($discount as $row)
                                                       <tr>
                                                        <td>{{$row->email}}</td>
                                                        <td><?php 
                                                              if($row->active == 0)
                                                              echo 'Inactive';
                                                              else{
                                                            echo'Active';  
                                                              }   
                                                            ?>
                                                        </td>
                                                        <td><?php 
                                                              if($row->active == 0)
                                                              echo 'Not Done';
                                                              else{
                                                            echo'Done';  
                                                              }   
                                                            ?></td>
                                                        <td>{{$row->created_at}}</td>
                                                         <td> <a href="{{url('/deleteuser/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                         <td> <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a></td>
                                                       </tr>
                                                       @endforeach
                                                     </tbody>
                                             </table>                                  
                                        </div>
                                    </div>
                                            
                                        </div>                                        
                                        <div class="tab-pane" id="tab-third">
                                              <h2>Full Access Users</h2>

                                            
                                           <div class="panel-body">
                                                <div class="table-responsive">
                                                   <table id="customers2" class="table datatable">
                                                        <thead>
                                                            <tr>
                                                            <th>Email </th>
                                                             <th>Account Status</th>
                                                            <th>Payment Status</th>
                                                            <th>Date</th>
                                                             <th>Delete</th>
                                                        <th>Status</th>
     
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($full as $row)
                                                           <tr>
                                                            <td>{{$row->email}}</td>
                                                         <td><?php 
                                                              if($row->active == 0)
                                                              echo 'Inactive';
                                                              else{
                                                            echo'Active';  
                                                              }   
                                                            ?>
                                                        </td>    
                                                            <td><?php 
                                                              if($row->active == 0)
                                                              echo 'Not Done';
                                                              else{
                                                            echo'Done';  
                                                              }   
                                                            ?></td>
                                                            <td>{{$row->created_at}}</td>
                                                             <td> <a href="{{url('/deleteuser/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                         <td> <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a></td>
                                                           </tr>
                                                           @endforeach
                                                         </tbody>
                                                    </table>                                      
                                                </div>
                                           </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>                                
                            
                             
                        </div>
                    </div>                    
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
     

<div class="modal animated fadeIn" id="modal_change" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update User </h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 
                <form class="form-horizontal"  method="post" action="{{url('updateuser')}}" >

                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id">

                <label class="col-md-3 control-label avatarName">Type</label>
                <div class="col-md-9">
                <select class="form-control select type" id="test" name="type">
                <option value="free">Free</option>
                <option value="%age">Discounted</option>
                <option value="full">Full Access</option>
                </select> 
               <span class="help-block">Update User Type </span>

                </div>
                </div> 
                <div class="form-group">
                 <label class="col-md-3 control-label avatarName">Status</label>
                    <div class="col-md-9">
                    <select class="form-control select active" id="test" name="active">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                    </select>
                     <span class="help-block">Update Account Status </span>
  
                    </div> 
                    </div>
                </div>
               
                <div class="modal-footer">
                  <input type="submit" class="btn btn-danger" name="" value="Update">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
               
               
               
            </div>
        </div> 
        </div>       
        

<script type="text/javascript">

      $(document).ready(function(){

      $(document).on('click', '#edit', function(e){
      data =$(this).data('id');
      // console($(this).data('id'));
      $.ajax({
      method:"GET",
      data: 'id='+ data,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      modal: true,
      url:'edituser/'+data,
      success:function(data){
           $("#modal_change").modal('show');
         
       $('.id').val(data.id);
      $('.email').val(data.email);
      $('.type').val(data.type);
      $('.active').val(data.active);
      //console.log(data);
       
      },
      error: function()
      {
      toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

      }
      });

      });
      });


</script>


    @endsection
