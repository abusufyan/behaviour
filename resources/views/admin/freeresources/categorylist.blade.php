@extends('layouts.adminhead')
@section('content')

<!-- PAGE TITLE -->
<div class="page-title">
<h2><span class="fa fa-arrow-circle-o-left"></span> Free Resources Lists</h2>
</div>
<!-- END PAGE TITLE -->
@if(session('message'))
<p class="alert alert-warning">
{{session('message')}}</p>
@endif
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
 
<div class="row">
<div class="col-md-12">

<!-- START DATATABLE EXPORT -->
<div class="panel panel-default">
<div class="panel-heading">
<div class="btn-group pull-right">
<a href="{{url('/addcategory')}}">
<button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
</a>
</div>

</div>
<div class="panel-body">
        <div class="table-responsive">
        <table id="customers2" class="table datatable">
        <thead>
        <tr>
        <th>Category Name</th>
        <th>File Name</th>
        <th>Description</th>
        <th>File</th>
        <th>Delete</th>
        <th>Edit</th>

        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <tr>
        <td>{{$row->category}}</td>
        <td>{{$row->title}}</td>
        <td>{{$row->description}}</td>
        <td><a href="{{url('filedownload/'.$row->id)}}" ><i class="fa fa-download" aria-hidden="true"></i></a></td>
         <td><a href="{{url('/deletecategory/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
        <td>
         
        <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a>

        </td>

        <td></td>

        </tr>
        @endforeach
        </tbody>
        </table>
        </div>
  </div>
</div>
 
</div>
</div>

</div>
 </div>
 </div>
 
<div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update</h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 

                <form class="form-horizontal" action="{{url('editcategory/')}}" method="Post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label category">Category</label>
                <div class="col-md-9">
                <select class="form-control term1 custom-select category" name="category" required="">
                           <option value="" selected disabled >Choose Category</option>
                           <option value="specialbuddytokens"><td>Special Buddy Tokens</td></option>
                           <option value="buddybucks"><td>Buddy Bucks</td></option>
                           <option value="coloringsheets"><td></td>Coloring Sheets</option>
                        </select> 
                </div>
                </div>
                
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label st">Title</label>
                <div class="col-md-9">
                <input type="text" class="form-control title" name="title" value="" />
                </div>
                </div>

                <div class="form-group">
                <label class="col-md-3 control-label st">Description</label>
                <div class="col-md-9">
                <input type="text" class="form-control description" name="description" value="" />
                </div>
                </div>


                <div class="form-group">
                <label class="col-md-3 control-label">File</label>
                <div class="col-md-9">
                <input type="file" class="form-control" name="file"/>
                </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
            </div>
        </div>        
        

<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#edit', function(e){
data =$(this).data('id');
// alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'updatecategory/'+data,
 
success:function(data){
     $("#modal_change_password").modal('show');
   
 $('.id').val(data.id);
$('.category').val(data.category);
$('.st').val(data.status);
$('.title').val(data.title);
$('.description').val(data.description);

$('.file').val(data.file);
//console.log(data);
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>

@endsection










