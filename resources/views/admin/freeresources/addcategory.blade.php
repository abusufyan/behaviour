@extends('layouts.adminhead')

@section('content')
            
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="{{url('insertcategory')}}" method="Post" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <input type="hidden" name="id" value="">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add Category</strong></h3>
                                    
                                @if(session('message'))
                                <p class="alert alert-warning">
                                {{session('message')}}</p>
                                @endif
                                </div>
                                
                <div class="panel-body">

                <div class="form-group" >

                    <label class="col-md-3 col-xs-12 control-label">Category</label>
                    <div class="col-md-6 col-xs-12">  

                    <div class="input-group">
                        <span class="input-group-addon"><span class="fa fa-list-ul"></span></span>
                        <select class="form-control term1 custom-select" name="category" required="">
                           <option value="" selected disabled >Choose Category</option>
                           <option value="specialbuddytokens"><td>Special Buddy Tokens</td></option>
                           <option value="buddybucks"><td>Buddy Bucks</td></option>
                           <option value="coloringsheets"><td></td>Coloring Sheets</option>
                        </select> 
                            <i class="mdi mdi-account-convert icon1"></i>
                    </div>                                            
                            <span class="help-block">Category</span>
                    </div>
                </div> 

                 <div class="form-group" >                                        
                    <label class="col-md-3 col-xs-12 control-label">Title</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="text" name="title" class="form-control"  required="" />
                        </div>            
                        <span class="help-block">File</span>
                    </div>
                </div>

                 <div class="form-group" >                                        
                    <label class="col-md-3 col-xs-12 control-label">Description</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="text" name="description" class="form-control"   />
                        </div>            
                        <span class="help-block">Description</span>
                    </div>
                </div>                                                                       
                        
                                    

                                    
                <div class="form-group" >                                        
                    <label class="col-md-3 col-xs-12 control-label">File</label>
                    <div class="col-md-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="file" name="file" class="form-control"  required="" />
                        </div>            
                        <span class="help-block">File</span>
                    </div>
                </div>




    

                                </div>
                                <div class="panel-footer">
                                     <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                                                           
            </div>            
         </div>
 
 

@endsection





