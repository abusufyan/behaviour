@extends('layouts.adminhead')

@section('content')
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Character's Lists</h2>
                </div>
                <!-- END PAGE TITLE -->                
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <div class="btn-group pull-right">
                                        <a href="{{url('/addcharacter')}}">
                                        <button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
                                    </a> 
                                    </div>                                    
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                     <th>Gender</th>
                                                     <th>Date</th>
                                                    <th>Delete</th>
                                                    <th>Edit</th>
           
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @foreach($character as $row)
                                                <tr>
                                                    <td>{{$row->characterName}}</td>
                                                    <td><img src="{{url('public/character/'.$row->characterImage)}}" style="height:70px;"></td>
                                                     <td>{{$row->characterGender}}</td>

                                                    <td>{{$row->created_at}}</td>
                                                    <td><a href="{{url('/deletecharacter/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                    <td>

                                                    <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a>

                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
         
<div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update</h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 

                <form class="form-horizontal" action="{{url('updatecharacter/')}}" method="Post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label avatarName">Character Name</label>
                <div class="col-md-9">
                <input type="text" class="form-control characterName" name="characterName" value="" />
                </div>
                </div>
                

                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label avatarName">Character Gender</label>
                <div class="col-md-9">
                <select class="form-control select characterGender" name="characterGender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>

                </select>   
                 </div>
                </div>

                
                <div class="form-group">
                <label class="col-md-3 control-label">New Avatar Image</label>
                <div class="col-md-9">
                <input type="file" class="form-control" name="image"/>
                </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
            </div>
        </div>        
        

<script type="text/javascript">

$(document).ready(function(){

$(document).on('click', '#edit', function(e){
data =$(this).data('id');
// alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'editcharacter/'+data,
 
success:function(data){
     $("#modal_change_password").modal('show');
   
 $('.id').val(data.id);
$('.characterName').val(data.characterName);
$('.characterImage').val(data.characterImage);
$('.characterGender').val(data.characterGender);

 console.log(data);
 
},
error: function()
{
toastr.error('Error!', 'Something Went Wrong. Please try again later. If the issue persists contact support.' ,{"positionClass": "toast-bottom-right"});

}
});

});
});


</script>   
 


@endsection


 








