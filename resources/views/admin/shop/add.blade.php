@extends('layouts.adminhead')

@section('content')
             
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12"> 
                            
                            <form class="form-horizontal" action="{{url('insertitem')}}" method="Post" enctype="multipart/form-data">
                      {{ csrf_field() }}
                           <input type="hidden" name="categorey" value="nav">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add Shop Item</strong></h3>
                                    
                                @if(session('message'))
                                <p class="alert alert-warning">
                                {{session('message')}}</p>
                                @endif
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="name" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Name of Item</span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Price</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="price" class="form-control"/>
                                            </div>                                            
                                            <span class="help-block">Number of Coins</span>
                                        </div>
                                    </div>
                                     <div class="form-group"  >
                                        <label class="col-md-3 col-xs-12 control-label">Select Type</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" id="test" name="type">
                                                <option value="Buddy">Buddy</option>
                                                <option value="BackgroundImage">BackgroundImage</option>
                                                 
                                            </select>
                                            <span class="help-block">Select Item Type</span>
                                        </div>
                                    </div>
                                    

                                    <div class="form-group" >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Image</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                <input type="file" name="image" class="form-control" required>
                                            </div>            
                                            <span class="help-block">Image of Item</span>
                                        </div>
                                    </div>



                                     

                                </div>
                                <div class="panel-footer">
                                     <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                                                           
            </div>            
         </div>
 
 

@endsection





