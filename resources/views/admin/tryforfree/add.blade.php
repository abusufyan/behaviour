@extends('layouts.adminhead')

@section('content')
             <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
             
            
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                            <form class="form-horizontal" action="{{url('insertfreetry')}}" method="Post" enctype="multipart/form-data">
                      {{ csrf_field() }}
                           <input type="hidden" name="categorey" value="nav">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Add New</strong></h3>
                                    
                                @if(session('message'))
                                <p class="alert alert-warning">
                                {{session('message')}}</p>
                                @endif
                                </div>
                                
                                <div class="panel-body">                                                                        
                                    
                                    <div class="form-group" >
                                        <label class="col-md-3 col-xs-12 control-label">Title</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" name="title" class="form-control"/>
                                            </div>                                            
                                         </div>
                                    </div>
                                    
                                    <div class="form-group" >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Background Image</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                <input type="file" name="backgroundimage" class="form-control" required>
                                            </div>            
                                            <span class="help-block">Image of Background</span>
                                        </div>
                                    </div>

                                     <div class="form-group" >                                        
                                        <label class="col-md-3 col-xs-12 control-label">Buddy Image</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                                <input type="file" name="buddyimage" class="form-control"/>
                                            </div>            
                                            <span class="help-block">Image of Buddy</span>
                                        </div>
                                    </div>
                                     <div class="form-group"  >
                                        <label class="col-md-3 col-xs-12 control-label">Select Steps</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="steps">
                                             <option value="6">6</option>
                                            <option value="8">8</option>
                                            <option value="11">11</option>

                                           </select>
                                        </div>
                                    </div>

                                     

                                </div>
                                <div class="panel-footer">
                                     <button class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            </form>
                            
                        </div>
                    </div>                    
                    
                </div>
                                                           
            </div>            
         </div>
 
 

@endsection





