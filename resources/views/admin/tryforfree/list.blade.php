@extends('layouts.adminhead')
@section('content')
 
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Try For Free</h2>
                </div>
                <!-- END PAGE TITLE -->                
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                               <!--  <div class="panel-heading">
                                     <div class="btn-group pull-right">
                                        <a href="{{url('/addtryforfree')}}">
                                        <button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
                                    </a> 
                                    </div>                                    
                                    
                                </div> -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Background Image</th>
                                                    <th>Buddy Image</th>
                                                    <th>No of Steps</th>
                                                     <th>Date</th>
                                                    <th>Delete</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @foreach($tryforfree as $row)
                                                <tr>
                                                    <td>{{$row->title}}</td>
                                                    <td><img src="{{url('public/tryforfree/'.$row->backgroundimage)}}" style="height:70px;"></td>
                                                     <td><img src="{{url('public/tryforfree/'.$row->buddyimage)}}" style="height:70px;"></td>
                                                     <td>{{$row->steps}}</td>
 
                                                    <td>{{$row->created_at}}</td>
                                                    <td><a href="{{url('/deletetryforfree/'.$row->id)}}"><i class="fa fa-trash" onclick="return confirm('Are you sure you want to delete this ?')"></i></a></td>
                                                    <td>   <a href="javascript:void(0);" class="btn btn-dark btn-sm update_product" id="edit" data-id="{{$row->id}}" style="margin-left: 5px;"><i class="fa fa-edit"></i></a></td>
</td>
                                                     
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>

                </div>         
             </div>            
         </div>
       

       <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="smallModalHead">Update</h4>
                    </div>
                    
                    <div class="modal-body form-horizontal form-group-separated"> 

                <form class="form-horizontal" action="{{url('updateTry/')}}" method="Post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                <input type="hidden" name="id" class="id" value="">

                <label class="col-md-3 control-label avatarName">Title</label>
                <div class="col-md-9">
                <input type="text" class="form-control title" name="title" value="" />
                </div>
                </div>
               
               
                <div class="form-group">   
                <label class="col-md-3 control-label avatarName">Steps</label>

                <div class="col-md-9">
                <select class="form-control select steps" id="test" name="steps">
                <option value="6">6</option>
                <option value="8">8</option>
                <option value="11">11</option>

                </select>
                 </div>
                </div>
                
                <div class="form-group" id="email" style="display: none;" >
                <label class="col-md-3 control-label avatarName">Email</label>
                <div class="col-md-9">                                               
                <input type="email" name="email" class="form-control email">

                <span class="help-block">Please enter email of user who can avail this coupon</span>
                </div>
                </div>
                <div class="form-group" id="percentage"  >                
                <label class="col-md-3 control-label avatarName">Buddyimage</label>
                <div class="col-md-9">
                <input type="file" class="form-control  " name="buddyimage" value="" />
                </div>
                </div>

                <div class="form-group" id="percentage"  >                
                <label class="col-md-3 control-label avatarName">Background Image</label>
                <div class="col-md-9">
                <input type="file" class="form-control  " name="backgroundimage" value="" />
                </div>
                </div>
               
                 
                </div>
                <div class="modal-footer">
                <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
            </div>
        </div>        
               
  

<script type="text/javascript">

$(document).ready(function(){
 
$(document).on('click', '#edit', function(e){
data =$(this).data('id');
 //alert($(this).data('id'));
$.ajax({
method:"GET",
data: 'id='+ data,
contentType: "application/json; charset=utf-8",
dataType: "json",
modal: true,

url:'edittryforfree/'+data,
 
success:function(data){
$("#modal_change_password").modal('show');
$('.id').val(data.id);
$('.title').val(data.title);
$('.steps').val(data.steps);
$('.buddyimage').val(data.buddyimage);
$('.backgroundimage').val(data.backgroundimage);
 

// console.log(data);
 
},
 
});

});
});


</script>
@endsection


 








