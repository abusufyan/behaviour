@extends('layouts.adminhead')

@section('content')
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Payment History</h2>
                </div>
                <!-- END PAGE TITLE -->                
                @if(session('message'))
                <p class="alert alert-warning">
                {{session('message')}}</p>
                @endif
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- START DATATABLE EXPORT -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <div class="btn-group pull-right">
                                        <a href="{{url('/addbackground')}}">
                                        <button class="btn btn-primary" ><i class="fa fa-bars"></i>Add new </button>
                                    </a> 
                                    </div>                                    
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="customers2" class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Email </th>
                                                    <th>Amount</th>
                                                    <th>Account Satus</th>
                                                    <th>Payment Date</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                               @foreach($payment as $row)
                                                <tr>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{$row->amount}} $</td>
                                                    <td><?php if($row->active == 0){
                                                    echo'Inactive';}
                                                    else{
                                                    echo 'Active'; 
                                                     } ?></td>
                                                    <td>{{$row->created_at}}</td>
                                                     
                                                     
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                    
                                    </div>
                                </div>
                            </div>
                            <!-- END DATATABLE EXPORT -->                            
                            
                            <!-- START DEFAULT TABLE EXPORT -->
                            
                            <!-- END DEFAULT TABLE EXPORT -->

                        </div>
                    </div>

                </div>         
                <!-- END PAGE CONTENT WRAPPER -->
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->    
 


@endsection


 








