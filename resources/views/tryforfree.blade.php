@include('layouts.header')
 
<div class="container-fluid mt-5">
    <div class="row mt-5 m-auto">
        <div class="col-lg-6 col-md-12 col-sm-12 ">
            <div class="col-lg-12 col-md-12 col-sm-12 ">
                <h2 class="font2 text-center mt-4">With over 40 buddies and <br> 10 backgrounds, your <br> options are endless. <br> Here are 3 sample charts  <br> to try for free!</h2>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 ">
                <img src="public/images/buddy logo holding box of stuff.png" alt="" class="m-auto video1">
            </div>
        </div>
        <div  class="col-lg-6 col-md-12 col-sm-12 ">

            <div class="col-lg-12 col-md-12 col-sm-12 ">
<a href="{{url('/freedemo')}}" title="">
                <img src="public/images/forest chart.png" alt="" class="img-fluid"></a>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 ">
<a href="{{url('freedemo2/')}}" title="">
                <img src="public/images/space chart.png" alt="" class="img-fluid"></a>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 ">
<a href="{{url('freedemo3/')}}" title="">
                <img src="public/images/hills chart.png" alt="" class="img-fluid"></a>
            </div>
        </div>
    </div>
</div>