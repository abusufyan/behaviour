@include('layouts.header')
@section('content')

@endsection

<style type="text/css">
.login-container{
    margin-top: 5%;
    margin-bottom: 5%;
}
.login-form-1{
    padding: 5%;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
}
.login-form-1 h3{
    text-align: center;
    color: #333;
}
.login-form-2{
    padding: 5%;
    background: #0062cc;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
}
.login-form-2 h3{
    text-align: center;
    color: #fff;
}
.login-container form{
     
    padding: 10%;
}
.btnSubmit
{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    border: none;
    cursor: pointer;
}
.login-form-1 .btnSubmit{
    font-weight: 600;
    color: #fff;
    background-color: #0062cc;
}
.login-form-2 .btnSubmit{
    font-weight: 600;
    color: #0062cc;
    background-color: #fff;
}
.login-form-2 .ForgetPwd{
    color: #fff;
    font-weight: 600;
    text-decoration: none;
}
.login-form-1 .ForgetPwd{
    color: #0062cc;
    font-weight: 600;
    text-decoration: none;
}
    
</style>

<div class="container d-flex" id="learn" >

    <div class=" login-container w-100"  >
                <div class="row justify-content-center">

                    <div class="col-md-6 login-form-1" style="background-color: #edffd78f;">
                        
                        <h3>Reset Password</h3>
                        @if(session('message'))
                        <p class="alert alert-white text-danger font text-center mt-2 " style="height: 30px; line-height: 0px; font-size: 16px;">
                        {{session('message')}}</p>
                        @endif
                        <form action="{{url('/updatepassword')}}" method="post">
                                                  {{ csrf_field() }}
                             <input type="hidden" name="user_id" value="{{$user_id}}"> 
                            <div class="form-group">
                                <input type="Password" class="form-control font"  name="password" placeholder="New Password" value="" />
                            </div>

                            <div class="form-group">
                                <input type="Password" class="form-control font"  name="confirmpassword" placeholder="Confirm Password" value="" />
                            </div>

                             
                            <div class="form-group d-flex justify-content-center mt-5">
                                <input type="submit" class="btnSubmit" value="Reset" />
                            </div>
                             
                        </form>
                    </div>
                    
               </div>
          </div>
</div>

@include('layouts.footer')
@section('content')
@endsection