   <!DOCTYPE html>
<html>
  <head>
    <title>Behavior</title>

    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
      crossorigin="anonymous"
    ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <style>
      .G1 { 
            transform: translate(5px,10px) rotate(-5deg); 
        } 
  
        .e1 { 
            transform: translate(2px,-2px) rotate(0deg); 
        } 
  
        .e2 { 
            transform: translate(5px, 10px) rotate(4deg); 
        } 
  
        .k1 { 
            transform: translate(3px, 15px) rotate(-15deg); 
        } 
  
        .s1 { 
            transform: translate(2px, 4px) rotate(-10deg); 
        } 
  
        .f { 
            transform: translate(0px, 0px) ; 
        } 
  
        .o { 
            transform: translate(0px, 3px) rotate(5deg); 
        } 
  
        .r { 
            transform: translate(-1px, 8px) rotate(5deg); 
        } 
  
        .G2 { 
            transform: translate(-2px, 14px) rotate(10deg); 
        } 
  
        .e3 { 
            transform: translate(-3px, 25px) rotate(15deg); 
        } 
  
        .e4 { 
            transform: translate(-6px, 40px) rotate(20deg); 
        } 
  
        .k2 { 
            transform: translate(-8px, 55px) rotate(25deg); 
        } 
  
        .s2 { 
            transform: translate(-20px, 80px) rotate(30deg); 
        } 
  
        /* An inline-block element is placed as an inline 
         element (on the same line as adjacent content),  
         but it behaves like a block element  */ 
  
        span { 
            display: inline-block; 
        } 

        .fontColor{
            color: #1d3d7a;
        }
        .main {
            background-image: url("images/bg2.jpg");
            height: 655px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
            overflow: hidden;
        }
        .line{
            background-color: #1d3d7a;
            min-height: 6px;
            border-radius: 20px;
        }
        .child3{
            transform: translate(-6px, 40px) rotate(20deg);
        }

        /* Media Quries */
        @media only screen and (max-width: 1200px) {
            .main {
                height: 700px;
            }
        }


        @media only screen and (max-width: 991px) {
            .main {
                height: 2100px;
            }
        }

.circle-left, .circle-right {
position: absolute;
}

.circle-right {
z-index:1;
margin: 13% 4% 4% 6%;
padding: 1%;
text-align: center;
width: 77%;
object-fit: cover;
}
   
        .imgClass{
            /* max-width: 200px;
            min-width: 50px; */
            width: 85px;
            height: 100px;
            /* max-height: 120px;
            min-height: 50px; */
            margin-left: 40%;
        }
      .object {
        animation: MoveUpDown 2s linear infinite;
        position: absolute;
        left: 0;
        bottom: 0;
        margin-bottom: 100px;
      }

      .object2 {
        animation: MoveUpDown 2s linear infinite;
        position: absolute;
        left: 0;
        bottom: 0;
        top: 100;
      }
      
      .nextbackRow{
          margin-top: 10%;
      }
      .pod{
          margin-top: 100px;
          width: "16%";
      }
      .pod2{
          width: "16%";
      }
      @keyframes MoveUpDown {
        0%,
        100% {
          bottom: 0;
        }
        50% {
          bottom: 15px;
        }
      }

      #tokenRotate1,#tokenRotate2,#tokenRotate3,#tokenRotate4,#tokenRotate5  {
          padding-bottom: 15%;
          margin-left: 30%;
          /* margin: 0 auto; */
        -webkit-animation: rotation 2s infinite linear;
      }

      @-webkit-keyframes rotation {
        from {
          -webkit-transform: rotate(0deg);
        }
        to {
          -webkit-transform: rotateY(359deg);
        }
      }
    </style>

    <script>

      

      count = 0;
   function onJump() {
    var a = count += 1;
    // alert(a);
    console.log(a)
        var elem = document.getElementById("foxID");
        var elem3 = document.getElementById("next");
        var audio = document.getElementById("myAudio");

        elem3.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }],
          {
            duration: 100,
          }
        );

        // var elem4 = document.getElementById("back");

        // elem4.animate(
        //   [{ transform: "scale(1)" }, { transform: "scale(0.66)" }],
        //   {
        //     duration: 100,
        //   }
        // );
        if (a===1) {
          audio.play()

          elem.animate(
          [
            // keyframes

            { top: "-5%" },
  { top: "-50%" },
  { left: "100%" },
  { bottom: "5%" },
  { bottom: "2%" },
  // { top: "0px" },
  // { top: "15px" },
  //           { top: "10px" },
            // { top: "20px" },

            // { bottom: "0px" },
            // { top: "0px" },
            // { top: "50px" },
            // { top: "0px" },
            // 100 % { top: "100px" },
          ],
          {
            // timing options
            duration: 2000,
            //   iterations: 2,
          }
        );
          setTimeout(function() {
          //   console.log("elem.style.left <= ");
          //   setInterval(function () {
            var elem2 = document.getElementById("tokenRotate"+a);

              elem.style.left = "100%";
              elem.style.paddingBottom = "10%";
              elem2.style.visibility = "hidden";
          //   }, 1000);
          }, 1000);
        } else if (a===2) {
          audio.play()

elem.animate(
[
  // keyframes
  // { top: "0px" },
  // { top: "-80px" },
  // { left: "440px" },
  // { bottom: "10px" },
  // { top: "-6px" },
  // { top: "0px" },
  // { top: "-80px" },
  // { left: "440px" },
  // { bottom: "10px" },
  // { top: "-6px" },
  // { top: "15px" },
  { top: "-5%" },
  { top: "-50%" },
  { left: a*100+"%" },
  { bottom: "5%" },
  { bottom: "2%" },

            // { top: "10px" },
],
{
  // timing options
  duration: 2000,
  //   iterations: 2,
}
);
setTimeout(function() {
    elem.style.left =  a*100+"%";
    var elem2 = document.getElementById("tokenRotate"+a);

    elem.style.paddingBottom = "20%";
    elem2.style.visibility = "hidden";
}, 1000);
} else if (a===3) {
  audio.play()

elem.animate(
[
  // keyframes
  // { top: "0px" },
  // { top: "-80px" },
  // { left: "670px" },
  // { top: "5px" },
  // {bottom:"5px"}
  // { top: "0px" },
  // { top: "-80px" },
  // { left: "670px" },
  // { bottom: "10px" },
  // { top: "-6px" },
  // { top: "15px" },
  { top: "-5%" },
  { top: "-50%" },
  { left: a*100+"%" },
  { bottom: "5%" },
  { bottom: "2%" },

],
{
  // timing options
  duration: 2000,
  //   iterations: 2,
}
);
setTimeout(function() {
  var elem2 = document.getElementById("tokenRotate"+a);
    elem.style.left =  a*100+"%";
    elem.style.paddingBottom = "20%";
    elem2.style.visibility = "hidden";
//   }, 1000);
}, 1000);
}else if (a===4) {
  audio.play()

elem.animate(
[
// keyframes
// { top: "0px" },
// { top: "-50px" },
// { left: "900px" },
// { top: "5px" },
// { bottom: "5px" },
// { top: "0px" },
//   { top: "-80px" },
//   { left: "900px" },
//   { bottom: "10px" },
//   { top: "-6px" },
//   { top: "15px" },
{ top: "-5%" },
  { top: "-50%" },
  { left: a*100+"%" },
  { bottom: "5%" },
  { bottom: "2%" },

],
{
// timing options
duration: 2000,
//   iterations: 2,
}
);
setTimeout(function() {
  var elem2 = document.getElementById("tokenRotate"+a);

elem.style.left =  a*100+"%";
elem.style.paddingBottom = "20%";
elem2.style.visibility = "hidden";
//   }, 1000);
}, 1000);
} else if (a===5) {
  audio.play()

elem.animate(
[
// keyframes
// { top: "0px" },
// { top: "-70px" },
// { left: "1120px" },
// // { top: "2px" },
// { bottom: "5px" },

// { top: "-5px" },
// { top: "0px" },
//   { top: "-80px" },
//   { left: "1120px" },
//   { bottom: "10px" },
//   { top: "-6px" },
//   { top: "15px" },
{ top: "-5%" },
  { top: "-50%" },
  { left: a*100+"%" },
  { bottom: "5%" },
  { bottom: "2%" },

],
{
// timing options
duration: 2000,
//   iterations: 2,
}
);
setTimeout(function() {
  var elem2 = document.getElementById("tokenRotate"+a);

elem.style.left =  a*100+"%";
elem.style.paddingBottom = "20%";
elem2.style.visibility = "hidden";
//   }, 1000);
}, 1000);
} else{
  var audio2 = document.getElementById("myAudio2");
  audio2.play()
 var SCREEN_WIDTH = window.innerWidth,
    SCREEN_HEIGHT = window.innerHeight,
    mousePos = {
        x: 400,
        y: 300
    },

    // create canvas
    canvas = document.createElement('canvas'),
    context = canvas.getContext('2d'),
    particles = [],
    rockets = [],
    MAX_PARTICLES = 400,
    colorCode = 0;

// init
$(document).ready(function() {
    document.body.appendChild(canvas);
    canvas.width = SCREEN_WIDTH;
    canvas.height = SCREEN_HEIGHT;
    canvas.style.marginTop="-"+SCREEN_HEIGHT+"px"

    setInterval(launch, 800);
    setInterval(loop, 1000 / 50);
});

// update mouse position
$(document).mousemove(function(e) {
    e.preventDefault();
    mousePos = {
        x: e.clientX,
        y: e.clientY
    };
});

// launch more rockets!!!
$(document).mousedown(function(e) {
    for (var i = 0; i < 5; i++) {
        launchFrom(Math.random() * SCREEN_WIDTH * 2 / 3 + SCREEN_WIDTH / 6);
    }
});

function launch() {
    launchFrom(mousePos.x);
}

function launchFrom(x) {
    if (rockets.length < 10) {
        var rocket = new Rocket(x);
        rocket.explosionColor = Math.floor(Math.random() * 360 / 10) * 10;
        rocket.vel.y = Math.random() * -3 - 4;
        rocket.vel.x = Math.random() * 6 - 3;
        rocket.size = 8;
        rocket.shrink = 0.999;
        rocket.gravity = 0.01;
        rockets.push(rocket);
    }
}

function loop() {
    // update screen size
    if (SCREEN_WIDTH != window.innerWidth) {
        canvas.width = SCREEN_WIDTH = window.innerWidth;
    }
    if (SCREEN_HEIGHT != window.innerHeight) {
        canvas.height = SCREEN_HEIGHT = window.innerHeight;
    }

    // clear canvas
    context.fillStyle = "rgba(0, 0, 0, 0.05)";
    context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    var existingRockets = [];

    for (var i = 0; i < rockets.length; i++) {
        // update and render
        rockets[i].update();
        rockets[i].render(context);

        // calculate distance with Pythagoras
        var distance = Math.sqrt(Math.pow(mousePos.x - rockets[i].pos.x, 2) + Math.pow(mousePos.y - rockets[i].pos.y, 2));

        // random chance of 1% if rockets is above the middle
        var randomChance = rockets[i].pos.y < (SCREEN_HEIGHT * 2 / 3) ? (Math.random() * 100 <= 1) : false;

/* Explosion rules
             - 80% of screen
            - going down
            - close to the mouse
            - 1% chance of random explosion
        */
        if (rockets[i].pos.y < SCREEN_HEIGHT / 5 || rockets[i].vel.y >= 0 || distance < 50 || randomChance) {
            rockets[i].explode();
        } else {
            existingRockets.push(rockets[i]);
        }
    }

    rockets = existingRockets;

    var existingParticles = [];

    for (var i = 0; i < particles.length; i++) {
        particles[i].update();

        // render and save particles that can be rendered
        if (particles[i].exists()) {
            particles[i].render(context);
            existingParticles.push(particles[i]);
        }
    }

    // update array with existing particles - old particles should be garbage collected
    particles = existingParticles;

    while (particles.length > MAX_PARTICLES) {
        particles.shift();
    }
}

function Particle(pos) {
    this.pos = {
        x: pos ? pos.x : 0,
        y: pos ? pos.y : 0
    };
    this.vel = {
        x: 0,
        y: 0
    };
    this.shrink = .97;
    this.size = 2;

    this.resistance = 1;
    this.gravity = 0;

    this.flick = false;

    this.alpha = 1;
    this.fade = 0;
    this.color = 0;
}

Particle.prototype.update = function() {
    // apply resistance
    this.vel.x *= this.resistance;
    this.vel.y *= this.resistance;

    // gravity down
    this.vel.y += this.gravity;

    // update position based on speed
    this.pos.x += this.vel.x;
    this.pos.y += this.vel.y;

    // shrink
    this.size *= this.shrink;

    // fade out
    this.alpha -= this.fade;
};

Particle.prototype.render = function(c) {
    if (!this.exists()) {
        return;
    }

    c.save();

    c.globalCompositeOperation = 'lighter';

    var x = this.pos.x,
        y = this.pos.y,
        r = this.size / 2;

    var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
    gradient.addColorStop(0.1, "rgba(255,255,255," + this.alpha + ")");
    gradient.addColorStop(0.8, "hsla(" + this.color + ", 100%, 50%, " + this.alpha + ")");
    gradient.addColorStop(1, "hsla(" + this.color + ", 100%, 50%, 0.1)");

    c.fillStyle = gradient;

    c.beginPath();
    c.arc(this.pos.x, this.pos.y, this.flick ? Math.random() * this.size : this.size, 0, Math.PI * 2, true);
    c.closePath();
    c.fill();

    c.restore();
};

Particle.prototype.exists = function() {
    return this.alpha >= 0.1 && this.size >= 1;
};

function Rocket(x) {
    Particle.apply(this, [{
        x: x,
        y: SCREEN_HEIGHT}]);

    this.explosionColor = 0;
}

Rocket.prototype = new Particle();
Rocket.prototype.constructor = Rocket;

Rocket.prototype.explode = function() {
    var count = Math.random() * 10 + 80;

    for (var i = 0; i < count; i++) {
        var particle = new Particle(this.pos);
        var angle = Math.random() * Math.PI * 2;

        // emulate 3D effect by using cosine and put more particles in the middle
        var speed = Math.cos(Math.random() * Math.PI / 2) * 15;

        particle.vel.x = Math.cos(angle) * speed;
        particle.vel.y = Math.sin(angle) * speed;

        particle.size = 10;

        particle.gravity = 0.2;
        particle.resistance = 0.92;
        particle.shrink = Math.random() * 0.05 + 0.93;

        particle.flick = true;
        particle.color = this.explosionColor;

        particles.push(particle);
    }
};

Rocket.prototype.render = function(c) {
    if (!this.exists()) {
        return;
    }

    c.save();

    c.globalCompositeOperation = 'lighter';

    var x = this.pos.x,
        y = this.pos.y,
        r = this.size / 2;

    var gradient = c.createRadialGradient(x, y, 0.1, x, y, r);
    gradient.addColorStop(0.1, "rgba(255, 255, 255 ," + this.alpha + ")");
    gradient.addColorStop(1, "rgba(0, 0, 0, " + this.alpha + ")");

    c.fillStyle = gradient;

    c.beginPath();
    c.arc(this.pos.x, this.pos.y, this.flick ? Math.random() * this.size / 2 + this.size / 2 : this.size, 0, Math.PI * 2, true);
    c.closePath();
    c.fill();

    c.restore();
}; 

window.setTimeout(function() {
  // alert("I m ALERTED after 8 seconds!")
//   document.location.reload(true)
// document.getElementById("modal").style.display="block"
//   data-toggle="modal" data-target="#exampleModal"
$('#modal').show();
console.log("OK")
},6000)
} 



}


function onJumpBack() {
    var a = count -= 1;

    console.log("JUMPBACK",a)
        var elem = document.getElementById("foxID");
//         var elem2 = document.getElementById("tokenRotate");
        var elem3 = document.getElementById("back");

        elem3.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }],
          {
            duration: 100,
          }
        );
var token=a+1
        if (a===0) {
          elem.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }, { transform: "scale(0.33)" }],
          {
            duration: 800,
          }
        );
          setTimeout(function() {

            var elem2 = document.getElementById("tokenRotate"+token);
              elem.style.left =  a*100+"%";
              elem.style.paddingBottom = "0px";
              elem2.style.visibility = "visible";
          //   }, 1000);
          }, 800);
        } else if (a===1) {
          elem.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }, { transform: "scale(0.33)" }],
          {
            duration: 800,
          }
        );

setTimeout(function() {

  var elem2 = document.getElementById("tokenRotate"+token);
  elem.style.left =  a*100+"%";
              elem.style.paddingBottom = "30px";

    elem2.style.visibility = "visible";
//   }, 1000);
}, 800);
} else if (a===2) {
          elem.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }, { transform: "scale(0.33)" }],
          {
            duration: 800,
          }
        );

setTimeout(function() {

  var elem2 = document.getElementById("tokenRotate"+token);
  elem.style.left =  a*100+"%";
              elem.style.paddingBottom = "70px";

    elem2.style.visibility = "visible";
//   }, 1000);
}, 800);
} else if (a===3) {
          elem.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }, { transform: "scale(0.33)" }],
          {
            duration: 800,
          }
        );

setTimeout(function() {

  var elem2 = document.getElementById("tokenRotate"+token);
  elem.style.left =  a*100+"%";
              elem.style.paddingBottom = "30px";

    elem2.style.visibility = "visible";
//   }, 1000);
}, 800);
} else if (a===4) {
          elem.animate(
          [{ transform: "scale(1)" }, { transform: "scale(0.66)" }, { transform: "scale(0.33)" }],
          {
            duration: 800,
          }
        );

setTimeout(function() {

  var elem2 = document.getElementById("tokenRotate"+token);
  elem.style.left =  a*100+"%";
              elem.style.paddingBottom = "70px";

    elem2.style.visibility = "visible";
//   }, 1000);
}, 800);
} else{
  alert("UNDEFINED ERROR")
} 
}

      
    </script>
  </head>

  <body style="overflow: hidden;">
    <?php $bgimg = "public/background/$profile->backgroundImage" ?>

     <div
      style="
        background-image: url('<?php echo url($bgimg);?>');
        background-repeat: no-repeat;
        width: 100%;
        height: 655px;
        overflow: hidden;
      "
      id="BG"
    >
      <div class="container-fluid">
         <div class="row mt-2">
          <div class="col-lg-3 col-md-3 col-sm-3 ml-auto d-block">
          <div class="containerab">
          <img
          src="{{url('public/images/name in upper right corner of chart-design.png')}}"
          class="img-fluid circle-left"/>
          <div class="circle-right row">
          <img src="{{url('public/images/Boy5.png')}}" class="img-fluid w-25 h-25"/>
          <div class="text-white">
          <h4 class="font ml-5 mt-3"><b>{{$profile->childName}}</b></h4>
          </div> 
          </div>
          </div>

          </div>
        </div>
        <div class="row mt-5">
        &nbsp;
        </div>           <div class="row mt-5">
        &nbsp;
        </div>           <div class="row mt-5">
        &nbsp;
        </div>          

        <audio src="{{url('public/audio/buddy jumping to pod.mp3')}}" id="myAudio"
        style="visibility: hidden">
          <!-- <source
            src="images/audio/jump.mp3"
            id="myAudio"
            type="audio/ogg"
          /> -->
        </audio>

        <audio src="{{url('public/images/audio/celebrate.mp3')}}" id="myAudio2"
        style="visibility: hidden">
          <!-- <source
            src="images/audio/jump.mp3"
            id="myAudio"
            type="audio/ogg"
          /> -->
        </audio>


        <div class="row mt-5">
          <div class="col-lg-2 col-md-2 col-sm-2 mt-5">
            <div class="col-lg-8 col-md-8 col-sm-8 object" id="foxID">
              <img
                src="{{url('public/avatar/'.$profile->avatarImage)}}"
                alt=""
                class="img-fluid imgClass"
              />
            </div>
            <div class="pod">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 mt-5">
            <div class="col-lg-12 col-md-12 col-sm-12 tokenID">
              <img
                src="{{url('public/images//token.png')}}"
                alt=""
                height="50"
                width="50"
                class="img-fluid"
                id="tokenRotate1"
              />
            </div>
            <div class="pod2">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 mt-3">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <img
                src="{{url('public/images//token.png')}}"
                alt=""
                id="tokenRotate2"
                class="img-fluid"
                height="50"
                width="50"

              />
            </div>
            <div class="pod2">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-2 mt-5">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <img
                src="{{url('public/images//token.png')}}"
                alt=""
                class="img-fluid"
                height="50"
                width="50"
                id="tokenRotate3"
              />
            </div>
            <div class="pod2">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-2 mt-3">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <img
                src="{{url('public/images//token.png')}}"
                alt=""
                class="img-fluid"
                height="50"
                width="50"
                id="tokenRotate4"
              />
            </div>
            <div class="pod2">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <img
                src="{{url('public/images//token.png')}}"
                alt=""
                class="img-fluid"
                height="50"
                width="50"
                id="tokenRotate5"
              />
            </div>
            <div class="pod2">
              <img src="{{url('public/images//pod create a chart tab.png')}}" alt="" class="img-fluid" />
            </div>
          </div>
        </div>
        <div class="row d-flex justify-content-end ">
          <div class="col-1 float-left" onclick="onJumpBack()">
            <img
              src="{{url('public/images/back.png')}}"
              alt=""
              class="img-fluid"
              id="back"
              height="80"
              width="80"
            />
          </div>
          <div class="col-1 float-right" onclick="onJump()">
            <img
              src="{{url('public/images//forward.png')}}"
              alt=""
              class="img-fluid"
              height="75"
              width="75"
              id="next"
            />
          </div>
        </div>
      </div>
    </div>


    <div class="modal slide-up" id="modal" tabindex="-1">
        <div class="modal-dialog" style="    max-width: 100% !important;">
          <div class="modal-content roundModal">
            
            <div class="modal-body">
                <div class="main" style="background-image: url('<?php echo url($bgimg);?>');
">
     <div class="row mt-5">
        <div class="col-lg-2 col-1"></div>
        <div class="col-8 border rounded border-dark pb-5" style="background-color: rgba(255, 255, 255, 0.7);">
            <div class="h1 text-center mt-2 fontColor achievementText ">
                <div class="row justify-content-center" style="font-family: 'Luckiest Guy';">
                    <span class="G1 h1">CERTIFICATE</span> 
                    <span class="e1 h1">&nbsp;&nbsp;&nbsp;OF&nbsp;&nbsp;</span> 
                    <span class="e2 h1">ACHIEVEMENT</span> 
                     
                    <svg viewBox="0 0 500 500">
                        <path id="curve" d="M73.2,148.6c4-6.1,65.5-96.8,178.6-95.6c111.3,1.2,170.8,90.3,175.1,97" />
                        <text width="500">
                          <textPath xlink:href="#curve">
                            Dangerous Curves Ahead
                          </textPath>
                        </text>
                      </svg>
                    
            
                </div>

            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 mt-auto ">
                    <img src="{{url('public/avatar/'.$profile->avatarImage)}}" height="100" width="100" class="img-fluid ml-5" alt="">
                    <img src="{{url('public/images/pod create a chart tab.png')}}" class="img-fluid" alt="">
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="row justify-content-center">
                        <img src="{{url('public/images/reward.png')}}" class="img-fluid jus" alt="reward-icon" height="120" width="120">
                    </div>
                    <div class="row justify-content-center fontColor">
                        <h5>is presented to</h5>
                    </div>
                    <div class="row justify-content-center mt-3 fontColor">
                        <h2>{{$profile->childName}}</h2>
                    </div>
                    <div class="row">
                        <!-- <h5>is presented to</h5> -->
                        <div class="col-2"></div>
                        <div class="col-8 line"></div>
                        <div class="col-2"></div>
                    </div>
                    <div class="row justify-content-center my-4 fontColor font-weight-bold">
                        <h2>for</h2>
                    </div>
                    <div class="row justify-content-center mt-3 fontColor">
                        <h3>{{$profile->behaviour}}</h3>
                    </div>
                    <div class="row">
                        <!-- <h5>is presented to</h5> -->
                        <div class="col-2"></div>
                        <div class="col-8 line"></div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-1"></div>
     </div>
    </div>
            </div>
          </div>
        </div>
      </div>


  </body>


</html>
