<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');
    return "Application cache cleared!";
});

// Configuration cached.
Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return "Configuration cache cleared!<br>Configuration cached successfully!";
});


Route::get('/','IndexController@index')->name('index');
Route::get('/getfullacesss','IndexController@getfullacesss')->name('getfullacesss');
Route::get('/pricing','CouponController@pricing');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*Admin Controller*/


Route::get('/adminlogin', function () {
    return view('admin.adminlogin');
});

 Route::post('/adminsignin', 'AdminController@signin');

/*Admin Side Work*/

 Route::group(['middleware' => 'admin'], function () {
	//admin
	Route::get('/dashboard', 'AdminController@index');
	Route::get('/adminprofile', 'AdminController@adminprofile');
	Route::post('/adminupdateprofile', 'AdminController@adminupdateprofile');
	Route::post('/changepassword', 'AdminController@changepassword');
	Route::get('/manageusers', 'AdminController@manageusers');
	Route::get('/deleteuser/{id}/', 'AdminController@deleteuser');
	Route::post('updateuser/', 'AdminController@updateuser');

	Route::get('edituser/{id}', 'AdminController@edituser');

	Route::get('/logout', 'AdminController@logout');
	//buddies
	Route::get('/addavatar', function () {
	return view('admin.avatar.addavatar');
	});
	Route::post('/insertavatar', 'AvatarController@insertavatar');
	Route::get('/avatarlist', 'AvatarController@avatarlist');
	Route::get('/delete/{id}/', 'AvatarController@deleteavatar');
	Route::post('/editavatar/', 'AvatarController@editavatar')->name('editavatar');
	Route::get('/updateavatar/{id}', 'AvatarController@updateavatar');
	//backgroundImages
	Route::get('/addbackground', function () {
		return view('admin.backgroundImages.addbackground');
		});
	Route::post('/insertbackground', 'BackgroundImagesController@insertbackground');
	Route::get('/backgroundlist', 'BackgroundImagesController@backgroundlist');
	Route::get('/deletebackground/{id}/', 'BackgroundImagesController@deletebackground');
	Route::post('/updatebackground/', 'BackgroundImagesController@updatebackground');
	Route::get('/editbackground/{id}', 'BackgroundImagesController@editbackground')->name('editbackground');;
  
    //characters
	Route::get('/addcharacter', function () {
		return view('admin.character.addcharacter');
		});
	Route::post('/insertcharacter', 'CharacterController@insertCharacter');
	Route::get('/characterlist', 'CharacterController@characterlist');
	Route::get('/deletecharacter/{id}/', 'CharacterController@deletecharacter');
	Route::post('/updatecharacter/', 'CharacterController@updatecharacter');
	Route::get('/editcharacter/{id}', 'CharacterController@editcharacter')->name('editcharacter');;
   //reward icons    
    Route::get('/addreward', function () {
		return view('admin.rewards.addreward');
		});
    Route::post('/insertreward', 'RewardsController@insertreward');
	Route::get('/rewardlist', 'RewardsController@rewardlist');
	Route::get('/deleterewards/{id}/', 'RewardsController@deletereward');
	Route::post('/updatereward/', 'RewardsController@updatereward');
	Route::get('/editreward/{id}', 'RewardsController@editreward')->name('editreward');
	 Route::get('/addtryforfree', function () {
		return view('admin.tryforfree.add');
		});
    Route::post('/insertfreetry', 'TryForFreeController@insertfreetry');
	Route::get('/tryforfreelist', 'TryForFreeController@list');
	Route::get('/deletetryforfree/{id}/', 'TryForFreeController@deletetryforfree');
	Route::get('/edittryforfree/{id}', 'TryForFreeController@edittryforfree');
    Route::post('/updateTry', 'TryForFreeController@updateTry');

	//privacypolicy  
	Route::get('/editprivacypolicy/', 'PrivacypolicyController@editprivacypolicy');
    Route::post('/updateprivacypolicy', 'PrivacypolicyController@updateprivacypolicy');
   //terms&conditions
    Route::get('/manageterms/', 'TermsAndConditionsController@manageterms');
    Route::post('/updateterms', 'TermsAndConditionsController@updateterms');

     Route::get('/manageaboutus/', 'AboutController@manageaboutus');
    Route::post('/updatesAboutus', 'AboutController@updatesAboutus');
    //coupons
	Route::get('/addcoupon', function () {
	return view('admin.coupon.add');
	});
	Route::post('/insertcoupon', 'CouponController@insertcoupon');
	Route::get('/couponlist', 'CouponController@couponlist');
	Route::get('/deletecoupon/{id}/', 'CouponController@deletecoupon');
	Route::get('/editcoupon/{id}', 'CouponController@editcoupon');
    Route::post('/updatecoupon', 'CouponController@updatecoupon');
    //paymenthistory
	Route::get('/paymenthistory', 'AdminController@paymenthistory');
    
	Route::get('/addcategory', function () {
	return view('admin.freeresources.addcategory');
	});
	Route::post('/insertcategory', 'FreeResourcesController@insertcategory');
	Route::get('/addcategory', 'FreeResourcesController@categorylist');
	Route::get('/categorylist', 'FreeResourcesController@categorylistt');
	Route::get('/deletecategory/{id}/', 'FreeResourcesController@deletecategory');
	Route::post('/editcategory/', 'FreeResourcesController@editcategory')->name('editcategory');
	Route::get('/updatecategory/{id}', 'FreeResourcesController@updatecategory');
    
    Route::get('/addshopitem', function () {
	return view('admin.shop.add');
	});
	Route::post('/insertitem', 'ShopController@insertitem');
	Route::get('/shoplist', 'ShopController@shoplist');
	Route::get('/deleteitem/{id}/', 'ShopController@deleteitem');
	Route::get('/edititem/{id}', 'ShopController@edititem');
    Route::post('/updateitem', 'ShopController@updateitem');

    });
/*Account Home After Account Created*/


Route::group(['middleware' => 'auth'], function () {    

Route::get('/accounthome', 'AccountHomeController@index')->name('accounthome');
Route::post('/insertprofile', 'ProfileController@insertprofile')->name('insertprofile');
Route::get('/deleteprofile/{id}/', 'ProfileController@deleteprofile');
Route::get('/editchildprofile/{id}/', 'ProfileController@editprofile');
Route::post('/updatechildprofile', 'ProfileController@updatechildprofile')->name('updatechildprofile');
/*Chart Screens*/

Route::get('/chart', 'ChartController@indexchart')->name('chart');
Route::get('/charthome', 'ChartController@charthome')->name('charthome');
Route::post('/insertchart', 'ChartController@insertchart');
Route::get('/indexhome/{id}/', 'ChartController@indexhome');
Route::get('/deletechart/{id}/', 'ChartController@deletechart');
Route::get('/editchart/{id}/', 'ChartController@editchart');
Route::post('/updatechart', 'ChartController@updatechart')->name('updatechart');
Route::get('/editprofile/{id}/', 'AccountHomeController@editprofile');
Route::post('/updateprofile', 'AccountHomeController@updateprofile')->name('updateprofile'); 
Route::post('/updatesteps/', 'StartoverController@updatesteps');
Route::post('/reversesteps/', 'StartoverController@reversesteps');
Route::get('/startover/{id}/', 'StartoverController@startover');
Route::get('/shopconfirm/{id}/', 'AccountHomeController@shopconfirm');
Route::post('/shopnow', 'AccountHomeController@shopnow');
Route::get('/shopBackground/{id}/', 'AccountHomeController@shopBackground');
Route::get('/shops', 'AccountHomeController@shops');  
Route::get('/certificateprint','StartoverController@download')->name('download');


Route::get('/certificate/{id}/', 'StartoverController@certificate');


 });


/*Route::get('/certificate/', function () {
    return view('certificate');
});*/


Route::get('/tryforfree', 'TryForFreeController@index')->name('tryforfree');
Route::get('/demo', 'TryForFreeController@indexdemo')->name('demo');

Route::get('/list/{id}/', 'FreeResourcesController@list');
Route::get('filedownload/{id}', 'FreeResourcesController@filedownload');

//privacypolicy
Route::get('/aboutus', 'AboutController@aboutus');

Route::get('/termsandcondition', 'TermsAndConditionsController@termsandcondition');
Route::get('/privacypolicy', 'PrivacypolicyController@privacypolicy');
Route::get('/applycoupon/{id}/', 'CouponController@applycoupon');
Route::post('/verifycoupon', 'CouponController@verifycoupon');
Route::get('/payment/', 'CouponController@payment');
Route::post('confirm/', 'CouponController@confirm');

///Shop

Route::get('/thankyou', function () {
    return view('thankyoupayment');
});
Route::get('/approval', function () {
    return view('approval');
});
Route::get('/tryforfree', function () {
    return view('tryforfree');
});
Route::get('/freedemo/', function () {
    return view('demo');
});
Route::get('/freedemo2/', function () {
    return view('demo2');
});

Route::get('/freedemo3/', function () {
    return view('demo3');
});

Route::get('/forgetpassword/', function () {
    return view('forgetpassword');
});
Route::post('/sendemail/', 'Auth\ForgotPasswordController@sendemail');
Route::get('/resetpassword','Auth\ForgotPasswordController@verifyUser')->name('resetpassword');
Route::post('/updatepassword','Auth\ForgotPasswordController@updatepassword')->name('updatepassword');
Route::get('/freeresources', 'FreeResourcesController@freeresources');



